var mongoose = require('mongoose');
var vote = mongoose.model('vote');

exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        vote.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.find = function (query, projection, callback) {
    process.nextTick(function () {
        vote.find(query, projection, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Data not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.update = function (query, update_data, callback) {
    process.nextTick(function () {
        vote.update(query, update_data, function (error, data) {

            if (!error) {
                if (data.nModified > 0) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Bookmark can't be updated" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.UpdateAndSave = function (query, update_data, callback) {
    process.nextTick(function () {
        vote.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {
            if (!error) {
                if (data !== null) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.count = function (query, callback) {
    process.nextTick(function () {
        vote.count(query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};