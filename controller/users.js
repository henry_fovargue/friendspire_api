// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var user = mongoose.model('user');


exports.findOne = function (query, projection, callback) {
    process.nextTick(function () {
        user.findOne(query, projection, function (error, data) {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "User not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.findOne_population = function (query, populate_data, callback) {
    process.nextTick(function () {
        user.findOne(query).populate(populate_data).exec(function (error, data) {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 2, message: "Users not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        user.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});

            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.find = function (query, projection, callback) {
    process.nextTick(function () {
        user.find(query, projection, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Users not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.findData = function (query, projection, callback) {
    process.nextTick(function () {
        user.find(query, projection, function (error, data) {
            if (!error) {
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};


exports.find_pagination = function (query, projection, skip, limit, callback) {
    process.nextTick(function () {
        user.find(query, projection).skip(skip).limit(limit).exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Users not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.find_pagination_sort = function (query, projection, skip, limit, sort, callback) {
    process.nextTick(function () {
        user.find(query, projection).sort(sort).skip(skip).limit(limit).exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Users not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.save = function (user_data, callback) {
    process.nextTick(function () {
        var new_user = new user(user_data);
        new_user.save(function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.update = function (query, update_data, callback) {
    process.nextTick(function () {
        user.update(query, update_data, {multi: true}, function (error, data) {
            //console.log(query);
            //console.log(update_data);
            if (!error) {
                //if (data.nModified > 0) {
                if (data) {
                    callback(null, {status: 1, message: "success"});
                } else {
                    callback(null, {status: 0, message: "Users data could not update"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.updateNew = function (query, update_data, callback) {
    process.nextTick(function () {
        user.update(query, update_data, {multi: true}, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, {status: 1, message: "success"});
                } else {
                    callback(null, {status: 0, message: "Users data could not update"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};



exports.findOneAndUpdate = function (query, update_data, callback) {
    process.nextTick(function () {
        user.findOneAndUpdate(query, update_data, {new : true}, function (error, data) {

            if (!error) {
                if (data !== null) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Users data could not update"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

//remove record
exports.remove = function (query, callback) {
    process.nextTick(function () {
        user.remove(query, function (error, data) {
            if (!error) {
                callback(null, {status: 1, message: "success"});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.count = function (query, callback) {
    process.nextTick(function () {
        user.count(query, function (error, data) {
            if (!error) {
                if (data[0] !== null) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Match not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};




