var mongoose = require('mongoose');
var recommend = mongoose.model('recommendation');


exports.findOneAndUpdate = function (query, update_data, callback) {
    process.nextTick(function () {
        recommend.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {

            if (!error) {
                if (data !== null) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findOneWithPopulate = function (query, projection, callback) {
    process.nextTick(function () {
        recommend.findOne(query, projection).populate('reference_id').exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Data not found", data: {} });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findOnePopulate = function (query, projection, populate, callback) {
    process.nextTick(function () {
        recommend.findOne(query, projection).populate(populate).exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Data not found", data: {} });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findOne = function (query, callback) {
    process.nextTick(function () {
        recommend.findOne(query, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Data not found", data: {} });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.find = function (query, callback) {
    process.nextTick(function () {
        recommend.find(query, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.update = function (query, update_data, callback) {
    process.nextTick(function () {
        recommend.update(query, update_data, function (error, data) {

            if (!error) {
                if (data.nModified > 0) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "User data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.findWithPagination = function (query, projection, sort_condition, skip, limit, callback) {
    process.nextTick(function () {
        recommend.find(query, projection).populate('reference_id').sort(sort_condition).skip(skip).limit(limit).exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.save = function (post_data, callback) {
    process.nextTick(function () {
        var new_data = new recommend(post_data);
        new_data.save(function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.count = function (query, callback) {
    process.nextTick(function () {
        recommend.count(query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findWithProjection = function (query, projection, callback) {
    process.nextTick(function () {
        recommend.find(query, projection, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });

};
exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        recommend.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });

            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.findAndPopulateLimitSortSkip = function (query, projection, population, limit, skip, sort, callback) {
    process.nextTick(function () {
        recommend.find(query, projection).populate(population).sort(sort).skip(skip).limit(limit).exec((error, data) => {
            //            console.log(data);
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findOnePromise = function (query, projection) {
    return new Promise((resolve, reject) => {
        recommend.findOne(query, projection, function (error, data) {
            if (!error) {
                resolve(data);
            } else {
                reject({ status: 0, message: error.message });
            }
        });
    });
};


exports.updateMulti = function (query, update_data, callback) {
    process.nextTick(function () {
        recommend.update(query, update_data, { multi: true }, function (error, data) {

            if (!error) {
                if (data.nModified > 0) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "User data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.updateMultiple = function (query, update_data, callback) {
    process.nextTick(function () {
        recommend.update(query, update_data, { multi: true }, function (error, data) {

            if (!error) {

                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "User data could not update recommend" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
