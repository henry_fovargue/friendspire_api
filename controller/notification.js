// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var notification = mongoose.model('notification');

exports.find_with_projection_population_and_pagination = function (query, projection, population, sort_condition, page_no, callback)
{
    process.nextTick(function ()
    {
        notification.find(query, projection).populate(population).sort(sort_condition).skip((page_no - 1) * 10).limit(10).exec(function (error, data) {
            if (!error)
            {
                if (data.length !== 0)
                {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: "success", data: data});
                } else
                {
                    callback(null, {status: 1, message: "Notification not found", data: []});
                }
            } else
            {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.update = function (query, update_data, callback)
{
    process.nextTick(function ()
    {
        notification.update(query, update_data, {multi: true}, function (error, data)
        {
            if (!error)
            {
                if (data.nModified > 0)
                {
                    callback(null, {status: 1, message: "success"});
                } else
                {
                    callback(null, {status: 0, message: "Notification data could not update"});
                }
            } else
            {
                callback(null, {status: 0, message: error.message});
            }

        });
    });
};

exports.create = function (notification_data, callback)
{
    process.nextTick(function ()
    {
        // if (notification_data.message)
        //     notification_data.message = "<font size='4'>" + notification_data.message + "</font>";
        
        notification.create(notification_data, function (error, data)
        {
            if (!error)
            {
                callback(null, {status: 1, message: "success", data: data});
            } else
            {
                if (error.errors)
                {
                    callback(null, {status: 0, message: "Invalid values"});
                } else
                {
                    callback(null, {status: 0, message: error.message});
                }
            }
        });
    });
};

exports.create_for_admin = function (notification_data, callback)
{
    process.nextTick(function ()
    {
        notification.create(notification_data, function (error, data)
        {
            if (!error)
            {
                callback(null, {status: 1, message: "success", data: data});
            } else
            {
                if (error.errors)
                {
                    callback(null, {status: 0, message: "Invalid values"});
                } else
                {
                    callback(null, {status: 0, message: error.message});
                }
            }
        });
    });
};

exports.findOne = function (query, callback) {
    process.nextTick(function () {
        notification.findOne(query ,function (error, data ) {
            if (!error) {
                //console.log("error", error);
                //console.log("data", data);
                if (data) {
                    callback(null, { status: 1, message: "review is already reported", data:data});
                } else {
                    callback(null, { status: 0, message: error.message});
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.findOne_report = function (query, callback) {
    process.nextTick(function () {
        notification.findOne(query ,function (error, data ) {
            if (!error) {
                //console.log("error", error);
                //console.log("data", data);
                if (data) {
                    callback(null, { status: 1, message: "review is already reported", data:data});
                } else if (data == null) {
                    callback(null, { status: 0, message: "review is not reported"});
                }else {
                    callback(null, { status: 0, message: error.message});
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.count = function (query, callback) {
    process.nextTick(function () {
        notification.count(query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
