// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var settings = mongoose.model('setting');

exports.findOne = function (query, projection, callback) {
    process.nextTick(function () {
        settings.findOne(query, projection, function (error, data) {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "data not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
