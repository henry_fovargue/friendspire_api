var mongoose = require('mongoose');
var comment = mongoose.model('comment');

exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        comment.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.find = function (query, projection, callback) {
    process.nextTick(function () {
        comment.find(query, projection, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Data not found"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.update = function (query, update_data, callback) {
    process.nextTick(function () {
        comment.update(query, update_data, function (error, data) {
            if (!error) {
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.updateMultiple = function (query, update_data, callback) {
    process.nextTick(function () {
        comment.update(query, update_data, { multi: true }, function (error, data) {

            if (!error) {

                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "comment data could not update recommend" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.findOneAndUpdate = function (query, update_data, callback) {
    process.nextTick(function () {
        comment.findOneAndUpdate(query, update_data, {upsert: true, new : true}, function (error, data) {
            if (!error) {
                if (data !== null) {
                    callback(null, {status: 1, message: "success", data: data});
                } else {
                    callback(null, {status: 0, message: "Users data could not update"});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};


exports.save = function (comment_data, callback) {
    process.nextTick(function () {
        var new_data = new comment(comment_data);
        new_data.save(function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, {status: 1, message: "success", data: data});
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};


exports.count = function (query, callback) {
    process.nextTick(function () {
        comment.count(query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};




exports.count_promise = function (query) {
    return new Promise((resolve, reject) => {
        comment.count(query, (error, result) => {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        });
    });
};