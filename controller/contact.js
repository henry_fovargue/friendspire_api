// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var contact_us = mongoose.model('contact_us');

exports.save = function (user_data, callback)
{
    process.nextTick(function ()
    {
        var new_request = new contact_us(user_data);
        new_request.save(function (error, data)
        {
            if (!error)
            {
                callback(null, {status: 1, message: "success", data: data});
            } else
            {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};


exports.find = function (query, projection, callback)
{
    process.nextTick(function ()
    {
        contact_us.find(query, projection, function (error, data) {
            if (!error)
            {
                if (data !== null)
                {
                    callback(null, {status: 1, message: "success", data: data});
                } else
                {
                    callback(null, {status: 0, message: "data not found"});
                }
            } else
            {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.findOne = function (query, callback)
{
    process.nextTick(function ()
    {
        contact_us.findOne(query, function (error, data) {
            if (!error)
            {
                if (data !== null)
                {
                    callback(null, {status: 1, message: "success", data: data});
                } else
                {
                    callback(null, {status: 0, message: "data not found"});
                }
            } else
            {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};




exports.update = function (query, update_data, callback)
{
    process.nextTick(function ()
    {
        contact_us.update(query, update_data, {multi: true}, function (error, data)
        {
            if (!error)
            {
                if (data.nModified > 0)
                {
                    callback(null, {status: 1, message: "success"});
                } else
                {
                    callback(null, {status: 0, message: "data could not update"});
                }
            } else
            {
                callback(null, {status: 0, message: error.message});
            }

        });
    });
};

