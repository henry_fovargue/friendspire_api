var mongoose = require('mongoose');
var report = mongoose.model('report');

exports.findOne = function (query, callback) {
    process.nextTick(function () {
        report.findOne(query ,function (error, data ) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "user already reported"});
                } else {
                    callback(null, { status: 0, message: "user can report"});
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};


exports.findOneAndUpdate = function (query, update_data, callback) {
    process.nextTick(function () {
        report.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {
            //console.log("err",error);
            //console.log("data--",data);
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Review is not reported" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        report.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });

            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.updateNew = function (query, update_data, callback) {
    process.nextTick(function () {
        report.update(query, update_data, { multi: true }, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success" });
                } else {
                    callback(null, { status: 0, message: "Data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};