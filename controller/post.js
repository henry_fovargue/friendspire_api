var mongoose = require('mongoose');
var post = mongoose.model('post');
exports.findWithPagination = function (query, sort_condition, skip, limit, options, callback) {
    process.nextTick(function () {
        post.find(query).sort(sort_condition).skip(skip).limit(limit).paginate(options, function (error, data) {
            if (!error) {
                if (data.length != 0) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "posts not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.aggregate = function (aggregate_query, callback) {
    process.nextTick(function () {
        post.aggregate(aggregate_query, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.save = function (post_data, callback) {
    process.nextTick(function () {
        //        console.log(post_data);
        var new_post = new post(post_data);
        new_post.save(function (error, data) {
            if (!error) {
                //console.log("data");
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.findOne = function (query, projection, callback) {
    process.nextTick(function () {
        post.findOne(query, projection, function (error, data) {
            if (!error) {
                //                console.log(data);
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.findAndUpdate = function (query, update_data, callback) {
    process.nextTick(function () {
        post.findOneAndUpdate(query, update_data, { new: true }, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success" });
                } else {
                    callback(null, { status: 0, message: "post data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};
exports.find = function (query, sort, limit, projection, callback) {
    process.nextTick(function () {
        post.find(query, projection).sort(sort).limit(limit).exec(function (error, data) {
            if (!error) {
                if (data) {
                    //console.log(data);
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "Users not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.find_data = function (query, projection, callback) {
    process.nextTick(function () {
        post.find(query, projection, function (error, data) {
            //console.log(query);
            if (!error) {
                if (data.length > 0) {
                    //console.log(data);
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 2, message: "posts not found", data: [] });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
}

exports.findAndPopulate = function (query, projection, population, callback) {
    process.nextTick(function () {
        post.find(query, projection).populate(population).exec((error, data) => {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post not found" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.update = function (query, update_data, callback) {
    process.nextTick(function () {
        post.update(query, update_data, function (error, data) {
            if (!error) {
                if (data.nModified > 0) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post not updated" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
}
exports.updateOrSave = function (query, update_data, callback) {
    process.nextTick(function () {
        post.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.distinct = function (query, condition, sort, callback) {
    process.nextTick(function () {
        post.distinct(query, condition).sort(sort).exec(function (error, data) {
            if (!error) {
                if (data) {
                    callback(null, { status: 1, message: "success", data: data });
                } else {
                    callback(null, { status: 0, message: "post data could not update" });
                }
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    }
    );
};

exports.count = function (query, callback) {
    process.nextTick(function () {
        post.count(query, function (error, data) {
            if (!error) {
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};

exports.saveMany = function (post_data, callback) {
    process.nextTick(function () {
        //        console.log(post_data);
        post.insertMany(post_data, function (error, data) {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                callback(null, { status: 1, message: "success", data: data });
            } else {
                callback(null, { status: 0, message: error.message });
            }
        });
    });
};