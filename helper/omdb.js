var Sync = require('sync');
var request = require('request');
var credentials = require('../Third_party_api_credentials.js').omdb;
var OmdbApi = require('omdb-api-pt');
const h2p = require('html2plaintext');


//exports.search_omdb = (title, type, callback) => {
////    console.log(credentials);
//    process.nextTick(function ()
//    {
//        var omdb = new OmdbApi(credentials);
//        omdb.byId({
//            title: title,
//            type: type, //series or movie
//            plot: 'full'
//        }).then(res => {
////            console.log("recommendations");
////            console.log(res);
//            if (res.Response == 'True') {
//                var res_json = {
//                    "reference_id": res.imdbID,
//                    "title": res.Title,
//                    "year": res.Year == "N/A" ? "" : res.Year,
//                    "genre": res.Genre == "N/A" ? [] : res.Genre.split(", "),
//                    "image": [res.Poster == "N/A" ? "" : res.Poster],
//                    "pages_duration": res.Runtime == "N/A" ? 0 : parseInt(res.Runtime.split(" ")[0]),
//                    "rated": res.Rated == "N/A" ? 0 : res.Rated,
//                    "rating": res.imdbRating == "N/A" ? 0.00 : parseFloat(res.imdbRating),
//                    "author": res.Writer == "N/A" ? "" : res.Writer, //writer
//                    "director": res.Director == "N/A" ? "" : res.Director,
//                    "actors": res.Actors == "N/A" ? "" : res.Actors,
//                    "description": res.Plot == "N/A" ? "" : h2p(res.Plot),
//                    "language": res.Language == "N/A" ? [] : res.Language.split(", "),
//                    "country": res.Country == "N/A" ? [] : res.Country.split(","),
//                    "awards": res.Awards == "N/A" ? "" : res.Awards,
//                    "release_date": res.Released == "N/A" ? null : new Date(res.Released)
//                }
//                callback(null, {status: 1, data: res_json});
//            } else {
//                callback(null, {status: 2, data: res["Error"]});
//            }
//
//        }).catch(err => {
//            callback(null, {status: 0, data: err.message});
//        });
//    });
//}


exports.search_omdb = (reference_id, type, callback) => {
//    console.log(credentials);
    process.nextTick(function ()
    {
        var omdb = new OmdbApi(credentials);
        omdb.byId({
            imdb: reference_id,
            type: type, //series or movie
            plot: 'full'
        }).then(res => {
//            console.log("recommendations");
//            console.log(res);
            if (res.Response == 'True') {
                var res_json = {
                    "title": res.Title,
                    // "year": res.Year == "N/A" ? "" : res.Year,
                    "image": res.Poster == "N/A" ? [] : [res.Poster],
                    "genre": res.Genre == "N/A" ? [] : res.Genre.split(", "),
                    "pages_duration": res.Runtime == "N/A" ? 0 : parseInt(res.Runtime.split(" ")[0]),
                    "rated": res.Rated == "N/A" ? "" : res.Rated,
                    "rating": res.imdbRating == "N/A" ? 0.00 : parseFloat(res.imdbRating),
                    "author": res.Writer == "N/A" ? "" : res.Writer, //writer
                    "director": res.Director == "N/A" ? "" : res.Director,
                    "actors": res.Actors == "N/A" ? "" : res.Actors,
                    "description": res.Plot == "N/A" ? "" : h2p(res.Plot),
                    "language": res.Language == "N/A" ? [] : res.Language.split(", "),
                    "country": res.Country == "N/A" ? [] : res.Country.split(", "),
                    "awards": res.Awards == "N/A" ? "" : res.Awards,
                    "release_date": res.Released == "N/A" ? null : new Date(res.Released)
                }
                if (type.toLowerCase() == "movie") {
                    res_json["year"] = res.Year == "N/A" ? 0 : parseInt(res.Year);
                } else {
                    let year = res.Year == "N/A" ? [] : res.Year.split("–");
                    if (year[0]) {
                        res_json["start_year"] = parseInt(year[0]);
                    } else {
                        res_json["start_year"] = 0;
                    }
                    if (year[1]) {
                        res_json["end_year"] = parseInt(year[1]);
                    } else {
                        res_json["end_year"] = 0;
                    }
                }
                callback(null, {status: 1, data: res_json});
            } else {
                callback(null, {status: 2, data: res["Error"]});
            }

        }).catch(err => {
            callback(null, {status: 0, data: err.message});
        });
    });
}