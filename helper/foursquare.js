var Sync = require('sync');
var request = require('request');
var credentials = require('../Third_party_api_credentials.js')["foursquare"];
const config = require("../config");
const util = require("util");
const h2p = require('html2plaintext');

//exports.search_foursquare = function (latitude, longitude, string, type, callback) {
////    console.log("condition");
////    console.log(string);
////    console.log(latitude + "," + longitude);
//    let querystring = {
//        client_id: credentials.clelntId,
//        client_secret: credentials.clelntSecret,
//        "ll": latitude + "," + longitude,
//        'radius': config.nearbyRaduis * 6371000,
//        query: string,
////        intent:"browse",
//        v: '20180323',
//        sortByDistance: 1,
//        limit: 1
//    };
//    if (type === 1) {
//        querystring["categoryId"] = '4d4b7105d754a06374d81259';
//    } else if (type === 2) {
//        querystring["categoryId"] = '4d4b7105d754a06376d81259';
//    }
//    request({
//        url: 'https://api.foursquare.com/v2/venues/search',
//        method: 'GET',
//        qs: querystring
//    }, function (err, res, body) {
////        console.log(querystring);
//        if (err) {
//            console.error(err);
//            callback(null, {status: 0, message: err.message});
//        } else {
//
//            let data = JSON.parse(body);
////            console.log("recommendation");
////            console.log(data.response.venues);
//            if (data.response.venues.length < 1) {
//                callback(null, {status: 2, message: "no restaurants"});
//            } else {
////                console.log(util.inspect(data, {showHidden: true, depth: null}));
//                let type = Object.prototype.toString.call(data.response.venues).toString();
//                if (type == "[object Array]") {
//                    var restaurant_json = {
//                        reference_id: data.response.venues[0]["id"],
//                        title: data.response.venues[0]["name"] || ""
//                    }
//                    if (data.response.venues[0].location) {
//                        restaurant_json["location"] = {coordinates: [data.response.venues[0].location.lng || null, data.response.venues[0].location.lat || null]};
//                        restaurant_json["city"] = data.response.venues[0].location["city"] || "";
//                        restaurant_json["country"] = [data.response.venues[0].location["country"]] || "";
//                        restaurant_json["address"] = data.response.venues[0].location.formattedAddress || [];
//                        restaurant_json["distance"] = data.response.venues[0]["location"]["distance"] || null;
//                    }
//                    let p1 = exports.details(restaurant_json["reference_id"]);
//                    let p2 = exports.search_opening_closing_hours(restaurant_json["reference_id"]);
//                    let p3 = exports.search_photos(restaurant_json["reference_id"]);
//                    Promise.all([p1, p2, p3]).then(([detail, timings, photos]) => {
//                        Object.assign(restaurant_json, detail);
//                        restaurant_json["timings"] = timings;
////                        console.log(restaurant_json["timings"]);
//                        restaurant_json["image"] = photos;
//                        callback(null, {status: 1, data: restaurant_json});
//                    }).catch(error => {
//                        console.log(error);
//                        callback(null, {status: 0, message: error.message});
//                    })
//                } else {
//                    callback(null, {status: 2, message: "no restaurants"});
//                }
//            }
//        }
//    }
//    );
//}

exports.search_foursquare = function (reference_id, callback) {
    var restaurant_json = {};
    let p1 = exports.details(reference_id);
    let p2 = exports.search_opening_closing_hours(reference_id);
    let p3 = exports.search_photos(reference_id);
    Promise.all([p1, p2, p3]).then(([detail, timings, photos]) => {
        Object.assign(restaurant_json, detail);
        restaurant_json["timings"] = timings;
//                        console.log(restaurant_json["timings"]);
        restaurant_json["image"] = photos;
        callback(null, {status: 1, data: restaurant_json});
    }).catch(error => {
        console.log(error);
        callback(null, {status: 0, message: error.message});
    })
}

exports.details = (venue_id) => {
    return new Promise((resolve, reject) => {
//        console.log("venue_id");
//        console.log(venue_id);
        request({
            url: 'https://api.foursquare.com/v2/venues/' + venue_id,
            method: 'GET',
            qs: {
                client_id: credentials.clelntId,
                client_secret: credentials.clelntSecret,
                v: '20180323',
            }
        }, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                let res = {};
                let detail = JSON.parse(body);
//                console.log("details");
//                detail, {showHidden: true, depth: null}));
                if (detail.response !== undefined || detail.response !== null) {
                    if (detail.response.venue) {
                        res["title"] = detail.response.venue["name"] || "";
                        let categories = detail.response.venue["categories"].map(value => value.shortName);
                        if (detail.response.venue["price"]) {
                            res["price_tier"] = detail.response.venue["price"]["tier"] || 0;
                        }
                        res["rating"] = detail.response.venue["rating"] || 0.00;
                        if (detail.response.venue["contact"]) {
                            res["phone"] = detail.response.venue["contact"]["phone"] || "";
                        }
                        if (detail.response.venue["location"])
                        {
                            res["location"] = {coordinates: [detail.response.venue.location.lng || 0.00, detail.response.venue.location.lat || 0.00]};
                            res["city"] = detail.response.venue.location["city"] || "";
                            res["country"] = [detail.response.venue.location["country"]] || "";
                            res["address"] = detail.response.venue.location.formattedAddress || [];
//                                      ] 
                        }
                        res["url"] = detail.response.venue["url"] || "";
                        res["description"] = h2p(detail.response.venue["description"]) || "";
                        res["timezone"] = detail.response.venue["timeZone"] || "";
                        if (detail.response.venue["popular"]) {
                            res["is_open"] = detail.response.venue["popular"]["isOpen"];
                        } else {
                            res["is_open"] = null;
                        }
                        res["cuisine"] = categories || [];
                        resolve(res);
                    } else {
                        reject({message: detail.meta.errorDetail});
                    }
                } else {
                    reject({message: detail.meta.errorDetail});
                }
            }
        });
    });
}

exports.search_opening_closing_hours = function (venue_id) {
    return new Promise((resolve, reject) => {
        request({
            url: 'https://api.foursquare.com/v2/venues/' + venue_id + "/hours",
            method: 'GET',
            qs: {
                client_id: credentials.clelntId,
                client_secret: credentials.clelntSecret,
                v: '20180323',
            }
        }, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                let newTimings = [];
//                 let newTimings = [[], [], [], [], [], [], []];
                body = JSON.parse(body);
                let timearray = [];
                if (body["response"]) {
                    if (body["response"]["popular"]) {
                        let timings = body["response"]["popular"]["timeframes"] || [];
                        timings.forEach(obj => {
                            obj["days"].forEach(day => {
                                timearray[day - 1] = obj["open"];
                            });
                        });
                    } else if (body["response"]["hours"]) {
                        let timings = body["response"]["hours"]["timeframes"] || [];
                        timings.forEach(obj => {
                            obj["days"].forEach(day => {
                                timearray[day - 1] = obj["open"];
                            });
                        });
                    }
                    if (timearray.length > 0) {
                        newTimings = [[], [], [], [], [], [], []];
                        timearray.forEach((objtiming, index) => {
                            objtiming.forEach(obj => {
                                if (obj["end"].charAt(0) == "+" && parseInt(obj["end"]) != 0000) {
                                    if (index == 6) {
                                        newTimings[0].push({"start": "0000", "end": parseInt(obj["end"]).toString()});
                                    } else {
                                        newTimings[index + 1].push({"start": "0000", "end": parseInt(obj["end"]).toString()});
                                    }
                                    newTimings[index].push({"start": obj["start"], "end": "2359"});
                                } else {
                                    if (parseInt(obj["end"]) == 0000) {
                                        newTimings[index].push({"start": obj["start"], "end": "2359"});
                                    } else {
                                        newTimings[index].push(obj);
                                    }

                                }
                            });
                        });
                    }
//                    console.log(newTimings);
                    resolve(newTimings);
                } else {
                    resolve(newTimings);
                }
            }

        });
    });
}

exports.search_photos = function (venue_id) {
    return new Promise((resolve, reject) => {
        request({
            url: 'https://api.foursquare.com/v2/venues/' + venue_id + "/photos",
            method: 'GET',
            qs: {
                client_id: credentials.clelntId,
                client_secret: credentials.clelntSecret,
                v: '20180323',
            }
        }, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                let photoarray = [];
                body = JSON.parse(body);
//                console.log("photos");
//                console.log(util.inspect(body, {depth: null}));
                if (body["response"]) {
                    if (body["response"]["photos"]) {
                        photoarray = body["response"]["photos"]["items"].map(obj => {
                            return obj.prefix + "size" + obj["suffix"]; //here size 100x200
                        });
                    }
                    resolve(photoarray);
                } else {
                    resolve(photoarray);
                }
            }

        });
    });
}
