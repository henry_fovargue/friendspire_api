var Sync = require('sync');
var request = require('request');
var OmdbApi = require('omdb-api-pt');
const credentials = require('../Third_party_api_credentials.js');
const goodreads = require('goodreads-api-node')(credentials.goodread);
const config = require('../config');
const util = require("util");

exports.thirdPartyPedictiveSearch = (search, callback) => {
    process.nextTick(function () {
        let title = search.title;
        let page = search.page;
        let category = search.category;
        let resultarray = [];
        let reference_ids = [];
        switch (category) {
            case 1:
            case 2:
            {
//                console.log("search foursquare");
                let querystring = {
                    client_id: credentials["foursquare"].clelntId,
                    client_secret: credentials["foursquare"].clelntSecret,
                    "ll": search.latitude + "," + search.longitude,
                    'radius': config.nearbyRaduis * 6371000,
                    llAc: 10000,
                    query: title,
                    v: '20180323',
                    sortByDistance: 1,
                    limit: page * config.predictiveSearchPagination,
                };
                if (category === 1) {
                    querystring["categoryId"] = '4d4b7105d754a06374d81259';
                } else if (category === 2) {
                    querystring["categoryId"] = '4d4b7105d754a06376d81259';
                }
                request({
                    url: 'https://api.foursquare.com/v2/venues/search',
                    method: 'GET',
                    qs: querystring
                }, function (err, res, body) {
                    if (err) {
                        console.error(err);
                        callback(null, {status: 0, message: err.message});
                    } else {
                        let data = JSON.parse(body);
                        if (data.meta.code == 200) {
                            if (data.response.venues.length < 1) {
                                callback(null, {status: 2, message: "no restaurants"});
                            } else {
                                let type = Object.prototype.toString.call(data.response.venues).toString();
                                if (type == "[object Array]") {
                                    let response = data.response.venues.slice((page - 1) * config.predictiveSearchPagination, page * config.predictiveSearchPagination);
//                                    console.log(util.inspect(response, {depth: null}));
                                    for (let i = 0; i < response.length; i++) {
                                        let restaurant_json = {};
                                        restaurant_json["reference_id"] = response[i]["id"];
                                        restaurant_json["title"] = response[i]["name"] || "";
                                        restaurant_json["type"] = category;
                                        if (data.response.venues[i].location) {
                                            restaurant_json["location"] = {coordinates: [response[i].location.lng || 0.00, response[i].location.lat || 0.00]};
                                        }
                                        restaurant_json["city"] = data.response.venues[i].location["city"] || "";
                                        restaurant_json["country"] = [data.response.venues[i].location["country"]] || "";
                                        restaurant_json["address"] = data.response.venues[i].location.formattedAddress || [];
//                                            restaurant_json["distance"] = data.response.venues[i]["location"]["distance"] || null;
                                        resultarray.push(restaurant_json);
                                        reference_ids.push(restaurant_json["reference_id"]);
                                    }
                                }
                            }
                        } else {
                            callback(null, {status: 2, message: "no restaurants"});
                        }
//                        console.log(util.inspect(resultarray, {depth: null}));
//                        console.log(reference_ids);
                        callback(null, {status: 1, result: resultarray, reference_ids: reference_ids});
                    }

                });
                break;
            }
            case 3:
            case 4:
            {
//                console.log("search omdb");
                if (category == 3) {
                    var type = "movie";
                } else {
                    var type = "series";
                }
                var omdb = new OmdbApi(credentials.omdb);
                if (title.length < 3) {
                    omdb.byId({
                        title: title,
                        type: type, //series or movie
                        plot: 'full'
                    }).then(res => {
//            console.log(res);
                        if (res.Response == 'True') {
                            if (category == 3) {

                                resultarray.push({
                                    "reference_id": res.imdbID || "",
                                    "type": category,
                                    "title": res.Title,
                                    "year": res.Year == "N/A" ? "" : res.Year,
                                    "image": [res.Poster == "N/A" ? "" : res.Poster]
                                });
                                reference_ids.push(res.imdbID)
                            } else {
                                let year = res.Year == "N/A" ? [] : res.Year.split("–");
                                resultarray.push({
                                    "reference_id": res.imdbID,
                                    "type": category,
                                    "title": res.Title,
                                    "start_year": year[0] ? parseInt(year[0]) : null,
                                    "end_year": year[1] ? parseInt(year[1]) : null,
                                    "image": res.Poster == "N/A" ? [] : [res.Poster],
                                });
                                reference_ids.push(res.imdbID)
                            }
                            callback(null, {status: 1, result: resultarray, reference_ids: reference_ids});
                        } else {
                            callback(null, {status: 2, data: res["Error"]});
                        }

                    }).catch(err => {
                        callback(null, {status: 0, data: err.message});
                    });
                } else {
                    omdb.bySearch({
                        search: title,
                        type: type, //series or movie
                        page: page
                    }).then(response => {
//                    console.log("response");
//                    console.log(response);
                        if (response.Response == 'True') {
                            let res = response.Search;
                            if (category == 3) {
                                for (let i = 0; i < res.length; i++) {
                                    resultarray.push({
                                        "reference_id": res[i].imdbID,
                                        "type": category,
                                        "title": res[i].Title,
                                        "year": res[i].Year == "N/A" ? "" : parseInt(res[i].Year),
                                        "image": res[i].Poster == "N/A" ? [] : [res[i].Poster],
                                    });
                                    reference_ids.push(res[i].imdbID)
                                }
                            } else {
                                for (let i = 0; i < res.length; i++) {
                                    let year = res[i].Year == "N/A" ? [] : res[i].Year.split("–");
                                    resultarray.push({
                                        "reference_id": res[i].imdbID,
                                        "type": category,
                                        "title": res[i].Title,
                                        "start_year": year[0] ? parseInt(year[0]) : null,
                                        "end_year": year[1] ? parseInt(year[1]) : null,
                                        "image": res[i].Poster == "N/A" ? [] : [res[i].Poster],
                                    });
                                    reference_ids.push(res[i].imdbID)
                                }
                            }
                        } else {
                            callback(null, {status: 2, data: response["Error"]});
                        }
                        callback(null, {status: 1, result: resultarray, reference_ids: reference_ids});
                    }).catch(err => {
                        callback(null, {status: 0, data: err.message});
                    });
                }



                break;
            }
            case 5:
            {
//                console.log("search goodreads");
                if (page % 2 === 0) {
                    pagegoodreads = (page / 2);
                } else {
                    pagegoodreads = (page / 2) + 1;
                }
                goodreads.searchBooks({'q': title, "field": "title", 'page': pagegoodreads}).then(function (data) {
                    if (data.search["total-results"] == '0') {
                        callback(null, {status: 2, message: "No book find"});
                    } else {
                        if (data.search.results.work.length < 1) {
                            callback(null, {status: 2, message: "No book find"});
                        } else {
//                            console.log(util.inspect(data,{depth:null}));
                            let type = Object.prototype.toString.call(data.search.results.work).toString();
                            if (type == "[object Array]") {
//                                console.log(data.search.results.work.length);
                                if (page % 2 !== 0) {
                                    var searchResult = data.search.results.work.slice(0, 10);
                                } else {
                                    var searchResult = data.search.results.work.slice(10, 20);
                                }
                                for (let i = 0; i < searchResult.length; i++) {
                                    if (data.search.results.work[i].best_book) {
                                        let book_json = {
                                            reference_id: data.search.results.work[i].best_book.id._ || "",
                                            type: category,
                                            title: data.search.results.work[i].best_book.title || "",
                                            author: data.search.results.work[i].best_book.author.name || "",
                                            year: parseInt(data.search.results.work[i].original_publication_year._) || "",
                                            image: [data.search.results.work[i].best_book.image_url || ""],
//                    image: data.search.results.work[i].best_book.small_image_url,
                                            rating: parseFloat(data.search.results.work[i].average_rating) || 0,
                                        };

                                        if (!data.search.results.work[i].original_publication_year.nil)
                                            book_json.year = parseInt(data.search.results.work[i].original_publication_year._) || 0;
                                        else
                                            book_json.year = 0;
                                        resultarray.push(book_json);
                                        reference_ids.push(book_json["reference_id"]);
                                    }
                                }
                            } else if (type == "[object Object]") {
                                if (data.search.results.work.best_book) {
                                    let book_json = {
                                        reference_id: data.search.results.work.best_book.id._ || null,
                                        type: 5,
                                        title: data.search.results.work.best_book.title || "",
                                        author: data.search.results.work.best_book.author.name || "",
                                        year: parseInt(data.search.results.work.original_publication_year._) || 0,
                                        image: [data.search.results.work.best_book.image_url || ""],
//                    image: data.search.results.work[0].best_book.small_image_url,
                                        rating: parseFloat(data.search.results.work.average_rating) || 0,
                                    };

                                    if (!data.search.results.work.original_publication_year.nil)
                                        book_json.year = parseInt(data.search.results.work.original_publication_year._) || 0;
                                    else
                                        book_json.year = 0;

                                    resultarray.push(book_json);
                                    reference_ids.push(book_json["reference_id"]);

                                } else {
                                    callback(null, {status: 2, message: "No book find"});
                                }
                            } else {
                                callback(null, {status: 2, message: "No book find"});
                            }
                            callback(null, {status: 1, result: resultarray, reference_ids: reference_ids});
                        }
                    }
                }).catch(error => {
                    console.log(error);
                    callback(null, {status: 0, message: error.message});
                });
                break;
            }
        }
    });
}


