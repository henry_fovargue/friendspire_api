exports.project = (category) => {
    let projection;
    switch (category) {
        case '1':
        case '2':
            {
                projection = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //    start_year: 0,
                    //    end_year: 0,
                    //    year: 0, //
                    //    genre: 0,
                    //    rated: 0,
                    //    author: 0, //author -> book,writer -> movie
                    //    director: 0, //author -> book,writer -> movie
                    //    actors: 0,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    //    language: 0,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    //    awards: 0,
                    //    pages_duration: 0, //page -> book, duration -> movies and series(minutes)
                    country: 1,
                    city: 1,
                    address: 1,
                    //    release_date: 0,
                    location: 1, //store in the form of long lat
                    price_tier: 1,
                    phone: 1,
                    url: 1,
                    cuisine: 1,
                    timings: 1,
                    timezone: 1,
                    bookmark: 1,
                    distance: 1,
                    updated_at: 1,
                    expiry_time: 1,
                    dummy: 1// exiration time for the recordes 
                };

                break;
            }
        case '3':
            {
                projection = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //                start_year: 1,
                    //                end_year: 1,
                    year: 1, //
                    genre: 1,
                    rated: 1,
                    author: 1, //author -> book,writer -> movie
                    director: 1,
                    actors: 1,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    language: 1,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    awards: 1,
                    pages_duration: 1, //page -> book, duration -> movies and series(minutes)
                    country: 1,
                    //    city: 0,
                    //    address: 0,
                    release_date: 1,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    updated_at: 1,
                    expiry_time: 1, // exiration time for the recordes 
                    dummy: 1
                };
                break;
            }
        case '4':
            {
                projection = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    start_year: 1,
                    end_year: 1,
                    year: 1, //
                    genre: 1,
                    rated: 1,
                    author: 1, //author -> book,writer -> movie
                    director: 1,
                    actors: 1,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    language: 1,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    awards: 1,
                    pages_duration: 1, //page -> book, duration -> movies and series(minutes)
                    country: 1,
                    //    city: 0,
                    //    address: 0,
                    release_date: 1,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    updated_at: 1,
                    expiry_time: 1, // exiration time for the recordes 
                    dummy: 1
                };
                break;
            }
        case '5':
            {
                projection = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //    start_year: 0,
                    //    end_year: 0,
                    year: 1, //
                    genre: 1,
                    //    rated: 0,
                    author: 1, //author -> book,writer -> movie
                    //    director: 0,
                    //    actors: 0,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    language: 1,
                    publisher: 1,
                    description: 1, //plot in case of book
                    isbn: 1,
                    //    awards: 0,
                    pages_duration: 1, //page -> book, duration -> movies and series(minutes)
                    //    country: 0,
                    //    city: 0,
                    //    address: 0,
                    //    release_date: 0,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    updated_at: 1,
                    expiry_time: 1, // exiration time for the recordes 
                    dummy: 1
                };
                break;
            }
    }
    return projection;
}

exports.projectlist = (category) => {
    let list;
    switch (category) {
        case '1':
        case '2':
            {
                list = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //    end_year: 0,
                    //    year: 0, //
                    //    genre: 0,
                    //    rated: 0,
                    //    author: 0, //author -> book,writer -> movie
                    //    director: 0, //author -> book,writer -> movie
                    //    actors: 0,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    //    language: 0,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    //    awards: 0,
                    //    pages_duration: 0, //page -> book, duration -> movies and series(minutes)
                    country: 1,
                    city: 1,
                    //    release_date: 0,
                    location: 1, //store in the form of long lat
                    price_tier: 1,
                    cuisine: 1,
                    timings: 1,
                    timezone: 1,
                    bookmark: 1,
                    distance: 1,
                    friends_rating: 1,
                    user_rating: 1,
                    everyone_count: 1,
                    updated_at: 1,
                    my_rating: 1,
                    my_bookmark_status: 1
                    //    expiry_time: 0  // exiration time for the recordes 
                };

                break;
            }

        case '3':
            {
                list = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //                start_year: 1,
                    //                end_year: 1,
                    year: 1, //
                    genre: 1,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    pages_duration: 1, //page -> book, duration -> movies and series(minutes)
                    //    city: 0,
                    //    address: 0,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    friends_rating: 1,
                    user_rating: 1,
                    everyone_count: 1,
                    updated_at: 1,
                    my_rating: 1,
                    my_bookmark_status: 1
                    //    expiry_time: 0  // exiration time for the recordes 
                };
                break;
            }
        case '4':
            {
                list = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    start_year: 1,
                    end_year: 1,
                    year: 1, //
                    genre: 1,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    //    publisher: 0,
                    description: 1, //plot in case of book
                    //    isbn: 0,
                    pages_duration: 1, //page -> book, duration -> movies and series(minutes)
                    //    city: 0,
                    //    address: 0,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    friends_rating: 1,
                    user_rating: 1,
                    everyone_count: 1,
                    updated_at: 1,
                    my_rating: 1,
                    my_bookmark_status: 1
                    //    expiry_time: 0  // exiration time for the recordes 
                };
                break;
            }
        case '5':
            {
                list = {
                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                    reference_id: 1,
                    title: 1,
                    //    start_year: 0,
                    //    end_year: 0,
                    year: 1, //
                    genre: 1,
                    //    rated: 0,
                    author: 1, //author -> book,writer -> movie
                    //    director: 0,
                    //    actors: 0,
                    friendspire_rating: 1,
                    rating: 1,
                    image: 1,
                    description: 1, //plot in case of book
                    //    awards: 0,
                    //    country: 0,
                    //    city: 0,
                    //    address: 0,
                    //    release_date: 0,
                    //    location: 0, //store in the form of long lat
                    //    price_tier: 0,
                    //    phone: 0,
                    //    url: 0,
                    //    cuisine: 0,
                    //    timings: 0,
                    bookmark: 1,
                    friends_rating: 1,
                    user_rating: 1,
                    pages_duration: 1,
                    everyone_count: 1,
                    updated_at: 1,
                    my_rating: 1,
                    my_bookmark_status: 1
                    //    expiry_time: 0  // exiration time for the recordes 

                };
                break;
            }
    }
    return list;
}




exports.projectlistInspirationRecommend = {

    friends: 1,
    "post_detail._id": 1,
    "post_detail.type": 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    //                reference_id: 1,
    "post_detail.title": 1,
    //    end_year: 0,
    //    year: 0, //
    "post_detail.genre": 1,
    //    rated: 0,
    //    author: 0, //author -> book,writer -> movie
    //    director: 0, //author -> book,writer -> movie
    //    actors: 0,
    "post_detail.friendspire_rating": 1,
    "post_detail.author": 1,
    //                rating: 1,
    "post_detail.image": 1,
    //    language: 0,
    //    publisher: 0,
    //                description: 1, //plot in case of book
    //    isbn: 0,
    //    awards: 0,
    //    pages_duration: 0, //page -> book, duration -> movies and series(minutes)
    "post_detail.country": 1,
    "post_detail.city": 1,
    //    release_date: 0,
    //                location: 1, //store in the form of long lat
    //                price_tier: 1,
    "post_detail.cuisine": 1,
    "count": 1,
    //                timings: 1,
    //                timezone: 1,
    //                bookmark: 1,
    "post_detail.distance": 1,
    //                friends_rating: 1,
    "post_detail.user_rating": 1,
    "post_detail.updated_at": 1,
    "post_detail.rated_at": 1,
    "post_detail.algorithm": { "$literal": 0 },
    "post_detail.friends": "$friends",
    "post_detail.recomendation_count": { '$size': '$total' }
    //    "recomendation_count":1
    //    expiry_time: 0  // exiration time for the recordes 
};

exports.projectlistInspirationRecommendAlgo = {

    friends: 1,
    "post_detail._id": 1,
    "post_detail.type": 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    //                reference_id: 1,
    "post_detail.title": 1,
    //    end_year: 0,
    //    year: 0, //
    "post_detail.genre": 1,
    //    rated: 0,
    //    author: 0, //author -> book,writer -> movie
    //    director: 0, //author -> book,writer -> movie
    //    actors: 0,
    "post_detail.friendspire_rating": 1,
    //                rating: 1,
    "post_detail.image": 1,
    "post_detail.author": 1,
    //    language: 0,
    //    publisher: 0,
    //                description: 1, //plot in case of book
    //    isbn: 0,
    //    awards: 0,
    //    pages_duration: 0, //page -> book, duration -> movies and series(minutes)
    "post_detail.country": 1,
    "post_detail.city": 1,
    //    release_date: 0,
    //                location: 1, //store in the form of long lat
    //                price_tier: 1,
    "post_detail.cuisine": 1,
    "count": 1,
    //                timings: 1,
    //                timezone: 1,
    //                bookmark: 1,
    "post_detail.distance": 1,
    //                friends_rating: 1,
    "post_detail.user_rating": 1,
    "post_detail.updated_at": 1,
    "post_detail.rated_at": 1,
    "post_detail.algorithm": { "$literal": 1 },
    "post_detail.recomendation_count": { '$size': '$total' }
    //    "post_detail.friends": "$friends",
    //    expiry_time: 0  // exiration time for the recordes 
};


exports.projectlistInspirationPost = {
    "_id": 1,
    "type": 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    //                reference_id: 1,
    "title": 1,
    //    end_year: 0,
    //    year: 0, //
    "genre": 1,
    //    rated: 0,
    //    author: 0, //author -> book,writer -> movie
    //    director: 0, //author -> book,writer -> movie
    //    actors: 0,
    "friendspire_rating": 1,
    //                rating: 1,
    "image": 1,
    "author": 1,
    //    language: 0,
    //    publisher: 0,
    //                description: 1, //plot in case of book
    //    isbn: 0,
    //    awards: 0,
    //    pages_duration: 0, //page -> book, duration -> movies and series(minutes)
    "country": 1,
    "city": 1,
    //    release_date: 0,
    //                location: 1, //store in the form of long lat
    //                price_tier: 1,
    "cuisine": 1,
    //                timings: 1,
    //                timezone: 1,
    //                bookmark: 1,
    "distance": 1,
    //                friends_rating: 1,
    "user_rating": 1,
    "count": 1,
    "algorithm": { "$literal": 1 },
    "updated_at": 1,
    "rated_at": 1,
    //    "recomendation_count":{ '$size': '$reviews' }
};
