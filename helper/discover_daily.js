var mongoose = require('mongoose');
var users = require('../controller/users.js');
var posts = require('../controller/post.js');
var recomendations = require('../controller/recommendations.js');
var algo_conditions = require('../helper/algo_conditions.js');
var project = require("../helper/projectionFields.js");
var Sync = require('sync');
const util = require("util");
exports.discover = function (req, res, user_id, current_loc, callback) {
    process.nextTick(function () {
        Sync(function () {
            var skip = 0, limit = 15;
            var friends = [];
            let page = parseInt(req.params.page_no);
            let lastday = new Date();
            lastday.setDate(lastday.getDate() - 1);
            //find the user 
            var user_detail = users.findOne.sync(null, {_id: user_id, is_deleted: 0, status: 1}, {friends: 1, longlat: 1});
            if (user_detail.status == 1) {
                //if (user_detail.data.longlat.length > 0) {
                //set rules for user 
                var rules = [], final_result = [];
                if (user_detail.data.longlat.length > 0 && req.query.home_city) {
                    rules = exports.set_rules.sync(null);
                } else {
                    rules = exports.set_rules_without_home.sync(null);
                }

                var data = [];
                if (user_detail.data.friends.length > 0) {
                    for (var i = 0; i < user_detail.data.friends.length; i++) {
                        if (user_detail.data.friends[i].status == 1 && user_detail.data.friends[i].is_blocked == 0) {
                            friends.push(mongoose.Types.ObjectId(user_detail.data.friends[i].user_id));
                        }
                    }
                    var my_recomended_posts = [];
                    var users_recommendations = recomendations.findWithProjection.sync(null,{user_id:req.decoded._id,is_deleted:0},{reference_id:1});
                    if(users_recommendations.status == 1){
                        for (var j = 0; j < users_recommendations.data.length; j++) {
                            my_recomended_posts.push(mongoose.Types.ObjectId(users_recommendations.data[j].reference_id))
                        }
                    }
                    console.log("my_recomended_posts");
                    console.log(my_recomended_posts);
                    var usersWhoBlockMe = users.find.sync(null, {blocked_user: {$eq: user_id}, is_deleted: 0, status: 1}, {_id: 1});
                    var usersWhoBlockMe_array = [];
                    for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                        usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                    }
                    friends = friends.filter(f => {
                        if (usersWhoBlockMe_array.includes(f.toString())) {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    friends = friends.map(obj => {
                        return mongoose.Types.ObjectId(obj)
                    });
                    // console.log(friends);
                    if (page === 1) {
                        data = exports.algorithm_based_on_rules.sync(null, req.decoded._id, rules, current_loc, user_detail.data.longlat, req.query.current_city, req.query.home_city, friends);
                        //    console.log("-----------------FROM algo ----------------");
                        //    console.log(util.inspect(data, {depth: null, hidden: true}));
                        limit = 16;
                    } else {
                        skip = (page - 1) * 15;
                    }
                    let postarray = data.map(post => {
                        if (post["post_detail"]) {
                            return mongoose.Types.ObjectId(post["post_detail"]._id);
                        } else {
                            return mongoose.Types.ObjectId(post._id);
                        }
                    });
                    // concat my_recomended posts and posts from algorithm
                    postarray = postarray.concat(my_recomended_posts);
                    
                    if (friends.length >= 0) {
                        var recomendation_count = recomendations.count.sync(null, {is_deleted: 0, $and: [{user_id: {$in: friends}}, {user_id: {$ne: mongoose.Types.ObjectId(req.decoded._id)}}], reference_id: {"$nin": postarray}});
                        //show recomendation from your friends in chrornological order
                        if (recomendation_count.data > 0) {
                            var recomendation_list_query = [
                                {$match: {is_deleted: 0, $and: [{user_id: {$in: friends}}, {user_id: {$ne: mongoose.Types.ObjectId(req.decoded._id)}}], reference_id: {"$nin": postarray}}},
                                {$sort: {updated_at: -1}},
                                {
                                    $group: {
                                        _id: "$reference_id",
                                        friends: {"$push": {_id: "$user_id", rating: "$rating", review: "$review"}},
                                        updated_at: {$first: "$updated_at"}
                                    }
                                },
                                {"$unwind": "$friends"},
                                {
                                    "$lookup": {
                                        from: "users",
                                        let: {ref_id: "$friends._id"},
                                        pipeline: [
                                            {
                                                $match:
                                                        {
                                                            $expr:
                                                                    {
                                                                        $and:
                                                                                [
                                                                                    {$eq: ["$_id", "$$ref_id"]},
                                                                                    {$eq: ["$status", 1]},
                                                                                    {$eq: ["$is_deleted", 0]}
                                                                                ]
                                                                    }
                                                        }
                                            },
                                            {"$project": {_id: 1, firstName: 1, lastName: 1, profile_pic: 1}}
                                        ],
                                        as: "user_detail"
                                    }
                                },
                                {"$unwind": "$user_detail"},
                                {
                                    "$group": {
                                        "_id": "$_id",
                                        "friends": {"$push": {user_detail: "$user_detail", rating: "$friends.rating", review: "$friends.review"}}
                                    }
                                },
                                {
                                    $lookup: {
                                        "from": "posts",
                                        "localField": "_id",
                                        "foreignField": "_id",
                                        "as": "post_detail"
                                    }
                                },
                                {$unwind: "$post_detail"},
                                {
                                    "$lookup": {
                                        from: "recommendations",
                                        let: {ref_id: "$post_detail._id"},
                                        pipeline: [
                                            {
                                                $match:
                                                        {
                                                            $expr:
                                                                    {
                                                                        $and:
                                                                                [
                                                                                    {$eq: ["$reference_id", "$$ref_id"]},
                                                                                    {$in: ["$user_id", friends]},
                                                                                    {$eq: ["$is_deleted", 0]}
                                                                                ]
                                                                    }
                                                        }
                                            },
                                        ],
                                        as: "total"
                                    }
                                },
                                {"$project": project.projectlistInspirationRecommend},
                                {$sort: {"post_detail.rated_at": -1}},
                                {"$skip": skip},
                                {$limit: limit}
                            ];
//                            console.log("condition-----------------------------------");
//                            console.log(util.inspect(recomendation_list_query, {depth: null}));
                            //                                console.log(projectionInspiration);
                            var list = recomendations.aggregate.sync(null, recomendation_list_query);
                            // console.log("list~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                            // console.log(util.inspect(list, { depth: null }));
                            if (list.status == 1) {
                                if (list.data.length >= 0) {
//                                    console.log(list["data"].length);
//                                    console.log("-----------------FROM recommendations ----------------");
//                                    console.log(util.inspect(list, {depth: null}));
//                                    console.log("recommendation length");
//                                    console.log(list["data"].length);
//                                    console.log("algo length");
//                                    console.log(data.length);
//                                    console.log(postarray.length);
                                    let length = list["data"].length + data.length;
//                                    console.log("total length");
//                                    console.log(length);
//                                    console.log("list[data].length", list["data"].length);
//                                    console.log("data.length", data.length);

                                    let count = 0;
                                    let algocount = 0;
                                    if (data.length >= 0) {
                                        try {
                                            for (let i = 0; i < length; i++) {
                                                if (final_result.length == 4 || final_result.length == 9 || final_result.length == 14 || final_result.length == 19) {
                                                    if (data[algocount] != undefined) {
                                                        if (data[algocount].reviews == 0) {
                                                            let post = posts.findOne.sync(null, {_id: data[algocount]["_id"]}, {});
                                                            if (post["status"] === 1) {
                                                                final_result.push(post["data"]);
                                                            }
                                                        } else {
                                                            if (data[algocount]["post_detail"]) {
                                                                final_result.push(data[algocount]["post_detail"]);
                                                            } else {
                                                                final_result.push(data[algocount]);
                                                            }
                                                        }
                                                    }
                                                    algocount++;
                                                }
                                                else {
                                                    if (list.data[count] != undefined) {

                                                        final_result.push(list.data[count].post_detail);
                                                        count++;
                                                    }
                                                }
                                            }
                                            //    console.log("===========================");
                                            //    console.log("recommendation count");
                                            //    console.log(count);
                                            //    console.log("algo count");
                                            //    console.log(algocount);

                                            if (list["data"].length - count > 0) {
                                                //                                                console.log("left recommendation iteration-------");
                                                for (i = count; i < list["data"].length; i++) {
                                                    if (list.data[count] != undefined) {
                                                        final_result.push(list.data[count].post_detail);
                                                        count++;
                                                    }

                                                }
                                            }
                                            if (data.length - algocount > 0) {
                                                for (i = algocount; i < data.length; i++) {
                                                    if (data[algocount] != undefined) {
                                                        if (data[algocount] != undefined) {
                                                            if (data[algocount].reviews == 0) {
                                                                let post = posts.findOne.sync(null, {_id: data[algocount]["_id"]}, {});
                                                                if (post["status"] === 1) {
                                                                    final_result.push(post["data"]);
                                                                }
                                                            } else {
                                                                if (data[algocount]["post_detail"]) {
                                                                    final_result.push(data[algocount]["post_detail"]);
                                                                } else {
                                                                    final_result.push(data[algocount]);
                                                                }
                                                            }
                                                        }
                                                        algocount++;
                                                    }
                                                }
                                            }
                                        } catch (err) {
                                            console.log(err);
                                        }
                                    }
                                }
                            }
                            callback(null, final_result);
                        } else {
                            if (req.params.page_no == 1) {
                                // show recomendations on basis of algorithm
                                var data = exports.algorithm_based_on_rules.sync(null, req.decoded._id, rules, current_loc, user_detail.data.longlat, req.query.current_city, req.query.home_city, friends);
                                for (let i = 0; i < data.length; i++) {
                                    if (data[i].reviews == 0) {
                                        let post = posts.findOne.sync(null, {_id: data[i]["_id"]}, {});
                                        if (post["status"] === 1) {
                                            final_result.push(post["data"]);
                                        }
                                    } else {
                                        if (data[i]["post_detail"]) {
                                            final_result.push(data[i]["post_detail"]);
                                        } else {
                                            final_result.push(data[i]);
                                        }
                                    }
                                }
                                callback(null, final_result);
                            } else {
                                callback(null, final_result);
                            }
                        }
                    } else {
                        if (req.params.page_no == 1) {
                            // show recomendations on basis of algorithm
                            var data = exports.algorithm_based_on_rules.sync(null, req.decoded._id, rules, current_loc, user_detail.data.longlat, req.query.current_city, req.query.home_city, friends);
                            for (let i = 0; i < data.length; i++) {
                                if (data[i].reviews == 0) {
                                    let post = posts.findOne.sync(null, {_id: data[i]["_id"]}, {});
                                    if (post["status"] === 1) {
                                        final_result.push(post["data"]);
                                    }
                                } else {
                                    if (data[i]["post_detail"]) {
                                        final_result.push(data[i]["post_detail"]);
                                    } else {
                                        final_result.push(data[i]);
                                    }
                                }
                            }
                            callback(null, final_result);
                        } else {
                            callback(null, final_result);
                        }
                    }
                } else {
                    if (req.params.page_no == 1) {
                        // show recomendations on basis of algorithm
                        var data = exports.algorithm_based_on_rules.sync(null, req.decoded._id, rules, current_loc, user_detail.data.longlat, req.query.current_city, req.query.home_city, friends);
                        //                            console.log(data);
                        for (let i = 0; i < data.length; i++) {
                            if (data[i].reviews == 0) {
                                let post = posts.findOne.sync(null, {_id: data[i]["_id"]}, {});
                                if (post["status"] === 1) {
                                    final_result.push(post["data"]);
                                }
                            } else {
                                if (data[i]["post_detail"]) {
                                    final_result.push(data[i]["post_detail"]);
                                } else {
                                    final_result.push(data[i]);
                                }
                            }
                        }
                        callback(null, final_result);
                    } else {
                        callback(null, final_result);
                    }
                }
                // } else {
                //     res.status(400).send({ message: "lat long not present in user data", status: 0 });
                // }
            }
        });
    });
}


exports.set_rules = function (callback) {
    process.nextTick(function () {
        try {
            var rules_array = [];
            var rules_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'];
            for (var i = 0; rules_array.length < 4; i++) {
                var no = Math.floor(Math.random() * 27);
                var rule_no = rules_list[no];
                if (rules_array.indexOf(rule_no) == -1) {
                    rules_array.push(rules_list[no]);
                } else {
                    continue;
                }
            }
            callback(null, rules_array);
        } catch (err) {
            console.log(err);
        }
    });
}
exports.set_rules_without_home = function (callback) {
    process.nextTick(function () {
        try {
            var rules_array = [];
            var rules_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];
            for (var i = 0; rules_array.length < 4; i++) {
                var no = Math.floor(Math.random() * 19);
                var rule_no = rules_list[no];
                if (rules_array.indexOf(rule_no) == -1) {
                    rules_array.push(rules_list[no]);
                } else {
                    continue;
                }
            }
            callback(null, rules_array);
        } catch (err) {
            console.log(err);
        }
    });
}
var rules_descripton = function (rule, current_city, home_city) {
    let message = "";
    var current_city = current_city;
    if (home_city)
        var home_city = home_city;
    else
        var home_city = "";
    switch (parseInt(rule)) {
        case 1:
            message = "Highest rated movie last 7 days"
            break;
        case 2:
            message = "Highest rated series last 7 days"
            break;
        case 3:
            message = "Highest rated book last 7 days"
            break;
        case 4:
            message = "Highest rated movie last 30 days"
            break;
        case 5:
            message = "Highest rated series last 30 days"
            break;
        case 6:
            message = "Highest rated book last 30 days"
            break;
        case 7:
            message = "Highest rated restaurant in " + current_city + " last 7 days"
            break;
        case 8:
            message = "Highest rated bar in " + current_city + " last 7 days"
            break;
        case 9:
            message = "Highest rated restaurant in " + current_city + " last 30 days"
            break;
        case 10:
            message = "Highest rated bar in " + current_city + " last 30 days"
            break;
        case 11:
            message = "Highest rated restaurant in " + home_city + " last 7 days"
            break;
        case 12:
            message = "Highest rated bar in " + home_city + " last 7 days"
            break;
        case 13:
            message = "Highest rated restaurant in " + home_city + " last 30 days"
            break;
        case 14:
            message = "Highest rated bar in " + home_city + " last 30 days"
            break;
        case 15:
            message = "Most rated movie last 7 days"
            break;
        case 16:
            message = "Most rated series last 7 days"
            break;
        case 17:
            message = "Most rated book last 7 days"
            break;
        case 18:
            message = "Most rated movie last 30 days"
            break;
        case 19:
            message = "Most rated series last 30 days"
            break;
        case 20:
            message = "Most rated book last 30 days"
            break;
        case 21:
            message = "Most rated restaurant in " + current_city + " last 7 days"
            break;
        case 22:
            message = "Most rated bar in " + current_city + " last 7 days"
            break;
        case 23:
            message = "Most rated restaurant in " + current_city + " last 30 days"
            break;
        case 24:
            message = "Most rated bar in " + current_city + " last 30 days"
            break;
        case 25:
            message = "Most rated restaurant in " + home_city + " last 7 days"
            break;
        case 26:
            message = "Most rated bar in " + home_city + " last 7 days"
            break;
        case 27:
            message = "Most rated restaurant in " + home_city + " last 30 days"
            break;
        case 28:
            message = "Most rated bar in " + home_city + " last 30 days"
            break;
    }
    return message;
}

exports.algorithm_based_on_rules = function (user_id, rules, current, home, current_city, home_city, friends, callback) {
    process.nextTick(function () {
        Sync(function () {
            var results = [];
            //            var recommendpostid = recomendations.findWithProjection.sync(null, {is_deleted: 0}, {reference_id: 1, user_id: 1})
            //            console.log("post ids recommended by me");
            //            console.log(recommendpostid);
            //            console.log("=================");
            //            var not_in_post_ids = recommendpostid["data"].map(obj => {
            //                return mongoose.Types.ObjectId(obj["reference_id"]);
            //            });
            var not_in_post_ids = [];
            //            var not_in_recomendation_ids = [];
            for (var i = 0; i < rules.length; i++) {
                var condition = algo_conditions.conditions.sync(null, user_id, rules[i], current, home, not_in_post_ids, friends);
                //                                console.log(JSON.stringify(condition.condition));
                //                                condition.condition.push({"$project": project.projectlistInspiration});
                // console.log("condition");
                // console.log(util.inspect(condition, { depth: null }));
                //                console.log("message");
                var message = rules_descripton(rules[i], current_city, home_city);
                //                console.log("message");
                //                console.log(message);
                if (condition.type == 2) {

                    condition.condition.push({"$project": project.projectlistInspirationRecommendAlgo});
                    //console.log(util.inspect(condition, { depth: null }));
                    var data = recomendations.aggregate.sync(null, condition.condition);
                    //                    console.log("data-----------------------");
                    //                    console.log(util.inspect(data, {depth: null}));
                    if (data.data[0]) {
                        if (data.data[0]) {
                            not_in_post_ids.push(mongoose.Types.ObjectId(data.data[0].reference_id));
                            not_in_post_ids.push(mongoose.Types.ObjectId(data.data[0]._id));
                            //                            console.log(data.data[0]);
                            if (data.data[0].post_detail) {
                                not_in_post_ids.push(mongoose.Types.ObjectId(data.data[0].post_detail._id));
                                data.data[0].post_detail.reason = message;
                            } else {
                                data.data[0].reason = message;
                            }
                            //                            console.log(data.data[0]);
                            results.push(data.data[0]);

                        }
                    }
                } else {
                    //                   console.log(util.inspect(condition,{depth:null}));
                    //                    condition.condition.push({"$project": project.projectlistInspirationPost});

                    var data = posts.aggregate.sync(null, condition.condition);
                    //                    console.log("data~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    //                    console.log(util.inspect(data, {depth: null}));
                    if (data.data[0]) {
                        not_in_post_ids.push(mongoose.Types.ObjectId(data.data[0]._id));
                        if (data.data[0].post_detail) {
                            not_in_post_ids.push(mongoose.Types.ObjectId(data.data[0].post_detail._id));
                            data.data[0].post_detail.reason = message;
                        } else {
                            data.data[0].reason = message;
                        }
                        //                        console.log(data.data[0]);
                        results.push(data.data[0]);
                    }
                }
                //                                console.log("not_in_post_ids");
                //                                console.log(not_in_post_ids);
            }
            callback(null, results);
        });
    });
}

