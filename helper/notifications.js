var fcm = require('fcm-push'); // project server key never change
var config = require('../config');
var fcm_push = new fcm(config.push_notification.server_key);

// send notifications
exports.send_notification = function (token, data, badge_count) {
    var message = {
        to: token,
        priority: "high",
        notification: {
            title: 'Friendspire',
            sound: 'default',
            body: data.body,
            'content-available': "true",
            click_action: "OPEN_ACTIVITY_DASHBOARD",
            event_type: data.type,
            badge : badge_count
        },
        data: data
    };
    fcm_push.send(message, function (err, response) {
        if (err) {
            console.log("err");
            console.log(err);
        } else {
            console.log("response");
            console.log(response);
        }
        return(1);
    });
}


exports.send_notification_to_all = function (tokens, data, badge_array)
{
    console.log(badge_array);
    for (var i = 0; i < badge_array.length; i++)
    {
        var message = {
            to: badge_array[i].device_token,
            priority: "high",
            notification: {
                title: 'Friendspire',
                sound: 'default',
                body: data.body,
               'content-available': "true",
                click_action: "OPEN_ACTIVITY_DASHBOARD",
                event_type: data.type,
                badge: badge_array[i].badge_count
            },
            data: data
        };
        console.log(message);
        fcm_push.send(message, function (err, response) {
            if (err) {
                console.log("notification error")
                console.log(err)
            } else {
                console.log("notification send")
                console.log(response)
            }
            return(1);
        });
    }
}

exports.cron_notification_to_all = function (data)
{
    //console.log(data);
    for (var i = 0; i < data.length; i++)
    {
        var message = {
            to: data[i].token,
            priority: "high",
            notification: {
                title: 'Friendspire',
                sound: 'default',
                body: data[i].message,
               'content-available': "true",
                click_action: "OPEN_ACTIVITY_DASHBOARD",
                event_type: 7,
                badge: data[i].badge_count
            },
            data: {
                title: 'Friendspire',
                body: data[i].message,
                type: 7
            }
        };
        console.log(message);
        fcm_push.send(message, function (err, response) {
            if (err) {
                console.log("notification error")
                console.log(err)
            } else {
                console.log("notification send")
                console.log(response)
            }
            return(1);
        });
    }
}

// exports.same_notification_to_all = function (tokens, data) {
//     var token_array;
//     while (tokens.length) {
//         token_array = tokens.splice(0, 1000);
//         var message = {
//             registration_ids: token_array,
//             priority: "high",
//             notification: {
//                 title: 'Mr. Clipper',
//                 body: data.body,
//                 'content-available': "true"
//             },
//             data: data
//         };
//         console.log("message");
//         console.log(message);
//         fcm_push.send(message, function (err, response) {
//             if (err) {
//                 console.log("notification error");
//                 console.log(err);
//             } else {
//                 console.log("notification send");
//                 console.log(response);
//             }
//         });
//     }
//     return(1);
// }