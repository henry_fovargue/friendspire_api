module.exports.login_response_json = function (user_data)
{
    var send_data = {};
    send_data._id = user_data._id;
    send_data.sns_type = user_data.sns_type;
    send_data.sns_id = user_data.sns_id;
    send_data.fb_connected = user_data.fb_connected;
    send_data.firstName = user_data.firstName;
    send_data.lastName = user_data.lastName;
    send_data.email = user_data.email;
    send_data.is_email_verified = user_data.is_email_verified;
    send_data.profile_pic = user_data.profile_pic;
    send_data.status = user_data.status;
    send_data.push_notification = user_data.push_notification;
    send_data.distance_unit = user_data.distance_unit;
    send_data.is_private = user_data.is_private;
    send_data.token = user_data.token;
    send_data.longlat = user_data.longlat;
    send_data.address = user_data.address;
    send_data.reference_code = user_data.reference_code;
    send_data.referral_code = user_data.referral_code;
    send_data.tutorial_read = user_data.tutorial_read;
    return send_data;
};