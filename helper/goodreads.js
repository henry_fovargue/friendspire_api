const credentials = require('../Third_party_api_credentials.js');
const goodreads = require('goodreads-api-node')(credentials.goodread);
const  Sync = require('sync');
const util = require("util");
const h2p = require('html2plaintext');
const isoConv = require('iso-language-converter');

//exports.search_goodreads = (string, callback) => {
//
//    process.nextTick(function () {
//        goodreads.searchBooks({'q': string, "field": "title", 'page': 1}).then(function (data) {
//            if (data.search["total-results"] == '0') {
//                callback(null, {status: 2, message: "No book find"});
//            } else {
////                console.log("recommendations");
////                console.log(util.inspect(data.search.results.work, {depth: null}));
//                if (data.search.results.work.length < 1) {
//                    callback(null, {status: 2, message: "No book find"});
//                } else {
//                    let type = Object.prototype.toString.call(data.search.results.work).toString();
//                    if (type == "[object Array]") {
//                        if (data.search.results.work[0].best_book) {
//                            let book_json = {
//                                reference_id: data.search.results.work[0].best_book.id._ || "",
//                                type: 5,
//                                title: data.search.results.work[0].best_book.title || "",
//                                author: data.search.results.work[0].best_book.author.name || "",
//                                year: parseInt(data.search.results.work[0].original_publication_year._) || "",
//                                image: [data.search.results.work[0].best_book.image_url || ""],
////                    image: data.search.results.work[0].best_book.small_image_url,
//                                rating: parseFloat(data.search.results.work[0].average_rating) || 0,
//                            };
//
//                            if (!data.search.results.work[0].original_publication_year.nil)
//                                book_json.year = parseInt(data.search.results.work[0].original_publication_year._) || null;
//                            else
//                                book_json.year = 0;
//
//                            exports.bookdetails(data.search.results.work[0].best_book.id._).then(data => {
//                                if (data) {
//                                    book_json["description"] = h2p(data.plot) || "";
//                                    book_json["isbn"] = data.isbn || "";
//                                    if (data["language"]) {
//                                        book_json["language"] = data.language.split(", ") || [];
//                                    } else {
//                                        book_json["language"] = [];
//                                    }
//                                    book_json["publisher"] = data.publisher || "";
//                                    book_json["pages_duration"] = parseInt(data.pages) || 0;
//                                    callback(null, {status: 1, data: book_json});
//                                }
//                            }).catch(error => {
//                                throw error;
//                            });
//                        }
//                    } else if (type == "[object Object]") {
//                        if (data.search.results.work.best_book) {
//                            let book_json = {
//                                reference_id: data.search.results.work.best_book.id._ || null,
//                                type: 5,
//                                title: data.search.results.work.best_book.title || "",
//                                author: data.search.results.work.best_book.author.name || "",
//                                year: parseInt(data.search.results.work.original_publication_year._) || null,
//                                image: [data.search.results.work.best_book.image_url || ""],
////                    image: data.search.results.work[0].best_book.small_image_url,
//                                rating: parseFloat(data.search.results.work.average_rating) || 0,
//                            };
//
//                            if (!data.search.results.work.original_publication_year.nil)
//                                book_json.year = parseInt(data.search.results.work.original_publication_year._) || null;
//                            else
//                                book_json.year = 0;
//
//                            exports.bookdetails(data.search.results.work.best_book.id._).then(data => {
//                                if (data) {
//                                    book_json["description"] = h2p(data.plot) || "";
//                                    book_json["isbn"] = data.isbn || "";
//                                    if (data["language"]) {
//                                        book_json["language"] = data.language.split(", ") || [];
//                                    } else {
//                                        book_json["language"] = [];
//                                    }
//                                    book_json["publisher"] = data.publisher || "";
//                                    book_json["pages_duration"] = parseInt(data.pages) || null;
//                                    callback(null, {status: 1, data: book_json});
//                                }
//                            }).catch(error => {
//                                throw error;
//                            });
//                        } else {
//                            callback(null, {status: 2, message: "No book find"});
//                        }
//                    } else {
//                        callback(null, {status: 2, message: "No book find"});
//                    }
//                }
//            }
//        }).catch(error => {
////            console.log("error here");
//            console.log(error);
//            callback(null, {status: 0, message: error.message});
//        });
//    });
//}

exports.search_goodreads = (reference_id, callback) => {

    process.nextTick(function () {
        let book_json = {};
        exports.bookdetails(reference_id).then(data => {
//            console.log("data");
//            console.log(data);
            if (data) {
                book_json["description"] = h2p(data.plot) || "";
                book_json["rating"] = data.rating || 0.00;
                book_json["author"] = data.author || "";
                book_json["isbn"] = data.isbn || "";
                if (data["language"]) {
//                    book_json["language"] = data.language.split(", ") || [];
                    let lang = data.language.split(", ") || [];
                    if (lang.length > 0) {
                        lang = lang.map(langcode => {
                            if (isoConv(langcode)) {
                                return isoConv(langcode);
                            } else {
                                return langcode;
                            }
                        });
                    }
                    book_json["language"] = lang || [];
                } else {
                    book_json["language"] = [];
                }
                book_json["publisher"] = data.publisher || "";
                book_json["pages_duration"] = parseInt(data.pages) || 0;
            }
            callback(null, {status: 1, data: book_json});

        }).catch(error => {
            callback(null, {status: 0, message: error.message});
        });
    });
}

exports.bookdetails = (book_id) => {
    return new Promise((resolve, reject) => {
//        console.log(book_id);
        goodreads.showBook(book_id).then((book) => {
            let book_detail = JSON.parse(JSON.stringify(book));
//            console.log(util.inspect(book, {showHidden: true, depth: null}));
//            console.log(util.inspect(book_detail, {showHidden: true, depth: null}));
            let detail = {};
            if (book_detail.book) {
//                console.log(book_detail.book.authors.author);
//                console.log(Array.isArray(book_detail.book.authors.author));
                if (book_detail.book.authors) {
                    if (Array.isArray(book_detail.book.authors.author)) {
                        let authorarray = book_detail.book.authors.author.map(obj => {
                            return obj.name;
                        });
//                        console.log(authorarray);
//                        console.log(authorarray.join());
                        detail["author"] = authorarray.join().toString();
                    } else {
                        detail["author"] = book_detail.book.authors.author.name || "";
//                        console.log("here");
                    }
                }
                detail["plot"] = book_detail.book.description || "";
                if (book_detail.book.isbn && book_detail.book.isbn13) {
                    detail["isbn"] = book_detail.book.isbn + " (ISBN13: " + book_detail.book.isbn13 + ")" || "";
                } else if (book_detail.book.isbn && !book_detail.book.isbn13) {
                    detail["isbn"] = book_detail.book.isbn || "";
                } else {
                    detail["isbn"] = "";
                }
                detail["language"] = book_detail.book.language_code || "";
                detail["publisher"] = book_detail.book.publisher || "";
                detail["pages"] = book_detail.book.num_pages || "";
                detail["rating"] = parseFloat(book_detail.book.average_rating) || 0.00;
                console.log(detail);
                resolve(detail);
            } else {
                resolve(detail)
            }

        }).catch(error => {
            console.log(error);
            reject(error);
        });
    });
}