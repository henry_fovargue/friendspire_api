var Sync = require('sync');

var config = require('../config');

var users = require('../controller/users.js');
var email_template = require('../controller/email_template.js');

var send_mail = require('../helper/send_mail.js');

var emailTemplate = require('../emails_templates'); // get our config file
var otp_verification = emailTemplate.otp_verification;
var email_verification = emailTemplate.email_verification;
var forgotPasswordTemp = emailTemplate.forgot_password_app_user;
var welcomeEmail = emailTemplate.welcome_email;

var thankYouMailContact = emailTemplate.thank_you_mail_contact;
var contactUsAdmin = emailTemplate.contact_us_admin;
var crypto = require('../helper/crypto.js'); //for encryption and decryption of user_id

// signup_email 
exports.signup_otp_email = function (userData, callback) {
    process.nextTick(function () {
        Sync(function () {
            try
            {
                var emailTemplate = email_template.findOne.sync(null, {_id: otp_verification});

                var otp_code = Math.random().toString().substr(2, 6);
                var timeStamp = new Date().getTime();
                var otp_expiry = timeStamp + (30 * 60 * 1000);

                if (emailTemplate.status == 1) {
                    content = emailTemplate.data.content;
                    content = content.replace("@name@", userData.firstName);
                    content = content.replace("@otp@", otp_code);

                    send_mail.sendMail.sync(null, userData.email, emailTemplate.data.subject, content);
//                    send_mail.sendMail.sync(null, config.admin.email, userData.email, emailTemplate.data.subject, content);
                }

                var updateQuery = {};
                updateQuery.email_otp_code = otp_code;
                updateQuery.email_otp_expiry = otp_expiry;

                var userUpdate = users.update.sync(null, {_id: userData._id}, updateQuery);
                console.log("otp_code");
                console.log(otp_code);
                if (userUpdate.status == 0)
                    return callback(null, {status: 0, message: "Error"});

                return callback(null, {status: 1, data: {otp_code: otp_code}});

            } catch (err) {
                console.log(err);
                return callback(null, {status: 0, message: err.message});
            }
        });
    });
}


// forgot pass_email 
exports.forgot_password_otp_email = function (userData, callback) {
    process.nextTick(function () {
        Sync(function () {
            try
            {

                var emailTemplate = email_template.findOne.sync(null, {_id: forgotPasswordTemp});
                var otp_code = Math.random().toString().substr(2, 6);
                var timeStamp = new Date().getTime();
                var otp_expiry = timeStamp + (30 * 60 * 1000);

                if (emailTemplate.status == 1) {
                    content = emailTemplate.data.content;
                    content = content.replace("@name@", userData.data.firstName);
                    content = content.replace("@otp@", otp_code);

//                    send_mail.sendMail.sync(null, config.admin.email, userData.data.email, emailTemplate.data.subject, content);
                    send_mail.sendMail.sync(null, userData.data.email, emailTemplate.data.subject, content);
                }

                var updateQuery = {};
                updateQuery.forgot_otp_code = otp_code;
                updateQuery.forgot_otp_expiry = otp_expiry;

                var userUpdate = users.update.sync(null, {_id: userData.data._id}, updateQuery);

                if (userUpdate.status == 0)
                    return callback(null, {status: 0, message: "Error"});

                return callback(null, {status: 1, data: {otp_code: otp_code}});


            } catch (err) {
                console.log(err);
                return callback(null, {status: 0, message: err.message});
            }
        });
    });
}

// contact_us_mail  
exports.change_email = function (user_data,email) {
    Sync(function () {
        try
        {
            //send thank you mail to user
            var emailTemplate = email_template.findOne.sync(null, {_id: email_verification});
            if (emailTemplate.status == 1) {
                var token = crypto.encrypt(user_data._id.toString());
                //send email for verification and after verification update the email in database
                var link = '<a href="' + config.env.serviceUrl + 'verify_email/' + token + '">link</a>';
                content = emailTemplate.data.content;

                // get email content that we have to send
                content = content.replace("@name@", user_data.firstName);
                content = content.replace("@email@", email);
                content = content.replace("@link@", link);


//                send_mail.sendMail.sync(null, config.admin.email, contactUsData.email, emailTemplate.data.subject, content);
                send_mail.sendMail.sync(null, user_data.email, emailTemplate.data.subject, content);
            }
            return;
        } catch (err) {
            console.log(err);
            return;
        }
    });
}


// change_email_otp 
exports.change_email_otp = function (userData, email, callback) {
    process.nextTick(function () {
        Sync(function () {
            try
            {
                var emailTemplate = email_template.findOne.sync(null, {_id: otp_verification});
                console.log(emailTemplate);
                var otp_code = Math.random().toString().substr(2, 6);
                var timeStamp = new Date().getTime();
                var otp_expiry = timeStamp + (30 * 60 * 1000);

                if (emailTemplate.status == 1) {
                    content = emailTemplate.data.content;
                    content = content.replace("@name@", userData.firstName);
                    content = content.replace("@otp@", otp_code);
                    send_mail.sendMail.sync(null, email, emailTemplate.data.subject, content);
//                    send_mail.sendMail.sync(null, config.admin.email, userData.email, emailTemplate.data.subject, content);
                }

                var updateQuery = {};
                updateQuery.email_otp_code = otp_code;
                updateQuery.email_otp_expiry = otp_expiry;

                var userUpdate = users.update.sync(null, {_id: userData._id}, updateQuery);
                console.log("otp_code");
                console.log(otp_code);
                if (userUpdate.status == 0)
                    return callback(null, {status: 0, message: "Error"});

                return callback(null, {status: 1, data: {otp_code: otp_code}});

            } catch (err) {
                console.log(err);
                return callback(null, {status: 0, message: err.message});
            }
        });
    });
}


// welcome_mail  
exports.welcome_mail = function (userData, callback) {
    process.nextTick(function () {
        Sync(function () {
            try
            {
                var emailTemplate = email_template.findOne.sync(null, {_id: welcomeEmail});
                if (emailTemplate.status == 1) {
                    content = emailTemplate.data.content;
                    content = content.replace("@name@", userData.firstName);

//                    send_mail.sendMail.sync(null, config.admin.email, userData.email, emailTemplate.data.subject, content);
                    send_mail.sendMail.sync(null, userData.email, emailTemplate.data.subject, content);
                }

                return callback(null, {status: 1});

            } catch (err) {
                console.log(err);
                return callback(null, {status: 0, message: err.message});
            }
        });
    });
}


// contact_us_mail  
exports.contact_us_mail = function (contactUsData) {
    Sync(function () {
        try
        {
            //send thank you mail to user
            var emailTemplate = email_template.findOne.sync(null, {_id: thankYouMailContact});
            if (emailTemplate.status == 1) {
                content = emailTemplate.data.content;
                content = content.replace("@name@", contactUsData.name);

//                send_mail.sendMail.sync(null, config.admin.email, contactUsData.email, emailTemplate.data.subject, content);
                send_mail.sendMail.sync(null, contactUsData.email, emailTemplate.data.subject, content);
            }



            //send contact us mail to admin
            var emailTemplate = email_template.findOne.sync(null, {_id: contactUsAdmin});
            if (emailTemplate.status == 1) {
                content = emailTemplate.data.content;
                content = content.replace("@subject@", contactUsData.subject);
                content = content.replace("@email@", contactUsData.email);
                content = content.replace("@message@", contactUsData.message);

//                send_mail.sendMail.sync(null, config.admin.email, config.admin.email, emailTemplate.data.subject, content);
                send_mail.sendMail.sync(null, config.admin.email, emailTemplate.data.subject, content);
            }

            return;

        } catch (err) {
            console.log(err);
            return;
        }
    });
}

