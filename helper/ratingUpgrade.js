var Sync = require('sync');
var request = require('request');
var OmdbApi = require('omdb-api-pt');
const credentials = require('../Third_party_api_credentials.js');
const goodreads = require('goodreads-api-node')(credentials.goodread);

const util = require("util");
exports.getNewRating = function (category, reference_id, callback) {

    process.nextTick(function () {
        switch (category) {
            case 1:
            case 2:
            {
                let updatedRating;
                request({
                    url: 'https://api.foursquare.com/v2/venues/' + reference_id,
                    method: 'GET',
                    qs: {
                        client_id: credentials["foursquare"].clelntId,
                        client_secret: credentials["foursquare"].clelntSecret,
                        v: '20180323',
                    }
                }, (err, res, body) => {
                    if (err) {
                        callback(null, {status: 0, message: err.message});
                    } else {
                        let detail = JSON.parse(body);
                        updatedRating = detail["response"]["venue"]["rating"];
                        callback(null, {status: 1, data: {rating: parseFloat(updatedRating)}});
                    }
                });
                break;
            }
            case 3:
            case 4:
            {
                var omdb = new OmdbApi(credentials.omdb);
                omdb.byId({
                    imdb: reference_id
                }).then(res => {
                    if (res.Response == 'True') {
                        var res_json = {
                            "rated": res.Rated == "N/A" ? 0 : res.Rated,
                            "rating": res.imdbRating == "N/A" ? 0.00 : parseFloat(res.imdbRating),
                            "awards": res.Awards == "N/A" ? "" : res.Awards,
                        }
                        callback(null, {status: 1, data: res_json});
                    } else {
                        callback(null, {status: 0, data: res["Error"]});
                    }
                }).catch(err => {
                    callback(null, {status: 0, data: err.message});
                });
                break;
            }
            case 5:
            {
                goodreads.showBook(reference_id).then((book) => {
                    let book_detail = JSON.parse(JSON.stringify(book));
                    if (book_detail.book) {
                        let updatedRating = book_detail.book.average_rating || 0;
                        callback(null, {status: 1, data: {rating: parseFloat(updatedRating)}});
                    } else {
                        callback(null, {status: 0, data: "detail not available"});
                    }
                }).catch(error => {
                    console.log(error);
                    callback(null, {status: 0, data: error.message});
                });
                break;
            }
        }
    });
}