const config = require("../config");
var moment = require('../public/javascripts/moment.js')
var mongoose = require('mongoose');
var projectionField = require("../helper/projectionFields");
exports.conditions = function (login_id, rule, current, home, not_in_ids, friends, callback) {
    process.nextTick(function () {
        try {
            login_id = mongoose.Types.ObjectId(login_id);
            var projectionPost = {};
            Object.assign(projectionPost, projectionField.projectlistInspirationPost)
            var current_date = new Date(moment().utc().toISOString());  //new Date();
            var seven_day_before = new Date(moment().utc().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).subtract(8, 'd').toISOString());
            var thirty_day_before = new Date(moment().utc().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).subtract(31, 'd').toISOString());
            var condition = []
            if (rule == '1') {
                condition = [{ $match: { type: 3, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                //{$sort: {friendspire_rating: 1}},

                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                {
                //                    $lookup: {
                //                        "from": "recommendations",
                //                        "localField": "post_detail._id",
                //                        "foreignField": "reference_id",
                //                        "as": "total"
                //                    }
                //                },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '2') {
                condition = [{ $match: { type: 4, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                //{$sort: {friendspire_rating: 1}},
                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '3') {
                condition = [{ $match: { type: 5, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                //{$sort: {friendspire_rating: 1}}, 
                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}

                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '4') {
                condition = [{ $match: { type: 3, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                //{$sort: {friendspire_rating: 1}},
                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '5') {
                condition = [{ $match: { type: 4, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                //{$sort: {friendspire_rating: 1}},
                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '6') {
                condition = [{ $match: { type: 5, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                //{$sort: {friendspire_rating: 1}},
                // {
                //     $group:
                //         {
                //             _id: "$reference_id",
                //             //                                    reference_id: {$first: "$_id"},
                //             count: { $sum: 1 }
                //         }
                // },
                // { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "reference_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                { $sort: { "post_detail.friendspire_rating": -1 } }
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '7') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    //                    {$project: {reviews: {$size: "$reviews"}}},
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '8') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    //                    {$project: {reviews: {$size: "$reviews"}}},
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '9') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }

                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '10') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '11') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '12') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '13') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '14') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    { $sort: { friendspire_rating: -1 } },
                    {
                        $lookup: {
                            "from": "recommendations",
                            "localField": "_id",
                            "foreignField": "reference_id",
                            "as": "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '15') {

                condition = [
                    { $match: { type: 3, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                    {
                        $group:
                        {
                            _id: "$reference_id",
                            //                                    reference_id: {$first: "$_id"},
                            count: { $sum: 1 }
                        }
                    },
                    { $sort: { count: -1 } },
                    {
                        $lookup: {
                            "from": "posts",
                            "localField": "_id",
                            "foreignField": "_id",
                            "as": "post_detail"
                        }
                    },
                    { $unwind: "$post_detail" },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "post_detail._id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "total"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$post_detail._id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "total"
                        }
                    },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},
                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '16') {
                condition = [{ $match: { type: 4, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                {
                    $group:
                    {
                        _id: "$reference_id",
                        //                                    reference_id: {$first:"$_id"},
                        count: { $sum: 1 }
                    }
                },
                { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },

                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},

                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '17') {
                condition = [{ $match: { type: 5, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: seven_day_before } } },
                {
                    $group:
                    {
                        _id: "$reference_id",
                        //                                    reference_id: {$first: "$_id"},
                        count: { $sum: 1 }
                    }
                },
                { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},

                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '18') {
                condition = [{ $match: { type: 3, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                {
                    $group:
                    {
                        _id: "$reference_id",
                        //                                    reference_id: {$first: "$_id"},
                        count: { $sum: 1 }
                    }
                },
                { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},

                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '19') {
                condition = [{ $match: { type: 4, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                {
                    $group:
                    {
                        _id: "$reference_id",
                        //                                    reference_id: {$first: "$_id"},
                        count: { $sum: 1 }
                    }
                },
                { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},

                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '20') {
                condition = [{ $match: { type: 5, is_deleted: 0, user_id: { $ne: login_id }, _id: { $nin: not_in_ids }, reference_id: { $nin: not_in_ids }, updated_at: { $lte: current_date, $gt: thirty_day_before } } },
                {
                    $group:
                    {
                        _id: "$reference_id",
                        //                                    reference_id: {$first: "$_id"},
                        count: { $sum: 1 }
                    }
                },
                { $sort: { count: -1 } },
                {
                    $lookup: {
                        "from": "posts",
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "post_detail"
                    }
                },
                { $unwind: "$post_detail" },
                //                    {
                //                        $lookup: {
                //                            "from": "recommendations",
                //                            "localField": "post_detail._id",
                //                            "foreignField": "reference_id",
                //                            "as": "total"
                //                        }
                //                    },
                {
                    "$lookup": {
                        from: "recommendations",
                        let: { ref_id: "$post_detail._id" },
                        pipeline: [
                            {
                                $match:
                                {
                                    $expr:
                                    {
                                        $and:
                                            [
                                                { $eq: ["$reference_id", "$$ref_id"] },
                                                { $in: ["$user_id", friends] },
                                                { $eq: ["$is_deleted", 0] }
                                            ]
                                    }
                                }
                            },
                        ],
                        as: "total"
                    }
                },
                    //                    {"$project": {_id: "$_id", friends: "$friends", post_detail: "$post_detail", recomendation_count: {"$size": "$total"}}},

                    //                    {$replaceRoot: {newRoot: "$post_detail"}}
                ];
                callback(null, { condition: condition, type: 2 });
            }
            if (rule == '21') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '22') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '23') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '24') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [current[1], current[0]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '25') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '26') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: seven_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '27') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance",
                            distanceMultiplier: 6371000,
                            query: { type: 1, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
            if (rule == '28') {
                projectionPost["reviews"] = { $size: "$reviews" };
                projectionPost["recomendation_count"] = { $size: "$friendreviews" };
                condition = [
                    {
                        $geoNear: {
                            near: [home[0], home[1]],
                            distanceField: "distance", distanceMultiplier: 6371000,
                            query: { type: 2, _id: { $nin: not_in_ids }, dummy: 0, rated_at: { $lte: current_date, $gt: thirty_day_before } },
                            maxDistance: config.nearbyRaduis,
                            spherical: true
                        }
                    },
                    //                    {
                    //                        $lookup: {
                    //                            "from": "recommendations",
                    //                            "localField": "_id",
                    //                            "foreignField": "reference_id",
                    //                            "as": "reviews"
                    //                        }
                    //                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "reviews"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "friendreviews"
                        }
                    },
                    { $project: projectionPost },
                    { $sort: { reviews: -1 } }
                ];
                callback(null, { condition: condition, type: 1 });
            }
        } catch (err) {
            console.log(err);
        }
    });
}

