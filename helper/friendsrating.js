const recommendations = require("../controller/recommendations.js");
const Sync = require("sync");
const util = require("util");
exports.calculate = function (friendsarray, post_id) {
    return new Promise((resolve, reject) => {
        Sync(() => {
            let aggregation_pipeline = [
                {"$match": {reference_id: post_id, user_id: {"$in": friendsarray}, is_deleted: 0}},
                {
                    $group:
                            {
                                _id: "$reference_id",
                                avgrating: {$avg: "$rating"},
                                count: {$sum: 1}
                            }
                }
            ];
            let result = recommendations.aggregate.sync(null, aggregation_pipeline);
        //    console.log("result");
        //    console.log(result);
            if (result["status"] == 1) {
                if (result["data"].length > 0) {
                    resolve([result["data"][0]["avgrating"],result["data"][0]["count"]]);
                } else {
                    resolve([0,0]);
                }
            } else {
                reject(result);
            }
        });
    });
}

