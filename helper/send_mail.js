var swig = require('swig');
var config = require('../config.js');
var nodemailer = require("nodemailer");
//create  smtp transport
var smtpTransport = nodemailer.createTransport(config.smtp);
var Sync = require('sync');

// send mail
//exports.sendMail = function (from, to, subject, content, callback) {
exports.sendMail = function (to, subject, content, callback) {
    process.nextTick(function () {
        Sync(function () {
            var tpl_swig = swig.compileFile('public/mail_page/index.html');
            var template = tpl_swig({
                content: content,
                image_logo: config.env.serviceUrl + 'images/email_logo.png',
//                fb_logo: config.env.serviceUrl + 'images/fb-logo.png',
//                insta_logo: config.env.serviceUrl + 'images/insta-logo.png',
                site_url: config.env.serviceUrl
            });
            console.log(to);
            smtpTransport.sendMail({
                from: global.admin_email, // sender address from configuration collection
                to: to, // user email_id
                subject: subject, // Subject line
                html: template
            }, function (mailError, info) {
                if (!mailError) {
                    console.log("mail_info " + info.messageId);
                } else {
                    console.log(mailError);
                }
            });
            callback(null, 1);
        });
    });
};
