var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
var Sync = require('sync');
//route for sync friends
router.put('/', function (req, res, next)
{
    if (req.body.friends)
    {
        Sync(function ()
        {
            try
            {
                var existing_friend_ids = [];
                var friends = [];
                var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {friends: 1});
                if (user_data.status == 1)
                {
                    friends = user_data.data.friends;
                    for (var j = 0; j < user_data.data.friends.length; j++)
                    {
                        existing_friend_ids.push(user_data.data.friends[j].user_id.toString());
                    }
                }
                existing_friend_ids.push(mongoose.Types.ObjectId(req.decoded._id));

                if (req.body.friends.length !== 0)
                {
                    var contacts = [];
                    for (var i = 0; i < req.body.friends.length; i++)
                    {
                        var findUser = users.findOne.sync(null, {sns_id: req.body.friends[i],type:2, status: 1, is_deleted: 0}, {});
                        if (findUser.status == 1) {
                            contacts.push(findUser.data._id);
                            if (existing_friend_ids.indexOf(findUser.data._id.toString()) < 0 && findUser.data._id != req.decoded._id) {
                                friends.push({user_id: findUser.data._id, status: 0,type:1});
                            } 
                        }
                    }
                    users.update.sync(null, {_id: req.decoded._id}, {friends: friends});
                    res.send({message: "Friends Added", status: 1});
                } else
                {
                    throw Error("Facebook Ids are required");
                }
            } catch (err) // catch errors
            {
                console.log(err)
                res.send({message: err.message, status: 0});
            }
        });
    } else {
        res.status(400).send({message: "Invalid request", status: 0});
    }
});

module.exports = router;