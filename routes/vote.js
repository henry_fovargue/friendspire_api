var express = require('express');
var router = express.Router();
var Sync = require('sync');
var vote = require('../controller/vote.js');

router.post('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.recommendationid && req.body.type) {
                var query = { recommendation_id: req.body.recommendationid, type: req.body.type, user_id: req.decoded._id };
                var update_data = { recommendation_id: req.body.recommendationid, type: req.body.type, user_id: req.decoded._id };
                var data_list = vote.findOneAndUpdate.sync(null, query, update_data);
                if (data_list.status == 1) {
                    res.status(200).send({ message: data_list.message, status: 1, data: data_list.data });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});

module.exports = router;