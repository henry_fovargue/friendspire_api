var express = require('express');
var router = express.Router();
const util = require("util");
var Sync = require('sync');
var user = require('../controller/users.js');
var cron = require('node-cron');
var notification = require('../helper/notifications.js');
var push_messages = require('../locales/en_push');

cron.schedule('1 0 * * *', () => {
// cron.schedule('* * * * * *', () => {
    Sync(function () {
        try {
            var date = new Date();
            date.setDate(date.getDate() - 1);
            console.log(date);
            var aggregation_pipeline = [
                { $match: { is_deleted: 0, status: 1, push_notification: 1, device_token: { $ne: "" } } },
                {
                    $project: {
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "friends",
                                cond: { $eq: ["$$friends.status", 1], $eq: ["$$friends.is_blocked", 0] }
                            }
                        },
                        push_notification: 1,
                        device_token: 1,
                        batch_count: 1
                    }
                },
                { $unwind: "$friends" },
                {
                    $lookup: {
                        from: "recommendations",
                        let: { user_id: "$friends.user_id" },
                        pipeline: [{
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$user_id", "$$user_id"] },
                                            { $eq: ["$is_deleted", 0] },
                                            { $gte: ["$created_at", date] }
                                        ]
                                }
                            }
                        },
                        { "$group": { _id: "$type", count: { $sum: 1 } } },
                        {
                            $project: { count: 1 },
                        }],
                        as: "friends"
                    }
                },
                { $unwind: "$friends" },
                {
                    $project: {
                        device_token: 1,
                        push_notification: 1,
                        batch_count: 1,
                        type: "$friends._id",
                        count: "$friends.count"
                    }
                },
                {
                    $group: {
                        _id: {
                            _id: "$_id",
                            type: "$type"
                        },
                        count: { $sum: "$count" },
                        push_notification: { $first: "$push_notification" },
                        device_token: { $first: "$device_token" },
                        batch_count: { $first: "$batch_count" },
                    }
                },
                {
                    $project: {
                        count: 1,
                        device_token: 1,
                        push_notification: 1,
                        batch_count: 1,
                        _id: "$_id._id",
                        type: "$_id.type"
                    }
                },
                { $sort: { "type": 1 } },
                {
                    $group: {
                        _id: "$_id",
                        data_notification: { $push: { type: "$type", count: "$count" } },
                        push_notification: { $first: "$push_notification" },
                        device_token: { $first: "$device_token" },
                        batch_count: { $first: "$batch_count" },
                    }
                },
            ];
            //console.log(util.inspect(aggregation_pipeline, { depth: null }));
            var result = user.aggregate.sync(null, aggregation_pipeline);
            //console.log(result);
            //console.log(util.inspect(result, { depth: null, hidden: true }));
            var data_array = [];
            var push_message;
            for (let i = 0; i < result.data.length; i++) {
                var recap_message = "";
                push_message = push_messages.dailyRecapMsgWithDiffCount;
                var len = result.data[i].data_notification.length - 1;
                for (let j = 0; j < result.data[i].data_notification.length; j++) {
                    var category = ["Restaurant", "Bar", "Movie", "Series", "Book"];

                    if (result.data[i].data_notification[j].type == 1) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[0];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[0] + "s";
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 2) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[1];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[1] + "s";
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 3) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[2];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[2] + "s";
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 4) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[3];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[3];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 5) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[4];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[4] + "s";
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                }
                //if (recap_message != "") {
                push_message = push_message.replace("@recap_message@", recap_message);
                var n = push_message.lastIndexOf(",");
                var final = betterReplace(push_message, ",", " and", n);
                data_array.push({ token: result.data[i].device_token, message: final, badge_count: result.data[i].batch_count });
                //}
            }
            // console.log(data_array);
            notification.cron_notification_to_all(data_array);
        } catch (err) {
            console.log(err);
        }
    });
});


router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            var date = new Date();
            date.setDate(date.getDate() - 1);
            console.log(date);
            var aggregation_pipeline = [
                { $match: { is_deleted: 0, status: 1, push_notification: 1, device_token: { $ne: "" } } },
                {
                    $project: {
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "friends",
                                cond: { $eq: ["$$friends.status", 1] }
                            }
                        },
                        push_notification: 1,
                        device_token: 1,
                    }
                },
                { $unwind: "$friends" },
                {
                    $lookup: {
                        from: "recommendations",
                        let: { user_id: "$friends.user_id" },
                        pipeline: [{
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$user_id", "$$user_id"] },
                                            { $eq: ["$is_deleted", 0] },
                                            { $gte: ["$created_at", date] }
                                        ]
                                }
                            }
                        },
                        { "$group": { _id: "$type", count: { $sum: 1 } } },
                        {
                            $project: { count: 1 },
                        }],
                        as: "friends"
                    }
                },
                { $unwind: "$friends" },
                {
                    $project: {
                        device_token: 1,
                        push_notification: 1,
                        type: "$friends._id",
                        count: "$friends.count"
                    }
                },
                {
                    $group: {
                        _id: {
                            _id: "$_id",
                            type: "$type"
                        },
                        count: { $sum: "$count" },
                        push_notification: { $first: "$push_notification" },
                        device_token: { $first: "$device_token" },
                    }
                },
                {
                    $project: {
                        count: 1,
                        device_token: 1,
                        push_notification: 1,
                        _id: "$_id._id",
                        type: "$_id.type"
                    }
                },
                { $sort: { "type": 1 } },
                {
                    $group: {
                        _id: "$_id",
                        data_notification: { $push: { type: "$type", count: "$count" } },
                        push_notification: { $first: "$push_notification" },
                        device_token: { $first: "$device_token" },
                    }
                },
            ];
            var result = user.aggregate.sync(null, aggregation_pipeline);
            //console.log("result");
            console.log(util.inspect(result, { depth: null, hidden: true }));

            var data_array = [];
            var push_message;
            for (let i = 0; i < result.data.length; i++) {
                var recap_message = "";
                push_message = push_messages.dailyRecapMsgWithDiffCount;
                var len = result.data[i].data_notification.length - 1;
                for (let j = 0; j < result.data[i].data_notification.length; j++) {
                    var category = ["Restaurant", "Bar", "Movie", "Series", "Book"];

                    if (result.data[i].data_notification[j].type == 1) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[0];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[0];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 2) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[1];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[1];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 3) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[2];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[2];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 4) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[3];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[3];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                    if (result.data[i].data_notification[j].type == 5) {
                        if (result.data[i].data_notification[j].count == 1) {
                            recap_message += "1 new " + category[4];
                        } else {
                            recap_message += result.data[i].data_notification[j].count + " new " + category[4];
                        }
                        if (j !== len) {
                            recap_message += ", ";
                        }
                    }
                }
                //if (recap_message != "") {
                push_message = push_message.replace("@recap_message@", recap_message);
                var n = push_message.lastIndexOf(",");
                var final = betterReplace(push_message, ",", " and", n);
                data_array.push({ token: result.data[i].device_token, message: final });
                //}
            }
            console.log(data_array);
            //var msg_try = "";

            // for (let i = 0; i < result.data.length; i++) {

            // }
            //console.log(msg);
            // for (let i = 0; i < msg.length; i++) {
            //     msg[i] = msg[i].replace('$countRestaurant ', "");
            //     msg[i] = msg[i].replace('$countBar ', "");
            //     msg[i] = msg[i].replace('$countMovie ', "");
            //     msg[i] = msg[i].replace('$countSeries ', "");
            //     msg[i] = msg[i].replace(', $countBook', "");
            //     var string_msg = msg[i];
            //     //var matchesCount = msg[i].split(",").length - 1;

            //     var n = string_msg.lastIndexOf(",");
            //     //console.log("matchesCount",n);
            //     var final = betterReplace(string_msg, ",", " and", n);
            //     final_msg.push(final);
            //     //console.log(finalmsg);
            // }
            // console.log(final_msg);
            // pushNotification(token_array, final_msg);
            notification.cron_notification_to_all(data_array);
            res.status(200).send({ message: result.message, status: 1 });
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});



function betterReplace(str, search, replace, from) {
    console.log("from", from);
    if (str.length > from) {
        return str.slice(0, from) + str.slice(from).replace(search, replace);
    }
    return str;
}


module.exports = router;