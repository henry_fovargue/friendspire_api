// =====================================
// Debut Infotect ======================
// Project: friendspire ===================
// page:index.js =======================
// =====================================

var express = require('express');
var router = express.Router();

// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var user = mongoose.model('user');

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sync = require('sync'); // require Sync module for Sync
var passwordHash = require('password-hash');
var passport = require('passport'); // require passport module
var LocalStrategy = require('passport-local').Strategy; //require the strategy that we want to use
var moment = require('moment');
//include file
var users = require('../controller/users.js');
var password_resets = require('../controller/password_reset.js');
var crypto = require('../helper/crypto.js');
var loginResponse = require('../helper/login_res_json.js');
var emails = require('../helper/emails.js');
const util = require("util");


//Post method for signup ======

var signup_validate = function (req, res, next) {
    //add rules for validations
    req.assert("sns_type").notEmpty().withMessage('Please enter sns type');
    req.assert("firstName").notEmpty().withMessage('Please enter First Name');
    req.assert("lastName").notEmpty().withMessage('Please enter Last Name');
    req.assert("email").isEmail().withMessage("Please enter valid email").notEmpty().withMessage("Please enter email");
    req.assert("password").notEmpty().withMessage("Safety first! Please enter your password.");
    req.assert("confirmPassword").notEmpty().withMessage("Please enter password");
    //    req.assert("gender").notEmpty().withMessage("Please select gender");
    //    req.assert("dob").notEmpty().withMessage("Please select date of birth");
    return req.validationErrors(true);
};

function generateReferralCode(firstname) {
    let uniquecode = "";
    let fromDate = "";
    if (firstname.length < 4) {
        fromDate = Date.now().valueOf().toString().slice(-(4 - firstname.length));
    }
    var num = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
    return firstname.toUpperCase().substring(0, 4) + fromDate.toString() + num;
}

function getUniqueCode(firstname) {
    let shortfirstname = firstname.length < 4 ? firstname : firstname.substring(0, 4);
    let uniqueCode = "";
    let usedReferenceCode = users.aggregate.sync(null, [{ $match: { firstName: new RegExp('^' + shortfirstname + '.*', 'i') } }, { $project: { _id: 0, reference_code: 1 } }, { $group: { _id: null, code: { $addToSet: "$reference_code" } } }]);
    console.log(usedReferenceCode);
    if (usedReferenceCode.status == 1) {
        if (usedReferenceCode["data"].length > 0) {
            do {
                uniqueCode = generateReferralCode(firstname);
                console.log(uniqueCode);
            } while (usedReferenceCode["data"][0]["code"].includes(uniqueCode));
        } else {
            uniqueCode = generateReferralCode(firstname);
        }
        return uniqueCode;
    } else {
        throw usedReferenceCode;
    }
}



router.post('/signup', function (req, res, next) {
    var errors = signup_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                if (req.body.password != req.body.confirmPassword)
                    res.status(400).send({ status: 0, message: global.messages.passwordUnmatched })
                else {
                    req.body.email = req.body.email.toLowerCase();
                    users.remove.sync(null, { email: req.body.email, status: 2 });
                    var check_email = users.findOne.sync(null, { email: req.body.email, is_deleted: 0 }, {});
                    if (check_email.status === 1) {  // user not found
                        if (check_email.data.status == 1) {
                            res.status(400).send({ status: 0, message: global.messages.emailAlreadyExists });
                        } else if (check_email.data.status == 0) {
                            res.status(400).send({ status: 0, message: global.messages.accountDisabledByAdmin });
                        }
                    } else {
                        if (req.body.password) {
                            req.body.password = passwordHash.generate(req.body.password);
                        }
                        var curentTimestamp = moment().utc().unix();
                        var userData = {
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email,
                            password: req.body.password,
                            dob: req.body.dob,
                            sns_type: req.body.sns_type,
                            fb_connected: 0,
                            status: 2,
                            type: 2,
                            reference_code: getUniqueCode(req.body.firstName),
                            last_login: curentTimestamp
                        };

                        if (req.body.code) {
                            let checkResult = users.findOne.sync(null, { reference_code: req.body.code }, { firstName: 1, reference_code: 1 });
                            if (checkResult.status == 1) {
                                userData["referral_code"] = req.body.code;
                            } else {
                                throw { message: "Invalid referral code" };
                            }
                        } else {
                            userData["referral_code"] = "";
                        }
                        var save_user = users.save.sync(null, userData);
                        if (save_user.status === 1) // user save successfully
                        {

                            //send otp verification mail to user
                            emails.signup_otp_email.sync(null, save_user.data);
                            //var response = loginResponse.login_response_json(save_user.data);
                            res.status(200).send({ message: global.messages.otpSignup, status: 2 });
                        }


                    }
                }
            } catch (err) {
                return next({ message: err.message });
            }
        });
    } else {
        res.status(400).send({ status: 0, message: "User validation failed", error: errors });
    }
}
);

//function to validate login request params
var signUp_otp_verify_validate = function (req, res, next) {
    req.assert('otp_code', 'otp_code key is  required').notEmpty();
    req.assert('email', 'Please enter email').notEmpty();
    return req.validationErrors(true);
};

router.post('/signup/otp_verify', function (req, res, next) {
    // check request is validated
    var errors = signUp_otp_verify_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                req.body.email = req.body.email.toLowerCase();
                // find user with email or phone 
                var user = users.findOne.sync(null, { email: req.body.email, is_deleted: 0 }, { status: 1, email_otp_code: 1, email_otp_expiry: 1, _id: 1, firstName: 1, lastName: 1, email: 1, is_email_verified: 1, is_deleted: 1, dob: 1, profile_pic: 1, sns_id: 1, sns_type: 1, fb_connected: 1, push_notification: 1, is_private: 1, longlat: 1, referral_code: 1, reference_code: 1, tutorial_read: 1 });
                if (user.status === 1) {
                    if (user.data.status == 0) {
                        res.status(400).send({ message: global.messages.accountDisabledByAdmin, status: 0 });
                    } else {
                        // check otp code
                        if (user.data.email_otp_code == req.body.otp_code) {
                            var current_date = new Date();
                            // call sync function to check otp code valid
                            var check_otp_expire = users.findOne.sync(null, { _id: user.data._id, email_otp_code: req.body.otp_code, email_otp_expiry: { $gte: current_date }, status: 2, is_deleted: 0 }, {});

                            if (check_otp_expire.status === 1) {
                                var data_update = { is_email_verified: 1, status: 1, email_otp_code: '' }; // coming first time
                                users.update.sync(null, { email: req.body.email, email_otp_code: req.body.otp_code }, data_update);
                                var token_obj = { _id: user.data._id, email: user.data.email, firstName: user.data.firstName, lastName: user.data.lastName }
                                // Create token with default (HMAC SHA256) Algorithm
                                var token = jwt.sign(token_obj, global.secret, {});
                                user.data.token = token;
                                delete user.data.email_otp_code;
                                delete user.data.email_otp_expiry;
                                delete user.data.status;
                                res.status(200).send({ message: "SignUp successfully.", status: 1, data: user.data });
                            } else {
                                res.status(200).send({ message: global.messages.otpExpired, status: 0 });
                            }

                        } else {
                            res.status(200).send({ message: global.messages.otpEnteredIncorrect, status: 0 });
                        }
                    }
                } else {
                    throw Error("Invalid email");
                }
            } catch (err) {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

//route for resend OTP
var resend_otp_validate = function (req, res, next) {
    req.assert('type', 'Type is  required').notEmpty();
    req.assert('email', 'Please enter email').notEmpty();
    return req.validationErrors(true);
};
router.post('/resend_otp', function (req, res, next) {
    var errors = resend_otp_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                // call sync function to check phone number already exists or not
                var userData = users.findOne.sync(null, { email: req.body.email, is_deleted: 0 }, {});
                if (userData.status != 1)
                    throw Error("User not found");
                if (userData.data.status === 1)
                    throw Error("Already verified");
                else {
                    if (req.body.type == 1)
                        emails.signup_otp_email.sync(null, userData.data);
                    else
                        emails.forgot_password_otp_email.sync(null, userData.data);
                    res.status(200).send({ message: global.messages.otpSentSuccessfully, status: 1 });
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
// Post method for login ==============

//function to validate login request params
var login_email_validate = function (req, res, next) {
    req.assert('sns_type', 'required').notEmpty();
    req.assert('email', 'Please enter email').notEmpty();
    req.assert('password', 'Please enter password').notEmpty();
    return req.validationErrors(true);
};

var login_social_validate = function (req, res, next) {
    //    req.assert('device_token', 'required').notEmpty();
    req.assert('sns_type', 'required').notEmpty();
    req.assert("sns_id").notEmpty().withMessage("Please provide sns id.");
    req.assert('firstName', 'Please enter First Name').notEmpty();
    req.assert('lastName', 'Please enter Last Name').notEmpty();
    req.assert('email', 'Please enter email').notEmpty();
    req.assert('is_email_verified', 'Please provide is_email_verified.').notEmpty();
    req.assert('is_email_verified', 'Please provide is_email_verified in integer value').isInt();
    return req.validationErrors(true);
};

router.post('/login', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.sns_type == 1) {
                var errors = login_email_validate(req, res, next);
                if (!errors) {
                    passport.authenticate('local', function (err, user1, info) {
                        try {
                            if (err) {
                                return next(err);
                            }
                            if (!user1)
                                return res.status(200).send({ message: info.message, status: 0 });
                            else {
                                if (user1.is_deleted == 1 || user1.is_deleted == true) {
                                    res.status(401).send({ message: global.messages.accountDeletedByAdmin, status: 0 });
                                } else if (user1.status == 2) {
                                    res.status(401).send({ message: global.messages.loginEmailNotVerified, status: 0 });
                                } else {
                                    if (user1.status == 0 || user1.status == false) {
                                        res.status(401).send({ message: global.messages.accountDisabledByAdmin, status: 0 });
                                    } else {
                                        var token_obj = { _id: user1._id, email: user1.email, firstName: user1.firstName, lastName: user1.lastName }
                                        var token = jwt.sign(token_obj, global.secret, {});
                                        user1 = JSON.parse(JSON.stringify(user1));
                                        user1.token = token;
                                        var response = loginResponse.login_response_json(user1);
                                        res.status(200).send({ message: global.messages.loginSuccess, status: 1, data: response });
                                    }
                                }
                            }
                        } catch (err) {
                            res.status(400).send({ message: err.message, status: 0 });
                        }
                    })(req, res, next);
                } else {
                    res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
                }
            } else if (req.body.sns_type == 2) {
                var errors = login_social_validate(req, res, next);
                if (!errors) {
                    users.remove.sync(null, { email: req.body.email, status: 2 });
                    var check_sns_id = users.findOne.sync(null, { sns_id: req.body.sns_id, status: { $ne: 2 }, is_deleted: 0, type: 2 }, {});
                    if (check_sns_id.status === 0) {
                        if (req.body.email)
                            req.body.email = req.body.email.toLowerCase();
                        var check_email = users.findOne.sync(null, { email: req.body.email, sns_id: { $ne: req.body.sns_id }, is_deleted: 0, type: 2 }, {});
                        if (check_email.status == 1) {
                            if (check_email.data.sns_type == 1) {
                                if (req.body.is_email_verified == 1) {
                                    var update_user = users.update.sync(null, { _id: check_email.data._id }, { sns_id: req.body.sns_id });
                                    var token_obj = { _id: check_email.data._id, email: check_email.data.email, firstName: check_email.data.firstName, lastName: check_email.data.lastName }
                                    var token = jwt.sign(token_obj, global.secret, {});
                                    check_email.data.token = token;
                                    var response = loginResponse.login_response_json(check_email.data);
                                    res.status(200).send({ message: "User registered successfully", status: 1, data: response });
                                } else {
                                    res.status(400).send({ status: 0, message: global.messages.emailAlreadyExists });
                                }
                            } else {
                                res.status(400).send({ status: 0, message: global.messages.emailAlreadyExists });
                            }
                        } else {
                            var curentTimestamp = moment().utc().unix();
                            var userData = {
                                firstName: req.body.firstName,
                                lastName: req.body.lastName,
                                email: req.body.email,
                                is_email_verified: req.body.is_email_verified,
                                sns_type: req.body.sns_type,
                                sns_id: req.body.sns_id,
                                fb_connected: 1,
                                status: 1,
                                type: 2,
                                reference_code: getUniqueCode(req.body.firstName),
                                last_login: curentTimestamp
                            };
                            if (req.body.profile_pic) {
                                userData.profile_pic = req.body.profile_pic;
                            }

                            if (req.body.is_email_verified == 0) {
                                userData.status = 2;
                            }
                            var save_user = users.save.sync(null, userData);
                            if (save_user.status == 1) {
                                //call login function 
                                var token_obj = { _id: save_user.data._id, email: save_user.data.email, firstName: save_user.data.firstName, lastName: save_user.data.lastName };
                                var token = jwt.sign(token_obj, global.secret, {});


                                if (req.body.is_email_verified == 0) {
                                    //send otp verification mail to user
                                    emails.signup_otp_email.sync(null, save_user.data);
                                    res.status(200).send({ message: global.messages.otpSignup, status: 2, token: token }); //, data: response
                                } else {
                                    save_user.data.token = token;
                                    var response = loginResponse.login_response_json(save_user.data);
                                    res.status(200).send({ message: "User registered successfully", status: 1, data: response });
                                }

                            } else {
                                res.status(400).send({ message: save_user.message, status: 0 });
                            }
                        }

                    } else {
                        if (check_sns_id.data.email !== req.body.email) {
                            res.status(400).send({ status: 0, message: global.messages.alreadySignUp });
                        } else {
                            var token_obj = { _id: check_sns_id.data._id, email: check_sns_id.data.email, firstName: check_sns_id.data.firstName, lastName: check_sns_id.data.lastName }
                            var token = jwt.sign(token_obj, global.secret, {});
                            check_sns_id.data.token = token;
                            var response = loginResponse.login_response_json(check_sns_id.data);
                            res.status(200).send({ message: global.messages.loginSuccess, status: 1, data: response });
                        }

                    }
                } else {
                    res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
                }
            } else {
                res.status(400).send({ message: "sns type required", status: 0 });
            }
        } catch (err) {
            return next({ message: err.message });
        }

    });
});

//Middleware for supplied strategy and their configuration
passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'password', passReqToCallback: true }, // change default Field Names 
    function (req, email, password, done) {
        try {
            email = email.toLowerCase();
            // find user with requested email
            user.findOne({ "email": email, type: 2, is_deleted: 0 }, function (err, user1) {
                if (err) {
                    return done(err);
                }
                if (!user1) {
                    return done(null, false, { message: global.messages.emailNotExists, status: 0 });
                }
                if (!passwordHash.verify(password, user1.password)) {
                    return done(null, false, { message: global.messages.wrongPassword, status: 0 }); // here second parameter user1.password is the hashed password
                }
                return done(null, user1);
            });
        } catch (err) {
            return done(err);
        }
    }));

//SerializeUser method of passport
passport.serializeUser(function (user1, done) {
    try {
        done(null, user1.id);
    } catch (err) {
        return done(null, false, { message: err.message, status: 0 });
    }
});

//DeserializeUser method of passport
passport.deserializeUser(function (id, done) {
    try {
        user.findById(id, function (err, user1) {
            done(err, user1);
        });
    } catch (err) {
        return done(null, false, { message: err.message, status: 0 });
    }
});

//Post method for forgot password
router.post('/forgot_password', function (req, res, next) {
    Sync(function () {
        try {
            if (!req.headers.lang)
                req.headers.lang = "en"
            req.body.email = req.body.email.toLowerCase();
            var userData = users.findOne.sync(null, { email: req.body.email, type: 2, status: { $ne: 2 }, is_deleted: 0 }, {});
            if (userData.status === 0)
                return res.status(200).send({ message: global.messages.emailNotExists, status: 0 });
            //Email not verified yet
            if (userData.data.is_email_verified != 1)
                return res.status(401).send({ message: global.messages.emailNotVerified, status: 401 });
            //Disabled by admin
            // if (userData.data.status == 0)
            //     return res.status(401).send({ message: global.messages.accountDisabledByAdmin, status: 401 });

            //call forget password function to send email
            var forgetOtpEmail = emails.forgot_password_otp_email.sync(null, userData);
            if (forgetOtpEmail.status === 0)
                throw Error("OTP not send.");

            //            var token = crypto.encrypt(userData.data._id.toString());
            res.send({ message: global.messages.otpResendOnForgot, status: 1, otp_code: forgetOtpEmail.data.otp_code }); //, token: token
        } catch (err) {
            return next({ message: err.message });
        }
    });
});

//Post method for forgot password

//function to validate login request params
var forgot_otp_verify_validate = function (req, res, next) {
    req.assert('otp_code', 'otp_code key is  required').notEmpty();
    req.assert('email', 'Please enter email').notEmpty();
    return req.validationErrors(true);
};

router.post('/forgot_password/otp_verify', function (req, res, next) {
    // check request is validated
    var errors = forgot_otp_verify_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                req.body.email = req.body.email.toLowerCase();
                // find user with email or phone 
                var user = users.findOne.sync(null, { email: req.body.email, status: { $ne: 2 }, is_deleted: 0 }, {});
                if (user.status === 1) {
                    // if (user.data.status == 0) {
                    //     res.status(400).send({ message: global.messages.accountDisabledByAdmin, status: 0 });
                    // } else {
                    // check otp code
                    if (user.data.forgot_otp_code == req.body.otp_code) {
                        var current_date = new Date();
                        // call sync function to check otp code valid
                        var check_otp_expire = users.findOne.sync(null, { _id: user.data._id, forgot_otp_code: req.body.otp_code, forgot_otp_expiry: { $gte: current_date }, status: { $ne: 2 }, is_deleted: 0 }, {});
                        if (check_otp_expire.status === 1) {
                            // encrypt user id to use at reset password
                            var reset_token = crypto.encrypt(user.data._id.toString());
                            // remove all record with this user from password reset table
                            password_resets.remove.sync(null, { email: user.data.email });
                            // remove record with this user in password reset table
                            var password_resets_data = password_resets.save.sync(null, { email: req.body.email, reset_token: reset_token });
                            if (password_resets_data.status === 1) {
                                res.status(200).send({ message: "Verification code match successfully.", status: 1, reset_token: reset_token, email: req.body.email });
                            } else {
                                res.status(200).send({ message: "Verification code not verified please try again", status: 0 });
                            }
                        } else {
                            res.status(200).send({ message: global.messages.otpExpired, status: 0 });
                        }

                    } else {
                        res.status(200).send({ message: global.messages.otpEnteredIncorrect, status: 0 });
                    }
                    //}
                } else {
                    throw Error("Invalid email");
                }
            } catch (err) {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

// Post request for reset-password

var reset_password_validate = function (req, res, next) {
    req.assert('email', 'Please enter email').notEmpty();
    req.assert('password', 'Please enter password').notEmpty();
    req.assert('reset_token', 'reset_token key is required').notEmpty();
    return req.validationErrors(true);
};

router.put('/reset_password', function (req, res, next) {
    // check request is validated
    var errors = reset_password_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                req.body.email = req.body.email.toLowerCase();
                // find record corresponding to reset token and email
                var password_resets_find = password_resets.findOne.sync(null, { email: req.body.email, reset_token: req.body.reset_token });
                if (password_resets_find.status === 1) {
                    // decrypt user id
                    var user_id = crypto.decrypt(password_resets_find.data.reset_token.toString());
                    // find user by decrypted id
                    var user = users.findOne.sync(null, { _id: user_id, email: req.body.email, status: { $ne: 2 }, is_deleted: 0 }, {});
                    if (user.status === 1) {
                        // if (user.data.status == 0) {
                        //     res.status(200).send({ message: global.messages.accountDisabledByAdmin, status: 0 });
                        // } else {
                        req.body.password = passwordHash.generate(req.body.password); //to store generated hash pasword in req.body.password
                        // update password of user
                        var user_update = users.update.sync(null, { _id: user.data._id }, { password: req.body.password });
                        if (user_update.status === 1) {
                            // remove this record from password reset table
                            password_resets.remove.sync(null, { email: req.body.email, reset_token: req.body.reset_token });
                            res.status(200).send({ message: global.messages.passwordResetSuccess, status: 1 });
                        } else {
                            res.status(400).send(user_update);
                        }
                        //}
                    } else {
                        res.status(400).send({ message: global.messages.emailErrorMessage, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: "Session expired", status: 0 });
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});


var validateCode = function (req, res, next) {
    req.checkBody('code', 'Please enter code').notEmpty();
    return req.validationErrors(true);
};
router.post("/validatecode", function (req, res, next) {
    var errors = validateCode(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                let referral_code = req.body.code;
                let checkResult = users.findOne.sync(null, { reference_code: referral_code }, { firstName: 1, reference_code: 1 });
                if (checkResult.status == 1) {
                    console.log(checkResult);
                    res.status(200).send({ message: "valid referral code", status: 1 });
                } else {
                    res.status(200).send({ message: "invalid referral code", status: 0 });
                }

            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
module.exports = router;
