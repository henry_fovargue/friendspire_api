var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
var Sync = require('sync');
var notification = require('../helper/notifications.js');
const util = require("util");

router.get('/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            var _skip = (req.params.page_no - 1) * global.pagination_limit;
            var query = [{ $match: { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 } },
            {
                $project: {
                    friends: {
                        $filter: {
                            input: "$friends",
                            as: "friends",
                            //cond: { $eq: ["$$friends.status", 1] }
                            //cond: { $eq: ["$$friends.is_blocked", 0] }
                            cond: { $and: [{ $ne: ["$$friends.status", 0] }, { $eq: ["$$friends.is_blocked", 0] }] }
                        }
                    }
                }
            },
            { "$unwind": "$friends" },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "friends.user_id",
                    "foreignField": "_id",
                    "as": "user_detail"
                }
            },
            { "$unwind": "$user_detail" },
            { $match: { 'user_detail.is_deleted': 0, 'user_detail.status': 1 } },
            {
                $project: {
                    _id: "$user_detail._id",
                    firstName: "$user_detail.firstName",
                    lastName: "$user_detail.lastName",
                    profile_pic: "$user_detail.profile_pic",
                    status: "$friends.status",
                    blocked_user: "$user_detail.blocked_user"
                }
            },
            { $match: { blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } } },
            {
                '$project':
                {
                    _id: '$_id',
                    firstName: '$firstName',
                    lastName: '$lastName',
                    profile_pic: '$profile_pic',
                    status: '$status'
                }
            },
            { $sort: { firstName: 1, _id: 1 } },
            { $skip: _skip },
            { $limit: global.pagination_limit }

            ];
            // console.log(util.inspect(query, { depth: null, hidden: true }));
            var following_list = users.aggregate.sync(null, query);
            if (following_list.status == 1) {
                // console.log(util.inspect(following_list.data,{depth:null,hidden:true}));
                res.status(200).send({ message: "Success", status: 1, data: following_list.data });
            } else {
                res.status(400).send({ message: following_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});

router.put('/', function (req, res, next) {
    if (req.body.friend_id) {
        Sync(function () {
            try {
                var update_json = {};
                var user_data = users.findOne.sync(null, { _id: req.body.friend_id }, { is_privare: 1, friends: 1, push_notification: 1, device_token: 1 });
                if (user_data.status == 1) {
                    if (user_data.data.is_private == 1) {

                        if (user_data.data.push_notification == 1 && user_data.data.device_token != "") {
                            var message = global.push_messages.sendFollowRequest;
                            var data = {
                                title: 'Friendspire',
                                body: message,
                                type: 2
                            };
                            notification.send_notification(user_data.data.user_id.device_token, data);

                        }
                        update_json = { 'friends.$.status': 2 };
                    } else {
                        update_json = { 'friends.$.status': 1 };
                    }
                    var update_user = users.update.sync(null, { _id: req.decoded._id, 'friends.user_id': req.body.friend_id }, { 'friends.$.status': 1 });
                    if (update_user.status == 1)
                        res.status(200).send({ message: "Success", status: 1 });
                    else
                        res.status(400).send({ message: update_user.message, status: 0 });
                } else {
                    res.status(400).send({ message: user_data.message, status: 0 });
                }
            } catch (err) // catch errors
            {
                res.send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Invalid request", status: 0 });
    }
});

module.exports = router;