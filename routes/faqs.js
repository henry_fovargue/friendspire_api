var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var cms_page = mongoose.model('cms_page');

/* Get method for faqs */
router.get('/', function (req, res, next) {
    try {
        // find cms-page faqs
        cms_page.find({ type: 2, status: 1 }, { _id: 1, heading: 1, description: 1 }).sort({ updated_at: -1 }).exec(function (pageError, pageData) {
            if (!pageError) {
                res.status(200).send({ message: "Success", status: 1, data: pageData });
            } else {
                res.status(400).send({ message: pageError.message, status: 0 });
            }
        });
    } catch (err) {
        console.log(err);
        res.status(400).send({ message: "Error", status: 0 });
    }

});
module.exports = router;
