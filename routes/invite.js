var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('../config');

router.get('/', function (req, res, next) {
    try {
        var data = {
            "dynamicLinkInfo": {
                "dynamicLinkDomain": config.deeplink.dynamicLinkDomain,
                "link": config.url_redirect + "invalid_url",
                //"link": config.digital_ocean + "invalid_url",
                //"link": "https://itunes.apple.com/ph/app/friendspire/id1435691931?mt=8&ign-mpt=uo%3D2",
                "androidInfo": {
                    "androidPackageName": config.deeplink.androidPackageName,
                    "androidFallbackLink": config.deeplink.androidFallbackLink
                },
                "iosInfo": {
                    "iosBundleId": config.deeplink.iosBundleId,
                    "iosFallbackLink": config.deeplink.iosFallbackLink
                }
            },
            "suffix": {
                "option": "SHORT"
            }
        };
        var dataString = JSON.stringify(data);
        var options = {
            url: "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + config.deeplink.API_Key,
            method: 'POST',
            body: dataString
        };

        function callback(error, response, body) {
            body = JSON.parse(body);
            console.log(body);
            if (!error && response.statusCode === 200) {
                res.status(200).send({ status: 1, message: "success", link: body.shortLink });
            } else {
                res.status(400).send({ status: 0, message: body.error });
            }
        }
        request(options, callback);
    } catch (err) {
        return next(err);
    }
});

module.exports = router;