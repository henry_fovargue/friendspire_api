var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
var bookmarks = require('../controller/bookmarks.js');
var recommend = require('../controller/recommendations.js');
var notification = require('../helper/notifications.js');
var notification_controller = require('../controller/notification.js');
const util = require("util");
var Sync = require('sync');
router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            var pending_count = users.count.sync(null, { _id: { $nin: req.decoded.blocked_user }, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id), status: 2, is_blocked: 0 } }, is_deleted: 0, status: 1 });
            var follow_count = users.count.sync(null, { _id: { $nin: req.decoded.blocked_user }, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id), status: 1, is_blocked: 0 } }, is_deleted: 0, status: 1 });
            var following_count_query = [{ $match: { _id: mongoose.Types.ObjectId(req.decoded._id) } },
            {
                $project: {
                    friends: {
                        $filter: {
                            input: "$friends",
                            as: "friends",
                            //cond: { $eq: ["$$friends.status", 1] }
                            cond: { $and: [{ $eq: ["$$friends.status", 1] }, { $eq: ["$$friends.is_blocked", 0] }] }
                        }
                    }
                }
            }
            ];
            var following_count = users.aggregate.sync(null, following_count_query);
            // console.log("following_count");
            // console.log(util.inspect(following_count, { depth: null }));
            if (following_count.data[0].friends.length == 0) {
                res.status(200).send({ message: "Success", status: 1, data: { pending: pending_count.data, followers: follow_count.data, following: following_count.data[0].friends.length } });
            } else {
                var new_following_count_query = [{ $match: { _id: mongoose.Types.ObjectId(req.decoded._id), status: 1, is_deleted: 0 } },
                {
                    $project: {
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "friends",
                                //cond: { $eq: ["$$friends.status", 1] }
                                cond: { $and: [{ $eq: ["$$friends.status", 1] }, { $eq: ["$$friends.is_blocked", 0] }] }
                            }
                        }
                    }
                },
                { "$unwind": "$friends" },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "friends.user_id",
                        "foreignField": "_id",
                        "as": "user_detail"
                    }
                },
                { "$unwind": "$user_detail" },
                { $match: { "user_detail.blocked_user": { $nin: [mongoose.Types.ObjectId(req.decoded._id)] } } },
                {
                    $group: {
                        _id: "$_id", followings: { $sum: 1 }
                    }
                }
                ];
                // console.log("new_following_count_query");
                // console.log(util.inspect(new_following_count_query, { depth: null }));
                var new_following_count = users.aggregate.sync(null, new_following_count_query);
                var following_count_send;
                // console.log(new_following_count);
                if(new_following_count.data.length == 0){
                    following_count_send = 0;
                } else{
                    following_count_send = new_following_count.data[0].followings;
                }
                res.status(200).send({ message: "Success", status: 1, data: { pending: pending_count.data, followers: follow_count.data, following: following_count_send } });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/pending/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            var _skip = (req.params.page_no - 1) * global.pagination_limit;
            var pending_list = users.find_pagination.sync(null, { blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id), status: 2, is_blocked: 0 } } }, { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 }, _skip, global.pagination_limit);
            if (pending_list.status == 1) {
                res.status(200).send({ message: "Success", status: 1, data: pending_list.data });
            } else {
                res.status(400).send({ message: pending_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/search/:text', function (req, res, next) {
    Sync(function () {
        try {
            //console.log(req.decoded.friends);
            //console.log(req.decoded.friends.length);
            // var blockeduser_array = [];
            // for (var i = 0; i < req.decoded.friends.length; i++) {
            //     if (req.decoded.friends[i].is_blocked == 1) {
            //         blockeduser_array.push(req.decoded.friends[i].user_id);
            //     }
            // }
            // console.log(blockeduser_array);
            // console.log(blockeduser_array.length);
            search_firstName = req.params.text.split(' ')[0];
            search_lastName = req.params.text.split(' ')[1];
            //var condition = { is_deleted: 0, status: 1, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, _id: { $nin: req.decoded.blocked_user } };
            var condition = { is_deleted: 0, status: 1 };
            //condition.firstName = new RegExp('^' + search_firstName + '.*', 'i');
            condition.$or = [{ firstName: new RegExp('^' + search_firstName + '.*', 'i') },
            { lastName: new RegExp('^' + search_firstName + '.*', 'i') }];
            if (search_lastName != '' && search_lastName != undefined) {
                condition.firstName = new RegExp('^' + search_firstName, 'i');
                condition.lastName = new RegExp(search_lastName + '.*', 'i');
            }

            var aggregate_condition = [
                { $match: condition },
                {
                    $lookup: {
                        from: "users",
                        localField: "_id",
                        foreignField: "friends.user_id",
                        as: "followers"
                    }
                },
                { $project: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, followers: { $size: "$followers" } } },
                { $sort: { followers: -1 } }
            ];
            var users_list = users.aggregate.sync(null, aggregate_condition);
            //console.log(users_list);
            //            var users_list = users.find.sync(null, condition, {_id: 1, firstName: 1, lastName: 1});
            if (users_list.status == 1) {
                res.status(200).send({ message: "Success", status: 1, data: users_list.data });
            } else {
                res.status(400).send({ message: users_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.post('/search/:text', function (req, res, next) {
    Sync(function () {
        try {
            var blockeduser_array = [];
            for (var i = 0; i < req.decoded.friends.length; i++) {
                if (req.decoded.friends[i].is_blocked == 1) {
                    blockeduser_array.push(req.decoded.friends[i].user_id);
                }
            }
            search_firstName = req.params.text.split(' ')[0];
            search_lastName = req.params.text.split(' ')[1];
            //var condition = { is_deleted: 0, status: 1, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, _id: { $nin: req.decoded.blocked_user } };
            var condition = { is_deleted: 0, status: 1 };
            //condition.firstName = new RegExp('^' + search_firstName + '.*', 'i');
            condition.$or = [{ firstName: new RegExp('^' + search_firstName + '.*', 'i') },
            { lastName: new RegExp('^' + search_firstName + '.*', 'i') }];

            if (search_lastName != '' && search_lastName != undefined) {
                condition.firstName = new RegExp('^' + search_firstName, 'i');
                condition.lastName = new RegExp(search_lastName + '.*', 'i');
            }

            var query = [
                { $match: condition },
                { $project: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 } },
                //                {$lookup: {from: "users",
                //                        localField: "_id",
                //                        foreignField: "friends.user_id",
                //                        as: "followers"}},
                //                {$project: {_id: 1, firstName: 1, lastName: 1, profile_pic: 1, followers: {$size: "$followers"}}}
            ];
            var users_list = users.aggregate.sync(null, query);
            if (users_list.status == 1) {
                var follower_count = users.count.sync(null, { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(users_list.data[0]._id), status: 1, is_blocked: 0 } } });
                if (follower_count.status == 1)
                    users_list.data[0].followers = follower_count.data;
                else
                    users_list.data[0].followers = 0;
                res.status(200).send({ message: "Success", status: 1, data: users_list.data });
            } else {
                res.status(400).send({ message: users_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.put('/accept', function (req, res, next) {

    Sync(function () {
        try {
            if (req.body.friend_id) {
                var find_data = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0, status: 1, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } }, {});
                if (find_data.status == 1) {
                    //console.log("------------------------find_data----------------------");
                    //console.log(find_data);
                    var accept_request = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), 'friends.user_id': mongoose.Types.ObjectId(req.decoded._id) }, { 'friends.$.status': 1 });
                    //console.log("------------------------accept_request----------------------");
                    //console.log(accept_request);
                    if (accept_request.status == 1) {
                        var push_message, database_message;
                        push_message = global.push_messages.acceptPushRequest;
                        push_message = push_message.replace('$firstName', req.decoded.firstName);
                        push_message = push_message.replace('$lastName', req.decoded.lastName);

                        database_message = global.push_messages.acceptRequest;
                        database_message = database_message.replace('$firstName', req.decoded.firstName);
                        database_message = database_message.replace('$lastName', req.decoded.lastName);

                        if (find_data.data.push_notification == 1 && find_data.data.device_token != "") {
                            var data = {
                                title: 'Friendspire',
                                body: push_message,
                                type: 1,
                                user_id: req.decoded._id
                            };
                            notification.send_notification(find_data.data.device_token, data, find_data.data.batch_count+1);
                        }
                        var data = {
                            reference_id: req.body.friend_id,
                            //reference_user_id: req.decoded._id,
                            from: req.decoded._id,
                            to: req.body.friend_id,
                            type: 1,
                            message: database_message
                        };
                        var notification_data = notification_controller.create.sync(null, data);
                        //                        console.log(notification_data);
                        if (notification_data.status == 1) {
                            var batch_count_data = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0, status: 1 }, { $inc: { batch_count: 1 } });
                            //                            console.log(batch_count_data);
                            if (batch_count_data.status == 1) {
                                res.status(200).send({ message: "Success", status: 1 });
                            } else {
                                res.status(400).send({ message: batch_count_data.message, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: notification_data.message, status: 0 });
                        }
                        //res.status(200).send({message: "Success", status: 1});
                    } else {
                        res.status(400).send({ message: accept_request.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: global.messages.blockOrDelete, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.put('/reject', function (req, res, next) {

    Sync(function () {
        try {
            if (req.body.friend_id) {
                var find_data = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } }, {});
                if (find_data.status == 1) {
                    var reject_request = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), 'friends.user_id': mongoose.Types.ObjectId(req.decoded._id) }, { 'friends.$.status': 0 });
                    if (reject_request.status == 1) {
                        var push_message, database_message;
                        push_message = global.push_messages.declinePushRequest;
                        push_message = push_message.replace('$firstName', req.decoded.firstName);
                        push_message = push_message.replace('$lastName', req.decoded.lastName);

                        database_message = global.push_messages.declineRequest;
                        database_message = database_message.replace('$firstName', req.decoded.firstName);
                        database_message = database_message.replace('$lastName', req.decoded.lastName);

                        if (find_data.data.push_notification == 1 && find_data.data.device_token != "") {
                            var data = {
                                title: 'Friendspire',
                                body: push_message,
                                type: 2,
                                user_id: req.decoded._id
                            };
                            notification.send_notification(find_data.data.device_token, data, find_data.data.batch_count+1);
                        }
                        var data = {
                            reference_id: req.body.friend_id,
                            //reference_user_id: req.decoded._id,
                            from: req.decoded._id,
                            to: req.body.friend_id,
                            type: 2,
                            message: database_message
                        };
                        var notification_data = notification_controller.create.sync(null, data);
                        //console.log(notification_data);
                        if (notification_data.status == 1) {
                            var batch_count_data = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0 }, { $inc: { batch_count: 1 } });
                            //                            console.log(batch_count_data);
                            if (batch_count_data.status == 1) {
                                res.status(200).send({ message: "Success", status: 1 });
                            } else {
                                res.status(400).send({ message: batch_count_data.message, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: notification_data.message, status: 0 });
                        }
                        //res.status(200).send({message: "Success", status: 1});
                    } else {
                        res.status(400).send({ message: reject_request.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: find_data.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.put('/unfollow', function (req, res, next) {

    Sync(function () {
        try {
            if (req.body.friend_id) {
                var unfollow_request = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.decoded._id), 'friends.user_id': mongoose.Types.ObjectId(req.body.friend_id) }, { 'friends.$.status': 0 });
                if (unfollow_request.status == 1) {
                    res.status(200).send({ message: "Success", status: 1 });
                } else {
                    res.status(400).send({ message: unfollow_request.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});

router.get('/counts/:id', function (req, res, next) {
    Sync(function () {
        try {
            var blocked_status_array = req.decoded.blocked_user;
            //console.log("blocked_status_array", blocked_status_array);
            var users_data;
            var blocked_checked = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.params.id), is_deleted: 0 }, { firstName: 1, lastName: 1, profile_pic: 1, status: 1, blocked_user: 1 });
            //console.log(blocked_checked);
            if (blocked_checked.data.status == 0) {
                return res.status(400).send({ message: global.messages.DisabledAccount, status: 0 });
            }
            for (let i = 0; i < blocked_checked.data.blocked_user.length; i++) {
                blocked_status_array.push(blocked_checked.data.blocked_user[i]);
            }
            var array_id_tocheck = [mongoose.Types.ObjectId(req.params.id), mongoose.Types.ObjectId(req.decoded._id)];

            //console.log("blocked_status_array--------------", blocked_status_array);
            var found = array_id_tocheck.some(function (v) {
                return blocked_status_array.indexOf(v) != -1;
            });
            // console.log("found", found);
            if (found == true) {
                var data_send = { followers: 0, followings: 0, is_blocked: 1 };
                res.status(400).send({ message: "This user profile is private", status: 1, data: data_send });
            } else {
                var query_case = [{
                    $match: {
                        _id: mongoose.Types.ObjectId(req.params.id), is_deleted: 0,
                        blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }
                    }
                },
                {
                    $project: {
                        status: 1,
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "items",
                                //cond: { $eq: ["$$items.status", 1] }
                                cond: { $and: [{ $eq: ["$$items.status", 1] }, { $eq: ["$$items.is_blocked", 0], }] }
                            }
                        }
                    }
                },
                { $project: { status: 1, friends: 1 } }
                ];

                var query_case2 = [{ "$unwind": { path: "$friends", "preserveNullAndEmptyArrays": true } },
                { $match: { "friends.user_id": { $nin: req.decoded.blocked_user } } },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "friends.user_id",
                        "foreignField": "_id",
                        "as": "user_detail"
                    }
                },
                { "$unwind": { path: "$user_detail", "preserveNullAndEmptyArrays": true } },
                {
                    $match: {
                        "user_detail.blocked_user": { $nin: [mongoose.Types.ObjectId(req.decoded._id), mongoose.Types.ObjectId(req.params.id)] },
                        'user_detail.is_deleted': 0, 'user_detail.status': 1
                    }
                }
                ];

                var query_case3 = [{
                    $group: {
                        _id: "$_id", following: { $sum: 1 },
                    }
                }];
                // console.log("---------------------query_case------------------");
                // console.log(util.inspect(query_case, { depth: null, hidden: true }));

                var try_data = users.aggregate.sync(null, query_case);
                var query1 = query_case.concat(query_case2);
                // console.log("---------------------query1------------------");
                // console.log(util.inspect(query1, { depth: null, hidden: true }));

                var case_data = users.aggregate.sync(null, query1);
                // console.log("---------------------try_data------------------");
                // console.log(util.inspect(try_data, { depth: null, hidden: true }));
                if (try_data.data[0].status == 1) {
                    if (try_data.data[0].friends.length == 0) {
                        try_data.data[0].following = 0;
                        users_data = try_data;
                    } else if (case_data.data.length == 0) {
                        try_data.data[0].following = 0;
                        users_data = try_data;
                    } else {
                        var query2 = query1.concat(query_case3);
                        // console.log("---------------------query2------------------");
                        // console.log(util.inspect(query2, { depth: null, hidden: true }));
                        users_data = users.aggregate.sync(null, query2);
                    }

                    if (users_data.status == 1) {
                        if (users_data.data.length == 0) {
                            res.status(400).send({ message: "User is currently blocked", status: 0 });
                        }

                        var follower_count = users.count.sync(null, { _id: { $nin: req.decoded.blocked_user }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.params.id), status: 1, is_blocked: 0 } }, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, is_deleted: 0, status: 1 });
                        
                        if (follower_count.status == 1)
                            users_data.data[0].followers = follower_count.data;
                        else
                            users_data.data[0].followers = 0;

                        res.status(200).send({ message: "Success", status: 1, data: users_data.data[0] });

                    } else {
                        res.status(400).send({ message: users_data.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: global.messages.DisabledAccount, status: 0 });
                }
            }

        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/user_profile/:id', function (req, res, next) {
    Sync(function () {
        try {
            var blocked_status_array = req.decoded.blocked_user;
            //            console.log("blocked_status_array", blocked_status_array);
            var users_data;
            var blocked_checked = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.params.id), is_deleted: 0 }, { firstName: 1, lastName: 1, profile_pic: 1, status: 1, blocked_user: 1 });
            //            console.log(blocked_checked);
            if (blocked_checked.data.status == 0) {
                return res.status(400).send({ message: global.messages.DisabledAccount, status: 0 });
            }
            for (let i = 0; i < blocked_checked.data.blocked_user.length; i++) {
                blocked_status_array.push(blocked_checked.data.blocked_user[i]);
            }
            var array_id_tocheck = [mongoose.Types.ObjectId(req.params.id), mongoose.Types.ObjectId(req.decoded._id)];

            //            console.log("blocked_status_array--------------", blocked_status_array);
            var found = array_id_tocheck.some(function (v) {
                return blocked_status_array.indexOf(v) != -1;
            });
            console.log("found", found);
            if (found == true) {
                var data_send = { followers: 0, followings: 0, is_blocked: 1, firstName: blocked_checked.data.firstName, lastName: blocked_checked.data.lastName, profile_pic: blocked_checked.data.profile_pic };
                res.status(400).send({ message: "This user profile is private", status: 1, data: data_send });
            } else {
                var query_test = [{
                    $match: {
                        _id: mongoose.Types.ObjectId(req.params.id), is_deleted: 0,
                        blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }
                    }
                },
                {
                    $project: {
                        _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, status: 1,
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "items",
                                //cond: { $eq: ["$$items.status", 1] }
                                cond: { $and: [{ $eq: ["$$items.status", 1] }, { $eq: ["$$items.is_blocked", 0], }] }
                            }
                        }
                    }
                },
                { $project: { _id: 1, firstName: 1, lastName: 1, status: 1, profile_pic: 1, is_private: 1, friends: 1 } }
                ];


                var query_case = [{
                    $match: {
                        _id: mongoose.Types.ObjectId(req.params.id), is_deleted: 0,
                        blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }
                    }
                },
                {
                    $project: {
                        _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, status: 1,
                        friends: {
                            $filter: {
                                input: "$friends",
                                as: "items",
                                //cond: { $eq: ["$$items.status", 1] }
                                cond: { $and: [{ $eq: ["$$items.status", 1] }, { $eq: ["$$items.is_blocked", 0], }] }
                            }
                        }
                    }
                },
                { $project: { _id: 1, firstName: 1, lastName: 1, status: 1, profile_pic: 1, is_private: 1, friends: 1 } },
                { "$unwind": { path: "$friends", "preserveNullAndEmptyArrays": true } },
                { $match: { "friends.user_id": { $nin: req.decoded.blocked_user } } },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "friends.user_id",
                        "foreignField": "_id",
                        "as": "user_detail"
                    }
                },
                { "$unwind": { path: "$user_detail", "preserveNullAndEmptyArrays": true } },
                { $match: { "user_detail.blocked_user": { $nin: [mongoose.Types.ObjectId(req.decoded._id), mongoose.Types.ObjectId(req.params.id)] } } }
                ];

                var query = [
                    // { $match: { $and: [{ _id: mongoose.Types.ObjectId(req.params.id) }, { _id: { $nin: req.decoded.blocked_user } }], status: 1, is_deleted: 0, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } } },
                    // {
                    //     $project: {
                    //         _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, blocked_user: 1,
                    //         friends: {
                    //             $filter: {
                    //                 input: "$friends",
                    //                 as: "items",
                    //                 //cond: { $eq: ["$$items.status", 1] }
                    //                 cond: { $and: [{ $eq: ["$$items.status", 1] }, { $eq: ["$$items.is_blocked", 0] }] }
                    //             }
                    //         }
                    //     }
                    // },
                    // { $match: { "friends.user_id": { $nin: req.decoded.blocked_user } } },
                    // { $project: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, blocked_user: 1, followings: { $size: "$friends" } } }

                    {
                        $match: {
                            _id: mongoose.Types.ObjectId(req.params.id),
                            status: 1, is_deleted: 0,
                            blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }
                        }
                    },
                    {
                        $project: {
                            _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, blocked_user: 1,
                            friends: {
                                $filter: {
                                    input: "$friends",
                                    as: "items",
                                    //cond: { $eq: ["$$items.status", 1] }
                                    cond: { $and: [{ $eq: ["$$items.status", 1] }, { $eq: ["$$items.is_blocked", 0] }] }
                                }
                            }
                        }
                    },
                    { "$unwind": { path: "$friends", "preserveNullAndEmptyArrays": true } },
                    { $match: { "friends.user_id": { $nin: req.decoded.blocked_user } } },
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "friends.user_id",
                            "foreignField": "_id",
                            "as": "user_detail"
                        }
                    },
                    { "$unwind": { path: "$user_detail", "preserveNullAndEmptyArrays": true } },
                    {
                        $match: {
                            "user_detail.blocked_user": { $nin: [mongoose.Types.ObjectId(req.decoded._id), mongoose.Types.ObjectId(req.params.id)] },
                            'user_detail.is_deleted': 0, 'user_detail.status': 1
                        }
                    },
                    {
                        $group: {
                            _id: "$_id", followings: { $sum: 1 },
                            firstName: { $first: "$firstName" },
                            lastName: { $first: "$lastName" },
                            profile_pic: { $first: "$profile_pic" },
                            is_private: { $first: "$is_private" }
                        }
                    }
                ];
                // console.log("---------------------query_test------------------");
                // console.log(util.inspect(query_test, { depth: null, hidden: true }));
                var try_data = users.aggregate.sync(null, query_test);
                var case_data = users.aggregate.sync(null, query_case);
                if (try_data.status == 1) {
                    //console.log(try_data);
                    // console.log("---------------------try_data------------------");
                    // console.log(util.inspect(try_data, { depth: null, hidden: true }));
                    if (try_data.data[0].status == 1) {
                        if (try_data.data[0].friends.length == 0) {
                            try_data.data[0].followings = 0;
                            users_data = try_data;
                        } else if (case_data.data.length == 0) {
                            try_data.data[0].followings = 0;
                            users_data = try_data;
                        } else {
                            // console.log("---------------------query------------------");
                            // console.log(util.inspect(query, { depth: null, hidden: true }));
                            users_data = users.aggregate.sync(null, query);
                        }
                        // console.log("---------------------users_data------------------");
                        // console.log(util.inspect(users_data, { depth: null, hidden: true }));
                        //console.log(users_data);
                        if (users_data.status == 1) {
                            if (users_data.data.length == 0) {
                                res.status(400).send({ message: "User is currently blocked", status: 0 });
                            }
                            var follower_count = users.count.sync(null, { _id: { $nin: req.decoded.blocked_user }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.params.id), status: 1, is_blocked: 0 } }, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, is_deleted: 0, status: 1 });
                            if (follower_count.status == 1)
                                users_data.data[0].followers = follower_count.data;
                            else
                                users_data.data[0].followers = 0;
                            var aggregate_queryrecommend = [
                                {
                                    $match:
                                    {
                                        user_id: mongoose.Types.ObjectId(req.params.id),
                                        is_deleted: 0
                                    }
                                },
                                {
                                    $group: { _id: "$type", count: { $sum: 1 } }
                                }
                            ]
                            var aggregate_querybookmarks = [
                                {
                                    $match:
                                    {
                                        user_id: mongoose.Types.ObjectId(req.params.id),
                                        status: 1
                                    }
                                },
                                {
                                    $group: { _id: "$type", count: { $sum: 1 } }
                                }
                            ]
                            var recommend_data = recommend.aggregate.sync(null, aggregate_queryrecommend);
                            var bookmarks_data = bookmarks.aggregate.sync(null, aggregate_querybookmarks);
                            //                console.log(recommend_data);
                            //                console.log(bookmarks_data);
                            var total_recommend = total_bookmarks = 0;
                            var recommend_movie = recommend_series = recommend_bar = recommend_book = recommend_res = 0;
                            var bookmarks_movie = bookmarks_series = bookmarks_bar = bookmarks_book = bookmarks_res = 0;
                            for (var i = 0; i < recommend_data.data.length; i++) {
                                total_recommend = total_recommend + recommend_data.data[i].count;
                                //console.log(recommend_data.data[i]._id);
                                if (recommend_data.data[i]._id == 1) {
                                    recommend_res = recommend_data.data[i].count;
                                }
                                if (recommend_data.data[i]._id == 2) {
                                    recommend_bar = recommend_data.data[i].count;
                                }
                                if (recommend_data.data[i]._id == 3) {
                                    recommend_movie = recommend_data.data[i].count;
                                }
                                if (recommend_data.data[i]._id == 4) {
                                    recommend_series = recommend_data.data[i].count;
                                }
                                if (recommend_data.data[i]._id == 5) {
                                    recommend_book = recommend_data.data[i].count;
                                }
                            }
                            for (var i = 0; i < bookmarks_data.data.length; i++) {
                                total_bookmarks = total_bookmarks + bookmarks_data.data[i].count;
                                if (bookmarks_data.data[i]._id == 1) {
                                    bookmarks_res = bookmarks_data.data[i].count;
                                }
                                if (bookmarks_data.data[i]._id == 2) {
                                    bookmarks_bar = bookmarks_data.data[i].count;
                                }
                                if (bookmarks_data.data[i]._id == 3) {
                                    bookmarks_movie = bookmarks_data.data[i].count;
                                }
                                if (bookmarks_data.data[i]._id == 4) {
                                    bookmarks_series = bookmarks_data.data[i].count;
                                }
                                if (bookmarks_data.data[i]._id == 5) {
                                    bookmarks_book = bookmarks_data.data[i].count;
                                }
                            }
                            //console.log(find_follow_status(req.params.id, req.decoded._id));
                            users_data.data[0].is_blocked = 0;

                            if (req.params.id == req.decoded._id) {
                                users_data.data[0].profile_data = {
                                    all: [total_recommend, total_bookmarks],
                                    movie: [recommend_movie, bookmarks_movie],
                                    tv: [recommend_series, bookmarks_series],
                                    restaurant: [recommend_res, bookmarks_res],
                                    bar: [recommend_bar, bookmarks_bar],
                                    books: [recommend_book, bookmarks_book]
                                };
                                res.status(200).send({ message: "Success", status: 1, data: users_data.data[0] });
                            } else if (users_data.data[0].is_private == 0) {
                                users_data.data[0].follow_status = find_follow_status(req.params.id, req.decoded._id);

                                users_data.data[0].profile_data = {
                                    all: [total_recommend, total_bookmarks],
                                    movie: [recommend_movie, bookmarks_movie],
                                    tv: [recommend_series, bookmarks_series],
                                    restaurant: [recommend_res, bookmarks_res],
                                    bar: [recommend_bar, bookmarks_bar],
                                    books: [recommend_book, bookmarks_book]
                                };
                                res.status(200).send({ message: "Success", status: 1, data: users_data.data[0] });
                            } else if (users_data.data[0].is_private == 1 && find_follow_status(req.params.id, req.decoded._id) == 1) {
                                users_data.data[0].follow_status = find_follow_status(req.params.id, req.decoded._id);

                                users_data.data[0].profile_data = {
                                    all: [total_recommend, total_bookmarks],
                                    movie: [recommend_movie, bookmarks_movie],
                                    tv: [recommend_series, bookmarks_series],
                                    restaurant: [recommend_res, bookmarks_res],
                                    bar: [recommend_bar, bookmarks_bar],
                                    books: [recommend_book, bookmarks_book]
                                };
                                res.status(200).send({ message: "Success", status: 1, data: users_data.data[0] });
                            } else {
                                users_data.data[0].follow_status = find_follow_status(req.params.id, req.decoded._id);
                                users_data.data[0].profile_data = {};
                                res.status(200).send({ message: "Success", status: 1, data: users_data.data[0] });
                            }
                        } else {
                            res.status(400).send({ message: users_data.message, status: 0 });
                        }
                    } else {
                        res.status(400).send({ message: global.messages.DisabledAccount, status: 0 });
                    }

                } else {
                    res.status(400).send({ message: try_data.message, status: 0 });
                }
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/follow_list/:user_id/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            var list = [];
            //var user_data = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.params.user_id) }, { is_private: 1, friends: 1 })
            var user_data = users.findOne.sync(null, { $and: [{ _id: mongoose.Types.ObjectId(req.params.user_id) }, { _id: { $nin: req.decoded.blocked_user } }], status: 1, is_deleted: 0, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } }, { is_private: 1, friends: 1, blocked_user: 1 })
            req.decoded.blocked_user = req.decoded.blocked_user.concat(user_data.data.blocked_user);
            if (user_data.status == 1) {
                var exist = 0;
                for (var i = 0; i < user_data.data.friends.length; i++) {
                    if (user_data.data.friends[i].user_id == req.decoded._id) {
                        exist = 1;
                        break
                    } else {
                        continue;
                    }
                }
                if (exist == 1 || user_data.data.is_private == 0 || req.params.user_id == req.decoded._id) {
                    var search_text = {};
                    if (req.query.search_text != undefined && req.query.search_text != "") {
                        var search_firstName = req.query.search_text.split(' ')[0];
                        var search_lastName = req.query.search_text.split(' ')[1];
                        //search_text.firstName = new RegExp('^' + search_firstName + '.*', 'i');
                        search_text.$or = [{ 'firstName': new RegExp('^' + search_firstName + '.*', 'i') },
                        { 'lastName': new RegExp('^' + search_firstName + '.*', 'i') }];

                        if (search_lastName != '' && search_lastName != undefined) {
                            search_text.firstName = new RegExp('^' + search_firstName, 'i');
                            search_text.lastName = new RegExp(search_lastName + '.*', 'i');
                        }
                    }
                    list = follwers_list(search_text, req.params.user_id, req.params.page_no, req.decoded.blocked_user, req.decoded._id);
                    var my_friends = [];
                    var my_data = users.findOne.sync(null, { _id: req.decoded._id }, { is_private: 1, friends: 1 })
                    if (my_data.status == 1) {
                        my_friends = my_data.data.friends
                    }
                    for (var i = 0; i < list.length; i++) {
                        var status = 0;
                        for (var j = 0; j < my_friends.length; j++) {
                            if (my_friends[j].user_id == list[i]._id) {
                                status = my_friends[j].status;
                                break;
                            }
                        }
                        list[i].status = status;
                    }
                    var query = { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.params.user_id), status: 1, is_blocked: 0 } }, _id: { $nin: req.decoded.blocked_user }, blocked_user: { $ne: req.decoded._id } };
                    var pages = 0;
                    var total_count = users.count.sync(null, query);
                    if (total_count.status == 1) {
                        pages = Math.ceil(total_count.data / global.pagination_limit)
                    }
                    res.status(200).send({ message: "Success", status: 1, pages: pages, data: list });
                } else {
                    res.status(200).send({ message: "This is a private user.", status: 0 });
                }
            } else {
                res.status(400).send({ message: "User not found", status: 0 });
            }

        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/following_list/:user_id/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            // console.log(req.decoded._id);
            var list = [];
            var blockeduser_array = [];
            for (var i = 0; i < req.decoded.friends.length; i++) {
                if (req.decoded.friends[i].is_blocked == 1) {
                    blockeduser_array.push(req.decoded.friends[i].user_id);
                }
            }
            // console.log(blockeduser_array);
            //var user_data = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.params.user_id) }, { is_private: 1, friends: 1 })
            var user_data = users.findOne.sync(null, { $and: [{ _id: mongoose.Types.ObjectId(req.params.user_id) }, { _id: { $nin: blockeduser_array } }], status: 1, is_deleted: 0, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) } }, { is_private: 1, friends: 1 })
            if (user_data.status == 1) {
                var search_text = {};
                if (req.query.search_text != undefined && req.query.search_text != "") {
                    var search_firstName = req.query.search_text.split(' ')[0];
                    var search_lastName = req.query.search_text.split(' ')[1];
                    //search_text.firstName = { $regex: new RegExp('^' + search_firstName + '.*', 'i') };
                    search_text.$or = [{ 'user_detail.firstName': new RegExp('^' + search_firstName + '.*', 'i') },
                    { 'user_detail.lastName': new RegExp('^' + search_firstName + '.*', 'i') }];

                    if (search_lastName != '' && search_lastName != undefined) {
                        search_text.firstName = { $regex: new RegExp('^' + search_firstName, 'i') }
                        search_text.lastName = { $regex: new RegExp('^' + search_lastName, 'i') }
                    }
                }
                if (req.params.user_id == req.decoded._id) {
                    list = follwing_list(search_text, req.params.user_id, req.params.page_no, req.decoded.blocked_user, req.decoded._id);
                    res.status(200).send({ message: "Success", status: 1, data: list });
                } else if (user_data.data.is_private == 0) {
                    list = follwing_list(search_text, req.params.user_id, req.params.page_no, req.decoded.blocked_user, req.decoded._id);
                    var my_friends = [];
                    var my_data = users.findOne.sync(null, { _id: req.decoded._id }, { is_private: 1, friends: 1 })
                    if (my_data.status == 1) {
                        my_friends = my_data.data.friends
                    }
                    for (var i = 0; i < list.length; i++) {
                        var status = 0;
                        for (var j = 0; j < my_friends.length; j++) {
                            if (my_friends[j].user_id == list[i]._id) {
                                status = my_friends[j].status;
                                break;
                            }
                        }
                        list[i].status = status;
                    }
                    res.status(200).send({ message: "Success", status: 1, data: list });
                } else {
                    var find_friend = find_follow_status(req.params.user_id, req.decoded._id);
                    if (find_friend == 1) {
                        list = follwing_list(search_text, req.params.user_id, req.params.page_no, req.decoded.blocked_user, req.decoded._id);
                        var my_friends = [];
                        var my_data = users.findOne.sync(null, { _id: req.decoded._id }, { is_private: 1, friends: 1 })
                        if (my_data.status == 1) {
                            my_friends = my_data.data.friends
                        }
                        for (var i = 0; i < list.length; i++) {
                            var status = 0;
                            for (var j = 0; j < my_friends.length; j++) {
                                if (my_friends[j].user_id == list[i]._id) {
                                    status = my_friends[j].status;
                                    break;
                                }
                            }
                            list[i].status = status;
                        }
                        res.status(200).send({ message: "Success", status: 1, data: list });

                    } else {
                        res.status(200).send({ message: "This is a private user.", status: 0 });
                    }
                }
            } else {
                res.status(400).send({ message: "User not found", status: 0 });
            }

        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
// var follwers_list = function (search_text, user_id, page_no, blockeduser_array) {
//     var _skip = (page_no - 1) * global.pagination_limit;
//     var query = { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(user_id), status: 1, is_blocked: 0 } }, _id: { $nin: blockeduser_array } };
//     if (search_text != null || search_text != undefined) {
//         if (search_text.firstName)
//             query.firstName = search_text.firstName
//         if (search_text.lastName)
//             query.lastName = search_text.lastName
//     }
//     var projection = { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 };
//     var sort = { 'firstName': 1 }
//     var follow_list = users.find_pagination_sort.sync(null, query, projection, _skip, global.pagination_limit, sort);
//     if (follow_list.status == 1) {
//         return (follow_list.data);
//     } else {
//         return ([]);
//     }
// }
var follwers_list = function (search_text, user_id, page_no, blockeduser_array, decoded_id) {
    var _skip = (page_no - 1) * global.pagination_limit;
    var query = { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(user_id), status: 1, is_blocked: 0 } }, _id: { $nin: blockeduser_array }, blocked_user: { $nin: [decoded_id, user_id] }, is_deleted: 0, status: 1 };
    if (search_text != null || search_text != undefined) {
        if (search_text.firstName)
            query.firstName = search_text.firstName
        if (search_text.lastName)
            query.lastName = search_text.lastName
        if (search_text.$or) {
            query["$or"] = search_text.$or
        }
    }
    var projection = { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 };
    var sort = { firstName: 1, _id: 1 }
    var follow_list = users.find_pagination_sort.sync(null, query, projection, _skip, global.pagination_limit, sort);
    if (follow_list.status == 1) {
        return (follow_list.data);
    } else {
        return ([]);
    }
}
var follwing_list = function (search_text, user_id, page_no, blockeduser_array, decoded_id) {
    var match_cond = { 'user_detail.is_deleted': 0, 'user_detail.status': 1 };
    if (search_text != null || search_text != undefined) {
        if (search_text.firstName)
            match_cond[["user_detail.firstName"]] = search_text.firstName
        if (search_text.lastName)
            match_cond[["user_detail.lastName"]] = search_text.lastName
        if (search_text.$or) {
            match_cond[["$or"]] = search_text.$or
        }

    }
    var _skip = (page_no - 1) * global.pagination_limit;

    if (user_id == decoded_id) {
        var query_half1 = [{ $match: { _id: mongoose.Types.ObjectId(user_id) } },
        {
            $project: {
                friends: {
                    $filter: {
                        input: "$friends",
                        as: "friends",
                        //cond: { $eq: ["$$friends.status", 1] }
                        cond: { $and: [{ $ne: ["$$friends.status", 0] }, { $eq: ["$$friends.is_blocked", 0] }] }
                    }
                }
            }
        },];
    } else {
        var query_half1 = [{ $match: { _id: mongoose.Types.ObjectId(user_id) } },
        {
            $project: {
                friends: {
                    $filter: {
                        input: "$friends",
                        as: "friends",
                        //cond: { $eq: ["$$friends.status", 1] }
                        cond: { $and: [{ $eq: ["$$friends.status", 1] }, { $eq: ["$$friends.is_blocked", 0] }] }
                    }
                }
            }
        },];
    }
    var query_half2 = [
        { "$unwind": "$friends" },
        { $match: { "friends.user_id": { $nin: blockeduser_array } } },
        {
            "$lookup": {
                "from": "users",
                "localField": "friends.user_id",
                "foreignField": "_id",
                "as": "user_detail"
            }
        },
        { "$unwind": "$user_detail" },
        { $match: match_cond },
        {
            $project: {
                _id: "$user_detail._id",
                firstName: "$user_detail.firstName",
                lastName: "$user_detail.lastName",
                profile_pic: "$user_detail.profile_pic",
                blocked_user: "$user_detail.blocked_user",
                status: "$friends.status",
            }
        },
        { $match: { blocked_user: { $nin: [mongoose.Types.ObjectId(decoded_id), mongoose.Types.ObjectId(user_id)] } } },
        //{ $match: { blocked_user: { $ne: mongoose.Types.ObjectId(decoded_id) } } },
        {
            $project: {
                _id: "$_id",
                firstName: "$firstName",
                lastName: "$lastName",
                profile_pic: "$profile_pic",
                status: "$status",
            }
        },
        { $sort: { firstName: 1, _id: 1 } },
        { $skip: _skip },
        { $limit: global.pagination_limit },
    ];
    var query = query_half1.concat(query_half2);
    //console.log(util.inspect(query, { depth: null, hidden: true }));
    var following_list = users.aggregate.sync(null, query);
    if (following_list.status == 1) {

        return (following_list.data);
    } else {
        return ([]);
    }
}
var find_follow_status = function (friend_id, my_id) {
    var query =
        [{ $match: { '_id': mongoose.Types.ObjectId(my_id), 'friends.user_id': mongoose.Types.ObjectId(friend_id) } },
        {
            $project: {

                friend: {
                    $filter: {
                        input: "$friends",
                        as: "items",
                        cond: { $eq: ["$$items.user_id", mongoose.Types.ObjectId(friend_id)] }
                    }
                }
            }
        },
        { $project: { status: '$friend.status' } },
        { $unwind: "$status" }
        ];
    var status_data = users.aggregate.sync(null, query);
    if (status_data.status == 1) {
        if (status_data.data.length)
            return (status_data.data[0].status);
        else
            return (0);
    } else {
        return (0);
    }
}
module.exports = router;