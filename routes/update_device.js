// push_notification.js
var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');

// add device token to user account

var device_token_update_validate = function (req, res, next) {
    req.assert('device_token', 'device_token key is required').notEmpty();
    return req.validationErrors(true);
};

router.put('/', function (req, res, next)
{
    var errors = device_token_update_validate(req, res, next);
    if (!errors)
    {
        Sync(function ()
        {
            try
            {
                // delete the device token from other users
                users.update.sync(null, {_id: {$ne : req.decoded._id }, device_token : req.body.device_token }, {device_token: ""});
                // update user's deive token
                var device_token_check = users.update.sync(null, {_id: req.decoded._id}, {device_token: req.body.device_token});
                console.log(device_token_check);
                res.status(200).send({message: "success", status: 1, device_token: req.body.device_token});
            } catch (err) // catch errors
            {
                res.status(400).send({message: err.message, status: 0});
            }
        });
    } else
    {
        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});

module.exports = router;
