var express = require('express');
var router = express.Router();
var Sync = require('sync');
var users = require('../controller/users.js');

router.put('/', function (req, res, next) {
    Sync(function () {
        try {
            //console.log(req.decoded._id);
            var data_update = users.updateNew.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { device_token: "" });
            //console.log(data_update);
            if (data_update.status == 1) {
                res.status(200).send({ message: "Success", status: 1 });
            } else {
                res.status(400).send({ message: data_update.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});

module.exports = router;