
var express = require('express');
var router = express.Router();

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sync = require('sync');
var moment = require('moment');

var user = require('../controller/users.js');

//user authentication and redirection
router.use(function (req, res, next) {
    Sync(function () {
        try {
            // check header or url parameters or post parameters for token
            var token = req.headers['token'];
            // decode token
            if (token) {
                // console.log(token);
                // verifies secret and checks exp
                jwt.verify(token, global.secret, {
                    ignoreExpiration: true
                }, function (err, decoded) {
                    if (err) {
                        res.status(400).send({ status: 0, message: 'Your session has expired ,please signin again' });
                    } else {
                        Sync(function () {
                            try {
                                req.decoded = decoded;
                                var curentTimestamp = moment().utc().unix();
                                var userData = user.findOneAndUpdate.sync(null, { _id: decoded._id }, { last_login: curentTimestamp });
                                if (userData.status == 0)
                                    return next({ status: 401, message: "User authentication failed,please signin again" });

                                if (userData.data.is_deleted == true || userData.data.is_deleted == 1)
                                    return next({ message: "Your account has been deleted", status: 401 });

                                if (userData.data.status == false || userData.data.status == 0)
                                    return next({ message: global.messages.accountDisabledByAdmin, status: 401 });
                                //if (userData.status == 2)
                                //return next({status: 401, message: global.messages.emailNotVerifiedInLogin});
                                req.decoded = decoded;
                                req.decoded.friends = userData.data.friends;
                                req.decoded.firstName = userData.data.firstName;
                                req.decoded.lastName = userData.data.lastName;
                                req.decoded.blocked_user = userData.data.blocked_user;
                                next();

                            } catch (err) {
                                return next({ message: err.message });
                            }
                        });
                    }
                });
            } else {
                // if there is no token return an error
                return next({ status: 401, message: 'No token provided' });
            }
        } catch (err) {
            return next({ message: err.message });
        }
    });
});

module.exports = router;
