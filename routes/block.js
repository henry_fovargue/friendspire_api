var express = require('express');
var router = express.Router();
var Sync = require("sync");
var mongoose = require("mongoose");
const user = require("../controller/users");
/* for blocking the user*/
var blockuservalidate = function (req, res, next) {
    req.checkBody("user_id").notEmpty().withMessage('invalid user_id');
    return req.validationErrors(true);
};

router.post("/", function (req, res, next) {
    let errors = blockuservalidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                let user_id = mongoose.Types.ObjectId(req.body.user_id);
                let blockeduser = user.findOne.sync(null, {_id: user_id, is_deleted: 0, status: 1}, {first_name: 1, last_name: 1, friends: 1});
                if (blockeduser.status === 1) {
                    let conditionForAlredyBlock = {
                        _id: req.decoded._id,
                        blocked_user: {$elemMatch: {$eq: user_id}}
                    };
                    var alreadyblocked = user.find.sync(null, conditionForAlredyBlock, {});
                    if (alreadyblocked.status == 1) {
                        if (alreadyblocked["data"].length > 0) {
                            throw {message: "user already blocked"};
                        } else {
                            var blockUserResult = user.update.sync(null, {_id: req.decoded._id, is_deleted: 0, status: 1}, {$push: {blocked_user: user_id}})
                            if (blockUserResult.status == 1) {
                                let friendBlockCondition = {
                                    _id: mongoose.Types.ObjectId(req.decoded._id),
                                    friends: {$elemMatch: {user_id: user_id}}
                                };
                                var blockFriend = user.updateNew.sync(null, friendBlockCondition, {"friends.$.is_blocked": 1});
                                if (blockFriend.status == 1) {
                                    res.status(200).send({message: "success", status: 1});
                                } else {
                                    throw blockFriend;
                                }
                            } else {
                                throw blockUserResult;
                            }
                        }
                    } else {
                        throw alreadyblocked;
                    }
                } else {
                    throw blockeduser;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({message: err.message, status: 0});
            }
        });
    } else {
        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});

/* unblock the user */
var unblockuservalidate = function (req, res, next) {
    req.checkBody("user_id").notEmpty().withMessage('invalid user_id');
    return req.validationErrors(true);
};
router.put("/", function (req, res, next) {
    let errors = unblockuservalidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                let user_id = mongoose.Types.ObjectId(req.body.user_id);
                let blockeduser = user.findOne.sync(null, {_id: mongoose.Types.ObjectId(req.body.user_id), is_deleted: 0, status: 1}, {first_name: 1, last_name: 1});
                if (blockeduser.status === 1) {
                    let conditionForBlock = {
                        _id: req.decoded._id,
                        blocked_user: {$elemMatch: {$eq: user_id}}
                    };
                    var alreadyblocked = user.find.sync(null, conditionForBlock, {});
                    if (alreadyblocked.status == 1) {
                        if (alreadyblocked["data"].length > 0) {
                            var unblockUserResult = user.update.sync(null, {_id: req.decoded._id, is_deleted: 0, status: 1}, {$pull: {blocked_user: user_id}})
                            if (unblockUserResult.status == 1) {
                                let friendBlockCondition = {
                                    _id: mongoose.Types.ObjectId(req.decoded._id),
                                    friends: {$elemMatch: {user_id: user_id, is_blocked: 1}},
                                };
                                var blockFriend = user.updateNew.sync(null, friendBlockCondition, {"friends.$.is_blocked": 0});
                                if (blockFriend.status == 1) {
                                    res.status(200).send({message: "success", status: 1});
                                } else {
                                    throw blockFriend;
                                }
                            } else {
                                throw unblockUserResult;
                            }
                        } else {
                            throw {message: "user already unblock"};
                        }
                    } else {
                        throw alreadyblocked;
                    }
                } else {
                    throw blockeduser;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({message: err.message, status: 0});
            }
        });
    } else {
        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});

var listblockvalidate = function (req, res, next) {
    req.checkQuery("page").notEmpty().withMessage('page number is required');
    return req.validationErrors(true);
};
router.get("/", function (req, res, next) {
    let errors = listblockvalidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                let user_id = mongoose.Types.ObjectId(req.decoded._id);
                let skip = (parseInt(req.query.page) - 1) * global.pagination_limit;
                let aggregation = [
                    {$match: {_id: user_id}},
                    {$project: {blocked_user: 1, _id: 0}},
                    {$unwind: "$blocked_user"},
                    {
                        "$lookup": {
                            from: "users",
                            let: {ref_id: "$blocked_user"},
                            pipeline: [
                                {
                                    $match:
                                            {
                                                $expr:
                                                        {
                                                            $and:
                                                                    [
                                                                        {$eq: ["$_id", "$$ref_id"]},
                                                                        {$eq: ["$status", 1]},
                                                                        {$eq: ["$is_deleted", 0]}
                                                                    ]
                                                        }
                                            }
                                },
                                {"$project": {_id: 1, firstName: 1, lastName: 1, profile_pic: 1}}
                            ],
                            as: "user"
                        }
                    },
                    {$unwind: "$user"},
                    {$replaceRoot: {newRoot: "$user"}},
                    {$sort: {firstName: 1, lastName: 1}},
                    {"$skip": skip},
                    {"$limit": global.pagination_limit},
                ];
                var blocklist = user.aggregate.sync(null, aggregation);
                if (blocklist.status == 1) {
                    if (blocklist["data"].length > 0) {
                        res.status(200).send({message: "success", status: 1, data: blocklist["data"]})
                    } else {
                        res.status(200).send({message: "success", status: 0, data: blocklist["data"]})
                    }
                } else {
                    throw blocklist;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({message: err.message, status: 0});
            }
        });
    } else {
        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});
module.exports = router;
