var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
var Sync = require('sync');

//route for sync friends
router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            //            var condition = {_id: req.decoded._id};
            //            var population = [{
            //                    path: 'friends.user_id',
            //                    select: 'firstName lastName profile_pic'
            //                }];
            var friends = [];
            //            var query = [{$match: {_id: mongoose.Types.ObjectId(req.decoded._id)}},
            //                {$project: {
            //                        friends: {$filter: {
            //                                input: "$friends",
            //                                as: "items",
            //                                cond: {$eq: ["$$items.type", 1]}
            //                            }}
            //                    }},
            //                {$lookup: {from: "users",
            //                        localField: "friends.user_id",
            //                        foreignField: "_id",
            //                        as: "friend_data"}}
            //
            //
            //            ]
            var query = [{ $match: { _id: mongoose.Types.ObjectId(req.decoded._id) } },
            {
                $project: {
                    status: 1,
                    friends: {
                        $filter: {
                            input: "$friends",
                            as: "items",
                            cond: {
                                $and: [
                                    //{ $in: ["$$items.status", [0, 2]] },
                                    { $eq: ["$$items.status", 0] },
                                    { $eq: ["$$items.type", 1] }
                                ]
                            }
                        }
                    }

                }
            },
            { $unwind: "$friends" },
            {
                $lookup: {
                    from: "users",
                    localField: "friends.user_id",
                    foreignField: "_id",
                    as: "friends.user_id"
                }
            },
            { $project: { _id: 0, 'friends.status': 1, 'friends.user_id._id': 1, 'friends.user_id.firstName': 1, 'friends.user_id.lastName': 1, 'friends.user_id.profile_pic': 1 } },
            { $sort: { 'friends.user_id.firstName': 1 } }
            ]

            var user_data = users.aggregate.sync(null, query);
            //            var user_data = users.findOne_population.sync(null,condition,population);
            if (user_data.status === 1) {
                if (user_data.data.length) {
                    for (var i = 0; i < user_data.data.length; i++)
                        friends.push({ status: user_data.data[i].friends.status, user_id: user_data.data[i].friends.user_id[0] })
                    //                        friends.push(user_data.data[i].friends)
                }
                res.status(200).send({ message: "Success", status: 1, friends: friends });
            } else {
                res.status(400).send({ message: user_data.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});

module.exports = router;