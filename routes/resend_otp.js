var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
var emails = require('../helper/emails.js');
//route for resend OTP
router.post('/', function (req, res, next)
{
    Sync(function ()
    {
        try
        {
            // call sync function to check phone number already exists or not
            var userData = users.findOne.sync(null, {_id: req.decoded._id, is_deleted: 0}, {});
            if (userData.status != 1)
                throw Error("User not found");
            if (userData.data.status === 1)
                throw Error("Already verified");
            else
            {
                emails.signup_otp_email.sync(null, userData.data);
                res.status(200).send({message: global.messages.otpSentSuccessfully, status: 1});
            }
        } catch (err) // catch errors
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });

});

module.exports = router;
