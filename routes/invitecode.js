var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Sync = require('sync'); // require Sync module for Sync
var users = require('../controller/users.js');




var addReferralCode = function (req, res, next) {
    req.checkBody('code', 'Please enter code').notEmpty();
    return req.validationErrors(true);
};
router.post("/", function (req, res, next) {
    var errors = addReferralCode(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                let referralCode = req.body.code;
//                console.log(req.decoded._id);
                let checkResult = users.findOne.sync(null, {reference_code: req.body.code}, {firstName: 1, reference_code: 1});
                if (checkResult.status == 1) {
                    let alreadyrefCode = users.findOne.sync(null, {_id: req.decoded._id}, {referral_code: 1});
                    if (alreadyrefCode.status == 1) {
                        if (alreadyrefCode["data"]["referral_code"] == "") {
                            let updateRefferalCode = users.update.sync(null, {_id: req.decoded._id}, {referral_code: referralCode});
                            if (updateRefferalCode.status == 1) {
                                res.status(200).send({message: "success", status: 1});
                            } else {
                                throw updateRefferalCode;
                            }
                        } else {
                            throw {message: "referral code already present"};
                        }
                    } else {
                        throw alreadyrefCode;
                    }
                } else {
                    throw {message: "invalid referral code"};
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({message: err.message, status: 0});
            }
        });
    } else {
        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});

module.exports = router;