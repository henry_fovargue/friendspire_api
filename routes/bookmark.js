var express = require('express');
var router = express.Router();
var recommend = require('../controller/recommendations.js');
var bookmark = require('../controller/bookmarks.js');
var user = require('../controller/users.js');
var mongoose = require('mongoose');
var Sync = require('sync');
var post = require('../controller/post.js');
const moment = require('moment');
const momenttz = require('moment-timezone');
const projectionfield = require("../helper/projectionFields");
const config = require("../config");
var cuisine = require("../cuisine");
const util = require("util");

var bookmarklist_validate = (req, res, next) => {
    //add rules for validations
    if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
        req.assert("lat_long").notEmpty().withMessage('Please enter lat_long');
    }
    req.assert("category").notEmpty().withMessage('Please enter category');
    //    req.assert("friends").notEmpty().withMessage('Please enter friends key');
    if (req.body.map != 1) {
        req.assert("page").notEmpty().withMessage('Please enter page number');
    }
    return req.validationErrors(true);
};

router.post("/list", (req, res, next) => {
    let errors = bookmarklist_validate(req, res, next);
    if (!errors) {
        //        console.log(req.decoded._id);
        Sync(function () {
            try {
                req.body.distance = parseFloat(req.body.distance);
                var user_id;
                var timezone = req.headers.timezone;
                var blockeduserarray = [];
                var friends = [];
                var distance_user = user.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 }, { distance_unit: 1 });
                if (req.body.userid) {
                    user_id = mongoose.Types.ObjectId(req.body.userid);
                    var userinfo = user.findOne.sync(null, { _id: user_id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, friends: 1, blocked_user: 1 });
                    if (userinfo.status === 1) {
                        if (userinfo["data"]["blocked_user"].includes(req.decoded._id)) {
                            throw { message: "you are currently blocked by this user" };
                        } else {
                            var selfinfo = user.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, blocked_user: 1 });
                            //                            blockeduserarray = selfinfo["data"]["blocked_user"].map(obj => {
                            //                                return mongoose.Types.ObjectId(obj);
                            //                            });
                            blockeduserarray = selfinfo["data"]["blocked_user"];
                            if (blockeduserarray.includes(user_id.toString())) {
                                throw { message: "user is currently blocked by you" };
                            }

                        }
                        if (userinfo["data"]["is_private"] == 1) {
                            let query = { _id: req.decoded._id, "friends": { $elemMatch: { user_id: user_id, status: 1, is_blocked: 0 } } };
                            let userpri = user.findOne.sync(null, query, {});
                            if (userpri.status == 0) {
                                throw { message: "This user has private account" };
                            }
                        }
                        for (var j = 0; j < userinfo.data.friends.length; j++) {
                            if (userinfo.data.friends[j].status == 1 && userinfo.data.friends[j].is_blocked == 0)
                                friends.push(mongoose.Types.ObjectId(userinfo.data.friends[j].user_id.toString()));
                        }
                        // console.log("------------------",friends);
                        var usersWhoBlockMe = user.find.sync(null, { blocked_user: { $eq: user_id }, is_deleted: 0, status: 1 }, { _id: 1 });
                        // console.log(usersWhoBlockMe);
                        var usersWhoBlockMe_array = [];
                        for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                            usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                        }
                        // console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);

                        friends = friends.filter(f => {
                            if (usersWhoBlockMe_array.includes(f.toString())) {
                                return false;
                            } else {
                                return true;
                            }
                        });
                        // console.log("+++++++++++++++++++",friends);
                    } else {
                        throw userinfo;
                    }

                } else {
                    user_id = mongoose.Types.ObjectId(req.decoded._id);
                    var userinfo = user.findOne.sync(null, { _id: user_id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, blocked_user: 1, friends: 1 });
                    //                    blockeduserarray = userinfo["data"]["blocked_user"].map(obj => {
                    //                        return mongoose.Types.ObjectId(obj);
                    //                    });
                    for (var j = 0; j < userinfo.data.friends.length; j++) {
                        if (userinfo.data.friends[j].status == 1 && userinfo.data.friends[j].is_blocked == 0)
                            friends.push(mongoose.Types.ObjectId(userinfo.data.friends[j].user_id.toString()));
                    }
                    // console.log("------------------",friends);
                    var usersWhoBlockMe = user.find.sync(null, { blocked_user: { $eq: user_id }, is_deleted: 0, status: 1 }, { _id: 1 });
                    // console.log(usersWhoBlockMe);
                    var usersWhoBlockMe_array = [];
                    for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                        usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                    }
                    // console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);

                    friends = friends.filter(f => {
                        if (usersWhoBlockMe_array.includes(f.toString())) {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    // console.log("+++++++++++++++++++",friends);
                }
                var bookmarklist = bookmark.find.sync(null, { user_id: user_id, type: parseInt(req.body.category), status: 1 }, {});
                if (bookmarklist.status === 0) {
                    throw bookmarklist;
                }
                var bookmarkarray = bookmarklist["data"].map(obj => {
                    return mongoose.Types.ObjectId(obj.reference_id)
                });
                var filter = { type: parseInt(req.body.category), dummy: 0 };
                var filterPagesDuration;
                filter["_id"] = { "$in": bookmarkarray };
                var page = req.body.page;
                var aggregation_pipeline = [];
                let projection = projectionfield.projectlist(req.body.category.toString());
                var ref_ids = [];
                var sort = { "user_rating.updated_at": -1 };

                /* filters */
                if (req.body.search_text) {
                    filter["title"] = new RegExp('^' + req.body.search_text + '.*', 'i');
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (req.body.cuisine) {
                        if (Array.isArray(req.body.cuisine)) {
                            if (req.body.cuisine.includes("Other")) {
                                var index = req.body.cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    req.body.cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", req.body.cuisine);
                                req.body.cuisine = req.body.cuisine.concat(cuisine.other_cuisine);
                            }
                            filter["cuisine"] = { "$in": req.body.cuisine };
                        } else {
                            //filter["cuisine"] = { "$in": JSON.parse(req.body.cuisine) };
                            var array_cuisine = JSON.parse(req.body.cuisine);
                            // console.log(array_cuisine);
                            if (array_cuisine.includes("Other")) {
                                var index = array_cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    array_cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", array_cuisine);
                                array_cuisine = array_cuisine.concat(cuisine.other_cuisine);
                            }
                            // console.log("--------------------", array_cuisine);
                            filter["cuisine"] = { "$in": array_cuisine };
                        }
                    }
                    if (req.body.price) {
                        //between 1 to 4
                        if (Array.isArray(req.body.price)) {
                            filter["price_tier"] = { "$in": req.body.price };
                        } else {
                            filter["price_tier"] = { "$in": JSON.parse(req.body.price) };
                        }
                    }
                } else if (req.body.category == 3 || req.body.category == 4) { // for movies and series

                    if (req.body.genre) {
                        if (Array.isArray(req.body.genre)) {
                            filter["genre"] = { "$in": req.body.genre };
                        } else {
                            filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                        }
                    }
                    if (req.body.release_year && req.body.category == 3) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.release_year && req.body.category == 4) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        //let array = JSON.parse(req.body.release_year);
                        if (array[0])
                            filter["start_year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["start_year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.duration) {
                        // for movie duraion
                        if (Array.isArray(req.body.duration)) {
                            var array = req.body.duration;
                        } else {
                            var array = JSON.parse(req.body.duration);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                } else if (req.body.category == 5) {
                    // for books
                    if (req.body.pages) {
                        // for movie duraion
                        if (Array.isArray(req.body.pages)) {
                            var array = req.body.pages;
                        } else {
                            var array = JSON.parse(req.body.pages);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                    if (req.body.publish_year) {
                        //from 0 to current year
                        if (Array.isArray(req.body.publish_year)) {
                            var array = req.body.publish_year;
                        } else {
                            var array = JSON.parse(req.body.publish_year);
                        }
                        //let array = JSON.parse(req.body.publish_year);
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.genre) {
                        if (Array.isArray(req.body.genre)) {
                            filter["genre"] = { "$in": req.body.genre };
                        } else {
                            filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                        }
                    }
                }
                if (req.body.rating) {
                    //between 1 to 5
                    if (Array.isArray(req.body.rating)) {
                        var array = req.body.rating;
                    } else {
                        var array = JSON.parse(req.body.rating);
                    }
                    if (array[0])
                        filter["rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };


                }
                if (req.body.friendspire_rating) {
                    //between 1 to 10
                    if (Array.isArray(req.body.friendspire_rating)) {
                        var array = req.body.friendspire_rating;
                    } else {
                        var array = JSON.parse(req.body.friendspire_rating);
                    }
                    if (array[0])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (Array.isArray(req.body["lat_long"])) {
                        var coordinates = req.body["lat_long"];
                    } else {
                        var coordinates = JSON.parse(req.body["lat_long"]);
                    }
                    let distance;

                    if (req.body.distance) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance,
                                    spherical: true
                                }
                            }
                        ]
                    } else {
                        //console.log("~~~~~~~~bookmark~~~~~~~",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1)
                        {
                            distance = config.nearbyRaduisMiles;
                        } else {
                            distance = config.nearbyRaduisKm;
                        }
                        
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    //                                maxDistance: distance,
                                    spherical: true
                                }
                            }
                        ]
                    }

                    if (req.body.filterLatLong || req.body.distance == 0) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                        //console.log("------filterLatLong--------",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1)
                        {
                            var distance_set = config.nearbyRaduisMiles;
                        } else {
                            var distance_set = config.nearbyRaduisKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance_set,
                                    spherical: true
                                }
                            }
                        ]
                    }

                    if (req.body.map) {
                        if(distance_user.data.distance_unit == 1)
                        {
                            var distance_map = config.nearbyRaduisMapMiles;
                        } else {
                            var distance_map = config.nearbyRaduisMapKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance_map,
                                    spherical: true
                                }
                            }
                        ]
                    }

                } else {
                    aggregation_pipeline = [
                        { "$match": filter }
                    ];
                    if (filterPagesDuration) {
                        aggregation_pipeline.push({ "$match": filterPagesDuration });
                    }
                }

                if (req.body.category == 1 || req.body.category == 2) {  //restaurant
                    if (req.body.open_now == 1) {
                        let time = momenttz().tz(timezone);
                        let day_of_week = time.isoWeekday() - 1;
                        let inttime = parseInt(time.format("HHmm"));
                        var new_date = momenttz.tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                        var agg = [
                            { "$match": { timezone: timezone } },
                            {
                                $project: {
                                    timezone: 1,
                                    //                                    date1: {$toInt: "$date1"},
                                    dayOfWeek: 1,
                                    timings1: '$timings',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    timings: { $arrayElemAt: ['$timings', day_of_week] }
                                }
                            },
                            {
                                $match: { timings: { $ne: undefined } }
                            },
                            {
                                $project: {
                                    timezone: 1,
                                    date1: 1,
                                    dayOfWeek: 1,
                                    timings: '$timings1',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    temp: {
                                        $filter: {
                                            input: '$timings',
                                            as: "num",
                                            cond: {
                                                $and: [
                                                    { $gte: [inttime, { $toInt: "$$num.start" }] },
                                                    { $lte: [inttime, { $toInt: "$$num.end" }] },
                                                ]
                                            }
                                        }
                                    }
                                }

                            },

                            {
                                $match: {
                                    'temp.0': { $exists: true }
                                }
                            }
                        ];
                        aggregation_pipeline = aggregation_pipeline.concat(agg);
                    }
                }

                let commonAggregaion = [
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", user_id] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, updated_at: 1 } },
                            ],
                            as: "bookmark"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", avgrating: { $avg: "$rating" }, count_friends: { $sum: 1 } } },
                                { "$project": { _id: 0, avgrating: 1, count_friends: 1 } },
                            ],
                            as: "friends_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, status: 1 } },
                            ],
                            as: "my_bookmark_status"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "my_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", count_everyone: { $sum: 1 } } },
                                { "$project": { _id: 0, count_everyone: 1 } },
                            ],
                            as: "everyone_count"
                        }
                    },
                    //{"$sort": {"bookmark.updated_at": -1}}, // Latest first
                    { "$sort": { "title": 1 } }, // Latest first
                    //{"$skip": (page - 1) * global.pagination_limit},
                    //{"$limit": global.pagination_limit},
                    { "$project": projection }
                ];

                if (req.body.map != 1 || req.body.map == undefined) {
                    commonAggregaion.push({ "$skip": (page - 1) * global.pagination_limit });
                    commonAggregaion.push({ "$limit": global.pagination_limit });
                }

                let countAggregation = [
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "user_rating"
                        }
                    },
                    { "$sort": sort }, // Latest first
                    {
                        $group: {
                            _id: null,
                            count: { $sum: 1 }
                        }
                    }
                ];

                var countAggregationPipeline = aggregation_pipeline;
                countAggregationPipeline = countAggregationPipeline.concat(countAggregation);
                var countpost = post.aggregate.sync(null, countAggregationPipeline);
                if (countpost.status === 0) {
                    throw countpost;
                }
                var count;
                if (countpost["data"].length > 0) {
                    var count = countpost["data"][0]["count"];
                } else {
                    var count = 0;
                }
                aggregation_pipeline = aggregation_pipeline.concat(commonAggregaion);
                let result = post.aggregate.sync(null, aggregation_pipeline);
                //console.log(result);
                //                console.log(util.inspect(result, { depth: null }));
                if (result.status == 1) {
                    result["data"] = result["data"].map(obj => {
                        if (req.body.category == 1 || req.body.category == 2) {
                            let closed_at = 0;
                            let is_open = 2;
                            if (obj["timezone"] && obj["timings"].length > 0) {
                                let time = momenttz().tz(obj["timezone"]);
                                let timing = obj["timings"][time.isoWeekday() - 1];
                                let inttime = time.format("HHmm");
                                timing.forEach(object => {
                                    closed_at = parseInt(object["end"]) > closed_at ? parseInt(object["end"]) : closed_at;
                                    if (parseInt(object["start"]) <= inttime && inttime <= parseInt(object["end"])) {
                                        is_open = 1;
                                    }
                                    //                                    let endTime;
                                    //                                    if (object["end"].charAt(0) == "+") {
                                    //                                        endTime = 2400 + parseInt(object["end"]);
                                    //                                    } else {
                                    //                                        endTime = parseInt(object["end"]);
                                    //                                    }
                                    //
                                    //                                    if (parseInt(object["start"]) <= inttime && inttime <= endTime) {
                                    //                                        is_open = 1;
                                    //                                    }
                                    //                                    closed_at = endTime > closed_at ? endTime : closed_at;
                                    //                                    if (closed_at >= 2400) {
                                    //                                        closed_at = closed_at - 2400;
                                    //                                    }
                                });
                                obj["closed_at"] = moment(closed_at.toString(), "HHmm").format("HH:mm");

                                delete obj["timings"];
                                delete obj["timezone"];
                            } else {
                                is_open = 0;
                            }
                            obj["is_open"] = is_open;
                            if (obj["location"]) {
                                obj["location"] = [obj["location"]["coordinates"][1], obj["location"]["coordinates"][0]]
                            }
                        }
                        if (!obj["cuisine"]) {
                            obj["cuisine"] = [];
                        }

                        if (obj["friends_rating"].length > 0) {
                            obj["count_friends"] = parseInt(obj["friends_rating"][0]["count_friends"]);
                            obj["friends_rating"] = parseFloat(obj["friends_rating"][0]["avgrating"]);

                        } else {
                            obj["count_friends"] = parseInt(0)
                            obj["friends_rating"] = parseFloat(0.00);
                        }

                        if (obj["my_bookmark_status"].length > 0) {
                            obj["my_bookmark_status"] = obj["my_bookmark_status"][0]["status"];
                        } else {
                            obj["my_bookmark_status"] = 0;
                        }

                        if (obj["everyone_count"].length > 0) {
                            obj["everyone_count"] = parseInt(obj["everyone_count"][0]["count_everyone"]);

                        } else {
                            obj["everyone_count"] = parseInt(0)
                        }

                        if (obj["my_rating"].length > 0) {
                            obj["my_rating"] = obj["my_rating"][0]["rating"];
                        } else {
                            obj["my_rating"] = 0;
                        }

                        //                            if (obj["cuisine"].length < 1) {
                        //                                obj["cuisine"] = [];
                        //                            }

                        obj["is_bookmarked"] = true;
                        return obj;
                    });
                    res.status(200).send({ status: 1, message: "success", distance_unit: distance_user["data"]["distance_unit"], count: count, data: result["data"] });
                } else {
                    throw result;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }

        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

router.post('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.type && req.body.referenceid) {
                var query = {};
                query.user_id = req.decoded._id;
                query.reference_id = req.body.referenceid;
                var query_recommend = { is_deleted: 0 };
                query_recommend.user_id = req.decoded._id;
                query_recommend.reference_id = req.body.referenceid;
                query_recommend.type = req.body.type;
                var data = { status: 1 };
                data.type = req.body.type;
                data.user_id = req.decoded._id;
                data.reference_id = req.body.referenceid;
                var bookmarkdata_list = bookmark.findOne.sync(null, { type: req.body.type, user_id: req.decoded._id, reference_id: req.body.referenceid, status: 1 });
                if (bookmarkdata_list.status == 1) {
                    res.status(400).send({ message: "You have already bookmarked this", status: 0 });
                } else {
                    var recommend_data = recommend.findOne.sync(null, query_recommend);
                    if (recommend_data.status == 1) {
                        res.status(400).send({ message: "You have already recommended this post", status: 0 });
                    } else {
                        var data_list = bookmark.UpdateAndSave.sync(null, query, data);
                        if (data_list.status == 1) {
                            res.status(200).send({ message: "Success", status: 1 });
                        } else {
                            res.status(400).send({ message: data_list.message, status: 0 });
                        }
                    }
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});

router.delete('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.referenceid) {
                var query = { status: 1 };
                query.user_id = req.decoded._id;
                query.reference_id = req.body.referenceid;
                var data = { status: 0 };
                var bookmarkdata_list = bookmark.findOne.sync(null, { user_id: req.decoded._id, reference_id: req.body.referenceid, status: 0 });
                if (bookmarkdata_list.status == 1) {
                    res.status(400).send({ message: "You have already remove bookmark", status: 0 });
                } else {
                    var data_list = bookmark.update.sync(null, query, data);
                    if (data_list.status == 1) {
                        res.status(200).send({ message: "Success", status: 1 });
                    } else {
                        res.status(400).send({ message: data_list.message, status: 0 });
                    }
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});



router.post("/listing", (req, res, next) => {
    let errors = list_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                var timezone = req.headers.timezone;
                var filter = { type: parseInt(req.body.category) };
                var filterPagesDuration;
                var page = req.body.page;
                var aggregation_pipeline = [];
                var matchrecommend = { type: parseInt(req.body.category) };
                let projection = projectionfield.projectlist(req.body.category.toString());
                var ref_ids = [];
                var sort = { "updated_at": -1 };
                var userfriend = user.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { "friends": 1, distance_unit: 1 });

                if (userfriend.status === 1) {
                    //                    var friends = userfriend.data.friends.map(obj => mongoose.Types.ObjectId(obj.user_id));
                    var friends = [];
                    for (var j = 0; j < userfriend.data.friends.length; j++) {
                        if (userfriend.data.friends[j].status == 1 && userfriend.data.friends[j].is_blocked == 0)
                            friends.push(mongoose.Types.ObjectId(userfriend.data.friends[j].user_id));
                    }
                } else {
                    throw userfriend;
                }
                /* filters */
                if (req.body.search_text) {
                    filter["title"] = new RegExp('^' + req.body.search_text + '.*', 'i');
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (req.body.cuisine) {
                        filter["cuisine"] = { "$in": JSON.parse(req.body.cuisine) };
                    }
                    if (req.body.price) {
                        //between 1 to 4
                        filter["price_tier"] = { "$in": JSON.parse(req.body.price) };
                    }
                } else if (req.body.category == 3 || req.body.category == 4) { // for movies and series

                    if (req.body.genre) {
                        filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                    }
                    if (req.body.release_year && req.body.category == 3) {
                        //from 0 to current year
                        let array = JSON.parse(req.body.release_year);
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.release_year && req.body.category == 4) {
                        //from 0 to current year
                        let array = JSON.parse(req.body.release_year);
                        if (array[0])
                            filter["start_year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["start_year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.duration) {
                        // for movie duraion
                        let array = JSON.parse(req.body.duration);
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                } else if (req.body.category == 5) {
                    // for books
                    if (req.body.pages) {
                        // for movie duraion
                        let array = JSON.parse(req.body.pages);
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                    if (req.body.publish_year) {
                        //from 0 to current year
                        let array = JSON.parse(req.body.publish_year);
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.genre) {
                        filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                    }
                }
                if (req.body.rating) {
                    //between 1 to 5
                    let array = JSON.parse(req.body.rating);
                    if (array[0])
                        filter["rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };


                }
                if (req.body.friendspire_rating) {
                    //between 1 to 10
                    let array = JSON.parse(req.body.friendspire_rating);
                    if (array[0])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    var coordinates = JSON.parse(req.body["lat_long"]);
                    let distance;

                    if (req.body.distance) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                    } else {
                        //console.log("--------bookmark-------",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1)
                        {
                            distance = config.nearbyRaduisMiles;
                        } else {
                            distance = config.nearbyRaduisKm;
                        }
                        //distance = config.nearbyRaduis;
                    }
                    aggregation_pipeline = [
                        {
                            $geoNear: {
                                near: [coordinates[1], coordinates[0]],
                                distanceField: "distance",
                                query: filter,
                                distanceMultiplier: 6371000,
                                maxDistance: distance,
                                spherical: true
                            }
                        }
                    ]

                    if (req.body.filterLatLong) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                        if(distance_user.data.distance_unit == 1)
                        {
                            var distance_filterLatLong = config.nearbyRaduisMiles;
                        } else {
                            var distance_filterLatLong = config.nearbyRaduisKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance_filterLatLong,
                                    spherical: true
                                }
                            }
                        ]
                    }
                } else {
                    aggregation_pipeline = [
                        { "$match": filter }
                    ];
                    if (filterPagesDuration) {
                        aggregation_pipeline.push({ "$match": filterPagesDuration });
                    }
                }

                if (req.body.category == 1 || req.body.category == 2) {  //restau
                    if (req.body.open_now == 1) {
                        let time = momenttz().tz(timezone);
                        let day_of_week = time.isoWeekday() - 1;
                        let inttime = parseInt(time.format("HHmm"));
                        var new_date = momenttz.tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                        var agg = [
                            {
                                $project: {
                                    timezone: 1,
                                    //                                    date1: {$toInt: "$date1"},
                                    dayOfWeek: 1,
                                    timings1: '$timings',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    timings: { $arrayElemAt: ['$timings', day_of_week] }
                                }
                            },
                            {
                                $match: { timings: { $ne: undefined } }
                            },
                            {
                                $project: {
                                    timezone: 1,
                                    date1: 1,
                                    dayOfWeek: 1,
                                    timings: '$timings1',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    temp: {
                                        $filter: {
                                            input: '$timings',
                                            as: "num",
                                            cond: {
                                                $and: [
                                                    { $gte: [inttime, { $toInt: "$$num.start" }] },
                                                    { $lte: [inttime, { $toInt: "$$num.end" }] },
                                                ]
                                            }
                                        }
                                    }
                                }

                            },

                            {
                                $match: {
                                    'temp.0': { $exists: true }
                                }
                            }
                        ];
                        aggregation_pipeline = aggregation_pipeline.concat(agg);
                    }
                }

                let commonAggregaion = [
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: null, count: { $sum: 1 } } },
                                { "$project": { _id: 0, count: 1 } },
                            ],
                            as: "bookmark"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 0, rating: 1 } },
                            ],
                            as: "user_rating"
                        }
                    },
                    { "$sort": sort }, // Latest first
                    { "$skip": (page - 1) * global.pagination_limit },
                    { "$limit": global.pagination_limit },
                    { "$project": projection }
                ];
                aggregation_pipeline = aggregation_pipeline.concat(commonAggregaion);
                let result = post.aggregate.sync(null, aggregation_pipeline);
                // console.log(result);
                if (result.status == 1) {

                    result["data"] = result["data"].map(obj => {
                        if (req.body.category == 1 || req.body.category == 2) {
                            let closed_at = 0;
                            let is_open = 2;
                            if (obj["timezone"] && obj["timings"].length > 0) {
                                let time = momenttz().tz(obj["timezone"]);
                                let timing = obj["timings"][time.isoWeekday() - 1];
                                let inttime = time.format("HHmm");
                                timing.forEach(object => {
                                    closed_at = parseInt(object["end"]) > closed_at ? parseInt(object["end"]) : closed_at;
                                    if (parseInt(object["start"]) <= inttime && inttime <= parseInt(object["end"])) {
                                        is_open = 1;
                                    }
                                    //                                    let endTime;
                                    //                                    if (object["end"].charAt(0) == "+") {
                                    //                                        endTime = 2400 + parseInt(object["end"]);
                                    //                                    } else {
                                    //                                        endTime = parseInt(object["end"]);
                                    //                                    }
                                    //
                                    //                                    if (parseInt(object["start"]) <= inttime && inttime <= endTime) {
                                    //                                        is_open = 1;
                                    //                                    }
                                    //
                                    //                                    closed_at = endTime > closed_at ? endTime : closed_at;
                                    //                                    if (closed_at >= 2400) {
                                    //                                        closed_at = closed_at - 2400;
                                    //                                    }
                                });
                                obj["closed_at"] = moment(closed_at.toString(), "HHmm").format("HH:mm");

                                delete obj["timings"];
                                delete obj["timezone"];
                            } else {
                                is_open = 0;
                            }
                            obj["is_open"] = is_open;
                            if (obj["location"]) {
                                obj["location"] = [obj["location"]["coordinates"][1], obj["location"]["coordinates"][0]]
                            }
                        }
                        if (obj["bookmark"].length > 0) {
                            if (obj["bookmark"][0]["count"] > 0) {
                                obj["is_bookmarked"] = true;
                            }
                        } else {
                            obj["is_bookmarked"] = false;
                        }

                        if (obj["friends_rating"].length > 0) {
                            obj["friends_rating"] = parseFloat(obj["friends_rating"][0]["avgrating"]);
                        } else {
                            obj["friends_rating"] = parseFloat(0.00);
                        }

                        if (obj["user_rating"].length > 0) {
                            obj["user_rating"] = obj["user_rating"][0]["rating"];
                        } else {
                            obj["user_rating"] = 0.00
                        }
                        if (!obj["cuisine"]) {
                            obj["cuisine"] = [];
                        }
                        //                            if (obj["cuisine"].length < 1) {
                        //                                obj["cuisine"] = [];
                        //                            }

                        delete obj["bookmark"];
                        return obj;
                    });
                    res.status(200).send({ status: 1, message: "success", distance_unit: userfriend["data"]["distance_unit"], data: result["data"] });
                } else {
                    throw result;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});




module.exports = router;