var express = require('express');
var router = express.Router();
var posts = require('../controller/post.js');
var Sync = require('sync');
var request = require('request');
const mongoose = require("mongoose");
const foursquare = require('../helper/foursquare.js');
const goodreads = require('../helper/goodreads.js');
const omdb = require('../helper/omdb.js');
const foresquare = require("../helper/foursquare.js");
var comment = require('../controller/comment.js');
const config = require("../config");
var cuisine = require("../cuisine");
const projectionfield = require("../helper/projectionFields");
const util = require("util");
const users = require("../controller/users.js");
const recommendations = require("../controller/recommendations.js");
const moment = require('moment');
const momenttz = require('moment-timezone');
const bookmarks = require("../controller/bookmarks.js");
const friendsRating = require("../helper/friendsrating");
const ratingThirdParty = require("../helper/ratingUpgrade");
const prediciveThirdparty = require("../helper/thirdPartyPredictiveSearch");
const settings = require("../controller/settings");
router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            // console.log(req.query);
            if (req.query.category && req.query.search_text && req.query.page) {
                let searchCriteria = { title: req.query.search_text };
                searchCriteria["category"] = parseInt(req.query.category);
                searchCriteria["page"] = parseInt(req.query.page);

                //searchg the text on basis of category in db
                if (parseInt(req.query.category) == 1 || parseInt(req.query.category) == 2) {
                    if (req.query.lat_long == undefined) {
                        throw { message: "lat long not defined" };
                    } else {
                        let coords = JSON.parse(req.query.lat_long)
                        searchCriteria["latitude"] = coords[0];
                        searchCriteria["longitude"] = coords[1];
                    }
                }

                var post_expiry = settings.findOne.sync(null, {}, { record_expiry: 1 });
                //                console.log(post_expiry);
                if (post_expiry.status == 1) {


                    let today = new Date();
                    let exptime = new Date(today);
                    //                    console.log(today);
                    exptime.setHours(today.getHours() + parseInt(post_expiry["data"]["record_expiry"]));
                    //                let projection = projectionfield.project(req.query.category);
                    //                var condition = create_search_condition.sync(null, req, res, projection, "");
                    //
                    //                var search_result = posts.aggregate.sync(null, condition);
                    //                //record find send response to user
                    //                if (search_result.status == 1) {
                    //                    if (search_result.data.length > 0) {
                    //                        if (search_result["data"][0]["type"] === 1 || search_result["data"][0]["type"] === 2) {
                    //                            if (search_result["data"][0]["location"]) {
                    //                                if (search_result["data"][0]["location"]["coordinates"].length > 0) {
                    //                                    search_result["data"][0]["location"] = [search_result["data"][0]["location"]["coordinates"][1], search_result["data"][0]["location"]["coordinates"][0]]
                    //                                }
                    //                            }
                    //                            if (!search_result["data"][0]["cuisine"]) {
                    //                                search_result["data"][0]["cuisine"] = []
                    //                            }
                    //                        }
                    //                        res.status(200).send({message: "Success", status: 1, data: search_result.data[0]});
                    //                    } else {
                    //                        //search the data on basis of category using API
                    //                        let responceapi = search_from_api.sync(null, req, res);
                    //                        if (responceapi.status == 1) {
                    //                            // check with unique key that record exist in db 
                    //                            let result = responceapi.data;
                    //                            result["rated_at"] = new Date();
                    //                            result["expiry_time"] = exptime.getTime();
                    //                            let updateres = posts.updateOrSave.sync(null, {reference_id: result["reference_id"], type: req.query.category, expiry_time: {$gte: today.getTime()}}, result);
                    //                            if (updateres["status"] == 1) {
                    //                                //                                console.log(util.inspect(updateres, {depth: null}));
                    //                                //                                console.log(util.inspect(result, {depth: null}));
                    //                                //                                result["_id"] = updateres["data"]["_id"];
                    //                                updateres['data']["friends _rating"] = 0;
                    //                                //                                console.log(result);
                    //                                if (result["type"] == 1 || result["type"] == 2) {
                    //                                    //                                    updateres["data"]["location"] = [];
                    //                                    if (result["location"]) {
                    //                                        if (result["location"]["coordinates"].length > 0) {
                    //                                            let array = [result["location"]["coordinates"][1], result["location"]["coordinates"][0]];
                    //                                            Object.assign(updateres['data']["location"], array)
                    //                                        }
                    //                                    }
                    //                                    if (!result["cuisine"]) {
                    //                                        updateres["data"]["cuisine"] = []
                    //                                    }
                    //                                }
                    //                                //                                console.log(util.inspect(updateres, {depth: null}));
                    //                                res.status(200).send({message: "Success", status: 1, data: updateres["data"]});
                    //                            } else {
                    //                                throw updateres;
                    //                            }
                    //
                    //                            //                send response to the api
                    //                        } else if (responceapi.status == 2) {
                    //                            res.status(200).send({message: "No records found for the searched item", status: 0, data: {}});
                    //                        } else {
                    //                            throw responceapi;
                    //                        }
                    //                    }
                    //                } else {
                    //                    res.status(400).send({message: search_result.message, status: 0});
                    //                }
                    //                
                    var resThirdParty = prediciveThirdparty.thirdPartyPedictiveSearch.sync(null, searchCriteria);
                    //                console.log(resThirdParty);
                    if (resThirdParty.status === 1) {
                        var existingIds = posts.find_data.sync(null, { reference_id: { "$in": resThirdParty["reference_ids"] } }, { _id: 0, reference_id: 1 });
                        if (existingIds.status == 1 || existingIds.status == 2) {
                            var refIds = existingIds["data"].map(obj => {
                                return obj.reference_id
                            });
                            let resultArray = resThirdParty["result"].filter(obj => {
                                if (!refIds.includes(obj["reference_id"])) {
                                    obj["expiry_time"] = exptime.getTime();
                                    obj["dummy"] = 1;
                                    return obj;
                                }
                            });
                            var insertPosts = posts.saveMany.sync(null, resultArray);
                            //                        console.log(insertPosts);
                            if (insertPosts.status == 1) {
                                let projection = { type: 1, title: 1, author: 1, address: 1, year: 1, start_year: 1, end_year: 1, image: 1, city: 1, country: 1, rating: 1, distance: 1 };
                                let match_condition = { reference_id: { "$in": resThirdParty["reference_ids"] }, type: searchCriteria["category"] };
                                let aggregationcond = [];
                                if (searchCriteria["category"] == 1 || searchCriteria["category"] == 2) {
                                    aggregation_pipeline = [
                                        {
                                            $geoNear: {
                                                near: [searchCriteria["longitude"], searchCriteria["latitude"]],
                                                distanceField: "distance",
                                                query: match_condition,
                                                distanceMultiplier: 6371000,
                                                maxDistance: config.nearbyRaduis,
                                                spherical: true
                                            }
                                        },
                                        { $sort: { "distance": 1 } },
                                        { "$project": projection }
                                    ]
                                } else {
                                    aggregation_pipeline = [
                                        { "$match": match_condition },
                                        { "$project": projection }
                                    ];
                                }
                                //                            console.log(util.inspect(aggregation_pipeline,{depth:null}));
                                var finalresult = posts.aggregate.sync(null, aggregation_pipeline);
                                if (finalresult.status == 1) {
                                    res.status(200).send({ status: 1, message: "success", data: { max_pages: config.max_predictive_pages, result: finalresult["data"] } });
                                } else if (finalresult.status == 2) {
                                    res.status(200).send({ status: 1, message: "success", data: { max_pages: config.max_predictive_pages, result: [] } });
                                } else {
                                    throw finalresult;
                                }
                            } else {
                                throw insertPosts;
                            }
                        } else {
                            throw existingIds;
                        }
                    } else if (resThirdParty.status == 2) {
                        res.status(200).send({ status: 1, message: "success", data: { max_pages: config.max_predictive_pages, result: [] } })

                    } else {
                        throw resThirdParty;
                    }
                } else {
                    throw post_expiry;
                }
            } else {
                res.status(400).send({ status: 0, message: "Invalid request." })
            }
        } catch (err) //catch errors
        {
            //console.log(err);
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
})

router.get('/reviews', function (req, res, next) {
    Sync(function () {
        try {
            if (req.query.post_id && req.query.page_no) {
                var sep = 0;
                var skip;
                var notInUserId = [];
                var condition = { reference_id: mongoose.Types.ObjectId(req.query.post_id), is_deleted: 0 };
                var projection = { review: 1, rating: 1, user_id: 1, updated_at: 1 };
                var my_friends = [];
                var blockeduserarray = [];
                let userfriend = users.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { "friends": 1, "blocked_user": 1 });
                if (userfriend.status == 1) {
                    blockeduserarray = userfriend["data"]["blocked_user"].map(obj => {
                        return mongoose.Types.ObjectId(obj);
                    });
                    for (var j = 0; j < userfriend.data.friends.length; j++) {
                        if (userfriend.data.friends[j].status == 1 && userfriend.data.friends[j].is_blocked == 0)
                            my_friends.push(mongoose.Types.ObjectId(userfriend.data.friends[j].user_id));
                    }
                    //                     my_friends.push(req.decoded._id); // for user review
                }
                let usersblockedme = users.findData.sync(null, { blocked_user: { $elemMatch: { $eq: mongoose.Types.ObjectId(req.decoded._id) } } }, {});
                if (usersblockedme.status == 1) {
                    if (usersblockedme["data"].length > 0) {
                        usersblockedme["data"].forEach(obj => {
                            blockeduserarray.push(mongoose.Types.ObjectId(obj["_id"]));
                        });
                    }
                } else {
                    throw usersblockedme;
                }


                let count = recommendations.count.sync(null, { reference_id: req.query.post_id, is_deleted: 0, user_id: { $in: my_friends, $nin: blockeduserarray } });
                let friendPageCount = count["data"] / global.pagination_limit;
                //console.log(friendPageCount);
                if (friendPageCount > parseInt(friendPageCount)) {
                    friendPageCount = parseInt(friendPageCount) + 1;
                } else {
                    friendPageCount = parseInt(friendPageCount);
                }
                //console.log("friendPageCount");
                //console.log(friendPageCount);
                var aggpartial = [];
                if (friendPageCount >= req.query.page_no) {
                    //console.log("friends");
                    condition.user_id = { $in: my_friends, $nin: blockeduserarray };
                    if (req.query.page_no - friendPageCount === 0) {
                        sep = 1;
                    }
                    if (req.query.page_no) {
                        skip = (req.query.page_no - 1) * global.pagination_limit;
                    }
                    projection["friends"] = { "$literal": 1 };
                    aggpartial = [
                        { "$match": condition },
                        {
                            "$project": projection
                        }
                    ]

                } else {
                    //console.log("everyone");
                    //my_friends.push(mongoose.Types.ObjectId(req.decoded._id));
                    // condition.$or = [{ is_private: 0 }, { is_private: 1, user_id: { $nin: my_friends } }];
                    notInUserId = my_friends.concat(blockeduserarray);
                    condition.user_id = { $nin: notInUserId };
                    //condition.is_private = 0;
                    if (req.query.page_no) {
                        skip = (req.query.page_no - friendPageCount - 1) * global.pagination_limit;
                    }
                    projection["friends"] = { "$literal": 0 };
                    projection["is_private"] = {
                        $cond: [{
                            $and: [
                                { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] }
                            ]
                        },
                            0,
                            "$is_private"]
                    };
                    aggpartial = [
                        { "$match": condition },
                        {
                            "$project": projection
                        },
                        { $match: { is_private: 0 } },
                    ]

                    //console.log(my_friends);
                }
                //                let population = {
                //                    path: "user_id",
                //                    model: "user",
                //                    select: 'firstName lastName'
                //                };
                let sort = {
                    "updated_at": -1
                };
                //var list = recommendations.findAndPopulateLimitSortSkip.sync(null, condition, projection, population, global.pagination_limit, skip, sort);
                //console.log(condition);
                let new_aggregation = [
                    { $match: { post_id: mongoose.Types.ObjectId(req.query.post_id), is_deleted: 0, user_id: { $nin: notInUserId } } },
                    {
                        $lookup: {
                            from: "users",
                            localField: "user_id",
                            foreignField: "_id",
                            as: "commented_user_data"
                        }
                    },
                    { $unwind: "$commented_user_data" },
                    { $project: { recommendation_id: 1, comment: 1, user_id: 1, updated_at: 1, commented_user_data: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 } } },
                    { $group: { _id: "$recommendation_id", comments: { $push: { comment: '$comment', commented_user_data: "$commented_user_data", user_id: '$user_id', updated_at: '$updated_at' } } } },
                    {
                        $lookup: {
                            from: "recommendations",
                            localField: "_id",
                            foreignField: "_id",
                            as: "recommendation_data"
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            review: "$recommendation_data.review",
                            rating: "$recommendation_data.rating",
                            updated_at: "$recommendation_data.updated_at",
                            user_id: "$recommendation_data.user_id",
                            reference_id: "$recommendation_data.reference_id",
                            is_private: "$recommendation_data.is_private",
                            type: "$recommendation_data.type",
                            comment_data: "$comments"
                        }
                    },
                    { $unwind: "$review" },
                    { $unwind: "$rating" },
                    { $unwind: "$user_id" },
                    { $unwind: "$updated_at" },
                    { $unwind: "$reference_id" },
                    { $unwind: "$is_private" },
                    { $unwind: "$type" },
                    { $match: { user_id: { $nin: blockeduserarray } } },
                    {
                        $lookup: {
                            from: "users",
                            localField: "user_id",
                            foreignField: "_id",
                            as: "user_id"
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            review: 1,
                            rating: 1,
                            updated_at: 1,
                            user_id: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 },
                            reference_id: 1,
                            is_private: 1,
                            type: 1,
                            comment_data: 1
                        }
                    },
                    { $unwind: "$user_id" },
                ];



                let aggregationpartial2 = [
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$status", 1] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 } }
                            ],
                            as: "user_id"
                        }
                    },
                    { "$unwind": "$user_id" },
                    {
                        "$lookup": {
                            from: "comments",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$recommendation_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "comment_data"
                        }
                    },
                    { $unwind: { path: "$comment_data", "preserveNullAndEmptyArrays": true } },

                    {
                        $project: {
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            friends: 1,
                            comment_data: 1,
                            block_comment: {
                                $cond: {
                                    if: { $in: ["$comment_data.user_id", blockeduserarray] },
                                    then: 0,
                                    else: 1

                                }
                            },

                        },
                    },
                    {
                        "$group": {
                            _id: "$_id",
                            user_id: { $first: "$user_id" },
                            review: { $first: "$review" },
                            rating: { $first: "$rating" },
                            friends: { $first: "$friends" },
                            updated_at: { $first: "$updated_at" },
                            "comment": {
                                "$push": {
                                    "$cond": [
                                        { "$eq": ["$block_comment", 1] },
                                        "$comment_data",
                                        {}

                                    ]
                                }
                            }
                        }
                    },
                    {
                        $project: {
                            comment_data: {
                                $filter: {
                                    input: '$comment',
                                    as: 'item',
                                    cond: { $ne: ['$$item', {}] }
                                },
                            },
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            friends: 1,
                            //comment_data: 1
                        }
                    },
                    { $unwind: { path: "$comment_data", "preserveNullAndEmptyArrays": true } },
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$comment_data.user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] },
                                                    { $eq: ["$status", 1] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1, status: 1 } }
                            ],
                            as: "commented_user_data"
                        }
                    },
                    { $unwind: { path: "$commented_user_data", "preserveNullAndEmptyArrays": true } },
                    // { $match: { "commented_user_data.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) }, "commented_user_data._id": { $nin: req.decoded.blocked_user }, 'commented_user_data.status': { '$eq': 1 } } },
                    {
                        $project: {
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            friends: 1,
                            commented_user_data: {
                                _id: 1,
                                profile_pic: 1,
                                lastName: 1,
                                firstName: 1
                            },
                            comment_data: 1
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            friends: 1,
                            commented_user_data: {
                                _id: 1,
                                profile_pic: 1,
                                lastName: 1,
                                firstName: 1
                            },
                            comment_data: {
                                _id: 1,
                                updated_at: 1,
                                recommendation_id: 1,
                                user_id: 1,
                                comment: 1,
                                commented_user_data: "$commented_user_data"
                            }
                        }
                    },
                    // {
                    //     '$group':
                    //     {
                    //         _id: '$_id',
                    //         //commented_user_data: { '$first': '$commented_user_data' },
                    //         comment_data: { '$push': '$comment_data' },
                    //         user_id: { '$first': '$user_id' },
                    //         review: { '$first': '$review' },
                    //         rating: { '$first': '$rating' },
                    //         friends: { '$first': '$friends' },
                    //         updated_at: { '$first': '$updated_at' }
                    //     }
                    // },
                    {
                        "$group": {
                            _id: "$_id",
                            //comment_data: { $push: "$comment_data" },
                            comment_data: {
                                '$push': {
                                    $cond: [
                                        { $eq: ["$comment_data", {}] },
                                        null,
                                        "$comment_data"
                                    ]
                                }
                            },
                            user_id: { $first: "$user_id" },
                            review: { $first: "$review" },
                            rating: { $first: "$rating" },
                            friends: { $first: "$friends" },
                            updated_at: { $first: "$updated_at" },
                        }
                    },
                    {
                        '$project':
                        {
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            friends: 1,
                            comment_data: { $setDifference: ["$comment_data", [null]] }
                        }
                    },
                    { "$sort": sort }, // Latest first
                    { "$skip": skip },
                    { "$limit": global.pagination_limit },
                ]
                var aggregation = aggpartial.concat(aggregationpartial2);
                // console.log(util.inspect(aggregation, { depth: null }));
                var list = recommendations.aggregate.sync(null, aggregation);
                var comment_count = comment.count.sync(null, { recommendation_id: mongoose.Types.ObjectId(list.data[0]._id), is_deleted: 0 });
                //console.log(comment_count);
                list.data[0].comment_count = comment_count.data;
                //var list = comment.aggregate.sync(null, aggregation);
                // console.log(util.inspect(list, { depth: null }));
                if (list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1, partition: sep, data: list.data });
                } else {
                    res.status(400).send({ message: list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid Request", status: 0 });
            }

        } catch (err) // catch errors
        {
            console.log(err);
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.get('/detail', function (req, res, next) {
    Sync(function () {
        try {
            //console.log(req.decoded._id);
            if (req.query.category && req.query.postId) { //req.query.friend mans to show only friend review
                if (req.query.category == "1" || req.query.category == "2") {
                    if (req.query.lat_long == undefined) {
                        throw { message: "lat long not defined" };
                    }
                }
                var post_id = mongoose.Types.ObjectId(req.query.postId);
                let projection = projectionfield.project(req.query.category);
                let aggregation_condition = create_search_condition.sync(null, req, res, projection);
                var blockeduserarray = [];
                //                console.log(aggregation_condition);
                let post = posts.aggregate.sync(null, aggregation_condition);
                if (post.status == 1) {
                    if (post["data"].length > 0) {
                        post["data"] = post["data"][0];
                        //                    console.log("post);
                        let today = new Date();
                        let exptime = new Date(today);

                        /* for updated rating in database*/
                        if (post["data"]["expiry_time"] < today.getTime() || post["data"]["dummy"] == 1) {
                            var post_expiry = settings.findOne.sync(null, {}, { record_expiry: 1 });
                            //                console.log(post_expiry);
                            if (post_expiry.status == 1) {
                                exptime.setHours(today.getHours() + parseInt(post_expiry["data"]["record_expiry"]));
                                //                            let upgradeRating = ratingThirdParty.getNewRating.sync(null, parseInt(req.query.category), post["data"]["reference_id"])
                                //                            if (upgradeRating["status"] === 1) {
                                //                                upgradeRating["data"]["expiry_time"] = exptime.getTime();
                                //                                let ratingUpgradeRes = posts.update.sync(null, {_id: post_id}, upgradeRating["data"]);
                                //                                if (ratingUpgradeRes["status"] == 1) {
                                //                                    post["data"]["rating"] = upgradeRating["data"]["rating"];
                                //                                    if (req.query.category == 3 && req.query.category == 4) {
                                //                                        post["data"]["rated"] = upgradeRating["data"]["rated"];
                                //                                        post["data"]["awards"] = upgradeRating["data"]["awards"];
                                //                                        post["data"]["expiry_time"] = exptime.getTime();
                                //                                    }
                                //                                
                                //                                
                                //                                } else {
                                //                                    throw ratingUpgradeRes;
                                //                                }
                                //                            } else {
                                //                                throw upgradeRating;
                                //                            }
                                //                        console.log("here");
                                //                        console.log(post["data"]["reference_id"]);
                                //                            console.log("here");
                                var resultThirdParty = search_from_api.sync(null, post["data"]["reference_id"], req.query.category);
                                //                        console.log("resultThirdParty");
                                //                        console.log(resultThirdParty);
                                if (resultThirdParty.status === 1) {
                                    resultThirdParty["data"]["dummy"] = 0;
                                    resultThirdParty["data"]["expiry_time"] = exptime.getTime();
                                    var updateres = posts.update.sync(null, { _id: post_id }, resultThirdParty["data"]);
                                    //                            console.log("updateres");
                                    //                            console.log(updateres);

                                    //update rating only
                                    //                                var update_json = {};
                                    //                                if (post["data"]["expiry_time"] < today.getTime() && post["data"]["dummy"] != 1) {
                                    //                                    update_json.rating = resultThirdParty["data"]["rating"]
                                    //                                    update_json.expiry_time = exptime.getTime();
                                    //                                } else if (post["data"]["dummy"] == 1 && post["data"]["expiry_time"] > today.getTime()) {
                                    //                                    update_json = resultThirdParty["data"];
                                    //                                    update_json.dummy = 0;
                                    //                                } else {
                                    //                                    update_json = resultThirdParty["data"];
                                    //                                    update_json.dummy = 0;
                                    //                                    update_json.expiry_time = exptime.getTime();
                                    //                                }
                                    //                                var updateres = posts.update.sync(null, {_id: post_id}, update_json);
                                    if (updateres.status == 1) {
                                        post = posts.aggregate.sync(null, aggregation_condition);
                                        if (post.status == 1) {
                                            post["data"] = post["data"][0];
                                        } else {
                                            throw post;
                                        }
                                    } else {
                                        throw updateres;
                                    }
                                } else {
                                    throw resultThirdParty;

                                }
                            } else {
                                throw post_expiry;
                            }
                        }
                        //                    /*fuction for rest and bar open close*/
                        if (post["data"]["type"] === 1 || post["data"]["type"] === 2) {
                            post["data"]["reference_url"] = global.foursquare_url + post["data"]["reference_id"];
                            //console.log("-------------",post);
                            var is_open = 2;
                            if (post["data"]["timezone"] && post["data"]["timings"].length > 0) {
                                let time = momenttz().tz(post["data"]["timezone"]);
                                let timing = post["data"]["timings"][time.isoWeekday() - 1];
                                let inttime = parseInt(time.format("HHmm"));
                                timing.forEach(obj => {
                                    //                                    let endTime;
                                    //                                    if (obj["end"].charAt(0) == "+") {
                                    //                                        endTime = 2400 + parseInt(obj["end"]);
                                    //                                    } else {
                                    //                                        endTime = parseInt(obj["end"]);
                                    //                                    }
                                    //
                                    //                                    if (parseInt(obj["start"]) <= inttime && inttime <= endTime) {
                                    //                                        is_open = 1;
                                    //                                    }
                                    if (parseInt(obj["start"]) <= inttime && inttime <= parseInt(obj["end"])) {
                                        is_open = 1;
                                    }
                                });
                            } else {
                                is_open = 0
                            }
                            post["data"]["is_open"] = is_open;
                            //                            console.log(post["data"]["location"]);
                            post["data"]["location"] = [post["data"]["location"]["coordinates"][1], post["data"]["location"]["coordinates"][0]]
                            if (!post["data"]["cuisine"]) {
                                post["data"]["cuisine"] = [];
                            }
                        }
                        let match = { "reference_id": post_id, is_deleted: 0 };
                        let limit = 6;
                        let userfriend = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 }, { profile_pic: 1, "friends": 1, distance_unit: 1, blocked_user: 1 });
                        if (userfriend.status == 1) {
                            var usersWhoBlockMe = users.find.sync(null, { blocked_user: { $eq: mongoose.Types.ObjectId(req.decoded._id) }, is_deleted: 0, status: 1 }, { _id: 1 });
                            //console.log(usersWhoBlockMe);
                            var usersWhoBlockMe_array = [];
                            for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                                usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                            }
                            //console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);
                            //                            var friends = userfriend.data.friends.map(obj => mongoose.Types.ObjectId(obj.user_id));
                            //user blocked by me
                            if (userfriend["data"]["blocked_user"]) {
                                blockeduserarray = userfriend["data"]["blocked_user"].map(obj => {
                                    return mongoose.Types.ObjectId(obj);
                                });
                            }

                            var friends = [];
                            var friend_without_me = [];
                            for (var j = 0; j < userfriend.data.friends.length; j++) {
                                if (userfriend.data.friends[j].status == 1 && userfriend.data.friends[j].is_blocked == 0) {
                                    friends.push(mongoose.Types.ObjectId(userfriend.data.friends[j].user_id));
                                    friend_without_me.push(mongoose.Types.ObjectId(userfriend.data.friends[j].user_id));
                                }
                            }
                            //console.log("friend_without_me", friend_without_me);
                            //var try_array = [];
                            var try_array = friend_without_me.filter(f => {
                                if (usersWhoBlockMe_array.includes(f.toString())) {
                                    return false;
                                } else {
                                    return true;
                                }
                            });
                            //console.log("try_array", try_array);

                            //user who blocked me
                            let usersblockedme = users.findData.sync(null, { blocked_user: { $elemMatch: { $eq: mongoose.Types.ObjectId(req.decoded._id) } } }, {});
                            if (usersblockedme.status == 1) {
                                if (usersblockedme["data"].length > 0) {
                                    usersblockedme["data"].forEach(obj => {
                                        blockeduserarray.push(mongoose.Types.ObjectId(obj["_id"]));
                                    });
                                }
                            } else {
                                throw usersblockedme;
                            }
                            var user_detail = userfriend;
                            //                        console.log(friends);
                            //friends.push(mongoose.Types.ObjectId(req.decoded._id));

                            match["user_id"] = { "$in": friends, "$nin": blockeduserarray };
                            //                            match["user_id"] = {"$in": friends};
                        } else {
                            throw userfriend;
                        }
                        let population = {
                            path: "user_id",
                            model: "user",
                            select: 'firstName lastName profile_pic'
                        };
                        let sort = {
                            "updated_at": -1
                        };
                        var common_review_agg = [
                            { $project: { review: 1, rating: 1, user_id: 1, updated_at: 1 } },
                            {
                                $lookup: {
                                    "from": "users",
                                    "localField": "user_id",
                                    "foreignField": "_id",
                                    "as": "user_id"
                                }
                            },
                            {
                                '$lookup':
                                {
                                    from: 'comments',
                                    let: { ref_id: '$_id' },
                                    pipeline:
                                        [{
                                            '$match':
                                            {
                                                '$expr':
                                                {
                                                    '$and':
                                                        [{ '$eq': ['$recommendation_id', '$$ref_id'] },
                                                        { '$eq': ['$is_deleted', 0] }]
                                                }
                                            }
                                        },
                                        ],
                                    as: 'comment_data'
                                }
                            },
                            { $unwind: "$user_id" },
                            {
                                $project: {
                                    _id: 1,
                                    updated_at: 1,
                                    user_id: {
                                        firstName: 1,
                                        lastName: 1,
                                        profile_pic: 1,
                                        _id: 1
                                    },
                                    review: 1,
                                    rating: 1,
                                    comment_count: { $size: "$comment_data" }
                                }
                            },
                            { $sort: sort },
                            { $skip: 0 },


                        ];
                        var match_agg = [{
                            $match: match
                        }];
                        var limit_agg = [{ $limit: limit }];
                        var half_review_agg = match_agg.concat(common_review_agg);
                        var review_agg = half_review_agg.concat(limit_agg);
                        //console.log(util.inspect(review_agg, { depth: null }));
                        //let reviews = recommendations.findAndPopulateLimitSortSkip.sync(null, match, { review: 1, rating: 1, user_id: 1, updated_at: 1 }, population, limit, 0, sort);
                        let reviews = recommendations.aggregate.sync(null, review_agg);
                        var recommend_count = recommendations.count.sync(null, { reference_id: req.query.postId, is_deleted: 0 });
                        //console.log("recommend_count",recommend_count.data);
                        // console.log("--------------------reviews.data---------------------");
                        // console.log(reviews.data);
                        post["data"]["everyone_count"] = recommend_count["data"];

                        let array = [];
                        if (reviews["data"].length >= limit) {
                            post["data"]["reviews"] = reviews["data"];
                        } else {
                            let new_match = { "reference_id": post_id, is_deleted: 0 };
                            limit = limit - reviews["data"].length;
                            if (reviews["data"].length) {
                                array = array.concat(reviews["data"]);
                            }
                            friends.push(mongoose.Types.ObjectId(req.decoded._id));
                            // list of user id
                            let notInUserid = friends.concat(blockeduserarray);
                            new_match["user_id"] = { "$nin": notInUserid };
                            //                            new_match["user_id"] = {"$nin": friends};
                            new_match["is_private"] = 0;
                            var new_match_agg = [{
                                $match: new_match
                            }];
                            var new_limit_agg = [{ $limit: limit }];
                            var new_half_review_agg = new_match_agg.concat(common_review_agg);
                            var new_review_agg = new_half_review_agg.concat(new_limit_agg);
                            //console.log(util.inspect(new_review_agg, { depth: null }));
                            //let reviewsf = recommendations.findAndPopulateLimitSortSkip.sync(null, new_match, { review: 1, rating: 1, user_id: 1, updated_at: 1 }, population, limit, 0, sort);
                            let reviewsf = recommendations.aggregate.sync(null, new_review_agg);
                            if (reviewsf["data"].length > 0) {
                                array = array.concat(reviewsf["data"])
                            }
                            post["data"]["reviews"] = array;
                        }
                        //                        console.log("friend_without_me", friend_without_me);
                        let p1 = bookmarks.count({ user_id: mongoose.Types.ObjectId(req.decoded._id), reference_id: post_id, status: 1 });
                        let p2 = recommendations.findOnePromise({ user_id: mongoose.Types.ObjectId(req.decoded._id), reference_id: post_id, is_deleted: 0 }, { review: 1, rating: 1, updated_at: 1 });
                        let p3 = friendsRating.calculate(try_array, post_id);
                        Promise.all([p1, p2, p3]).then(([bookmark, userreview, friendsrating]) => {
                            //console.log(userreview);
                            //console.log("friendsrating",friendsrating);
                            //console.log("friendsrating",friendsrating[0]);
                            //console.log("friendsrating",friendsrating[1]);
                            if (bookmark > 0) {
                                post["data"]["is_bookmarked"] = true;
                            } else {
                                post["data"]["is_bookmarked"] = false
                            }
                            post["data"]["friends_rating"] = parseFloat(friendsrating[0]);
                            post["data"]["friends_count"] = parseInt(friendsrating[1]);
                            post["data"]["profile_pic"] = userfriend["data"]["profile_pic"];
                            post["distance_unit"] = user_detail["data"]["distance_unit"];
                            //console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", post);
                            if (userreview) {
                                post["data"]["user_review"] = userreview["review"];
                                post["data"]["user_rating"] = userreview["rating"];
                                post["data"]["user_updated_at"] = userreview["updated_at"];
                                post["data"]["user_recommendation_id"] = userreview["_id"];
                                var comment_count = comment.count_promise({ recommendation_id: mongoose.Types.ObjectId(userreview._id), is_deleted: 0 });
                                comment_count.then((count) => {
                                    //console.log(count);
                                    post["data"]["comment_count_of_user_review"] = count;
                                    res.status(200).send(post);
                                });
                            } else {
                                post["data"]["user_review"] = "";
                                post["data"]["user_rating"] = 0;
                                post["data"]["comment_count_of_user_review"] = 0;
                                res.status(200).send(post);
                            }
                        }).catch(error => {
                            throw error
                        });

                    } else {
                        res.status(400).send({ status: 0, data: {}, message: "no result found" })
                    }
                } else {
                    throw post;
                }

            } else {
                res.status(400).send({ message: "invalid request", status: 0 });
            }
        } catch (err) //catch errors
        {
            console.log(err);
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
var list_validate = (req, res, next) => {
    //add rules for validations
    if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
        req.assert("lat_long").notEmpty().withMessage('Please enter lat_long');
    }
    req.assert("category").notEmpty().withMessage('Please enter category');
    req.assert("friends").notEmpty().withMessage('Please enter friends key');
    if (req.body.map != 1) {
        req.assert("page").notEmpty().withMessage('Please enter page number');
    }
    return req.validationErrors(true);
};
router.post("/", (req, res, next) => {
    let errors = list_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            //            console.log(req.decoded._id);
            try {
                req.body.distance = parseFloat(req.body.distance);
                var timezone = req.headers.timezone;
                // console.log(req.body);
                var filter = { type: parseInt(req.body.category), dummy: 0 };
                var filterPagesDuration;
                var page = req.body.page;
                var aggregation_pipeline = [];
                var matchrecommend = { type: parseInt(req.body.category) };
                let projection = projectionfield.projectlist(req.body.category.toString());
                var ref_ids = [];
                //                var blockeduserarray = [];
                var sort;
                filter["friendspire_rating"] = { "$gt": 0 };
                if (req.body.friends == 1) {
                    sort = { friends_rating: -1, count_friends: -1, _id: 1 };
                } else {
                    sort = { friendspire_rating: -1, everyone_count: -1, _id: 1 };
                }
                //var sort = { "updated_at": -1 };
                // console.time("---1---");
                var userfriend = users.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { "friends": 1, distance_unit: 1, blocked_user: 1 });
                // console.timeEnd("---1---");

                if (userfriend.status === 1) {
                    //                    var friends = userfriend.data.friends.map(obj => mongoose.Types.ObjectId(obj.user_id));
                    //                    blockeduserarray = userfriend["data"]["blocked_user"].map(obj => {
                    //                        return mongoose.Types.ObjectId(obj);
                    //                    })
                    var friends = [];

                    for (var j = 0; j < userfriend.data.friends.length; j++) {
                        if (userfriend.data.friends[j].status == 1 && userfriend.data.friends[j].is_blocked == 0)
                            friends.push(mongoose.Types.ObjectId(userfriend.data.friends[j].user_id.toString()));
                    }
                } else {
                    throw userfriend;
                }
                // console.time("---2---");
                var usersWhoBlockMe = users.find.sync(null, { blocked_user: { $eq: mongoose.Types.ObjectId(req.decoded._id) }, is_deleted: 0, status: 1 }, { _id: 1 });
                // console.timeEnd("---2---");
                //console.log(usersWhoBlockMe);
                var usersWhoBlockMe_array = [];
                for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                    usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                }
                // console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);

                friends = friends.filter(f => {
                    if (usersWhoBlockMe_array.includes(f.toString())) {
                        return false;
                    } else {
                        return true;
                    }
                });
                //console.log("try_array", try_array);

                // console.time("---3---");
                /* filters */
                if (req.body.search_text) {
                    filter["title"] = new RegExp('^' + req.body.search_text + '.*', 'i');
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (req.body.cuisine) {
                        if (Array.isArray(req.body.cuisine)) {
                            // console.log("req.body.cuisine", req.body.cuisine);
                            // console.log("cuisine.other_cuisine", cuisine.other_cuisine);
                            // console.log(req.body.cuisine.includes("Other"));
                            if (req.body.cuisine.includes("Other")) {
                                var index = req.body.cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    req.body.cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", req.body.cuisine);
                                req.body.cuisine = req.body.cuisine.concat(cuisine.other_cuisine);
                            }
                            // console.log("--------------------", req.body.cuisine);
                            filter["cuisine"] = { "$in": req.body.cuisine };
                        } else {
                            var array_cuisine = JSON.parse(req.body.cuisine);
                            // console.log(array_cuisine);
                            if (array_cuisine.includes("Other")) {
                                var index = array_cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    array_cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", array_cuisine);
                                array_cuisine = array_cuisine.concat(cuisine.other_cuisine);
                            }
                            // console.log("--------------------", array_cuisine);
                            filter["cuisine"] = { "$in": array_cuisine };
                        }
                    }
                    if (req.body.price) {
                        //between 1 to 4
                        if (Array.isArray(req.body.price)) {
                            filter["price_tier"] = { "$in": req.body.price };
                        } else {
                            filter["price_tier"] = { "$in": JSON.parse(req.body.price) };
                        }
                    }
                } else if (req.body.category == 3 || req.body.category == 4) { // for movies and series

                    if (req.body.genre) {
                        if (Array.isArray(req.body.genre)) {
                            filter["genre"] = { "$in": req.body.genre };
                        } else {
                            filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                        }
                    }
                    if (req.body.release_year && req.body.category == 3) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.release_year && req.body.category == 4) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        if (array[0])
                            filter["start_year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["start_year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.duration) {
                        // for movie duraion
                        if (Array.isArray(req.body.duration)) {
                            var array = req.body.duration;
                        } else {
                            var array = JSON.parse(req.body.duration);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                } else if (req.body.category == 5) {
                    // for books
                    if (req.body.pages) {
                        // for movie duraion
                        if (Array.isArray(req.body.pages)) {
                            var array = req.body.pages;
                        } else {
                            var array = JSON.parse(req.body.pages);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                    if (req.body.publish_year) {
                        //from 0 to current year
                        if (Array.isArray(req.body.publish_year)) {
                            var array = req.body.publish_year;
                        } else {
                            var array = JSON.parse(req.body.publish_year);
                        }
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.genre) {
                        filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                    }
                }
                // console.timeEnd("---3---");
                if (req.body.rating) {
                    //between 1 to 5
                    if (Array.isArray(req.body.rating)) {
                        var array = req.body.rating;
                    } else {
                        var array = JSON.parse(req.body.rating);
                    }
                    if (array[0])
                        filter["rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };
                }
                if (req.body.friendspire_rating) {
                    //between 1 to 10
                    if (Array.isArray(req.body.friendspire_rating)) {
                        var array = req.body.friendspire_rating;
                    } else {
                        var array = JSON.parse(req.body.friendspire_rating);
                    }
                    if (array[0])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };
                }

                if (req.body.remove_rated == 1) {
                    let result = recommendations.findWithProjection.sync(null, { "user_id": mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0 }, { reference_id: 1, _id: 0 });
                    if (result["data"].length > 0) {
                        let ids = result["data"].map(obj => mongoose.Types.ObjectId(obj["reference_id"]));
                        filter["_id"] = { "$nin": ids };
                    }
                }

                if (req.body.friends == 1) {
                    matchrecommend["is_deleted"] = 0;
                    matchrecommend["user_id"] = { "$in": friends };
                    let lookup = {
                        from: "posts",
                        localField: "_id",
                        foreignField: "_id",
                        as: "post"
                    }
                    aggregation_pipeline = [
                        { "$match": matchrecommend },
                        { "$group": { _id: "$reference_id" } },
                    ];
                    let res = recommendations.aggregate.sync(null, aggregation_pipeline);
                    if (res.status == 1) {
                        ref_ids = res["data"].map(obj => mongoose.Types.ObjectId(obj._id));
                        //                        console.log(util.inspect(ref_ids, {depth: null}));
                        if (filter["_id"]) {
                            Object.assign(filter["_id"], { "$in": ref_ids });
                        } else {
                            filter["_id"] = { "$in": ref_ids }
                        }
                    }

                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (Array.isArray(req.body["lat_long"])) {
                        var coordinates = req.body["lat_long"];
                    } else {
                        var coordinates = JSON.parse(req.body["lat_long"]);
                    }
                    let distance;
                    if (req.body.distance) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                    } else {
                        //console.log(userfriend.data.distance_unit);
                        if(userfriend.data.distance_unit == 1){
                            distance = config.nearbyRaduisMiles;
                        } else {
                            distance = config.nearbyRaduisKm;
                        }   
                    }

                    if (req.body.map) {
                        if (req.body.distance) {
                            distance = parseFloat(req.body.distance) / 6371000; //in meters
                        } else {
                            //console.log("-------req.body.map----",userfriend.data.distance_unit);
                            if(userfriend.data.distance_unit == 1){
                                distance = config.nearbyRaduisMiles;
                            } else {
                                distance = config.nearbyRaduisKm;
                            }
                        }
                        //distance = config.nearbyRaduisMap;
                    }
                    aggregation_pipeline = [
                        {
                            $geoNear: {
                                near: [coordinates[1], coordinates[0]],
                                distanceField: "distance",
                                query: filter,
                                distanceMultiplier: 6371000,
                                maxDistance: distance,
                                spherical: true
                            }
                        }
                    ]
                } else {
                    aggregation_pipeline = [
                        { "$match": filter }
                    ];
                    if (filterPagesDuration) {
                        aggregation_pipeline.push({ "$match": filterPagesDuration });
                    }
                }

                if (req.body.category == 1 || req.body.category == 2) {  //restau
                    if (req.body.open_now == 1) {
                        let time = momenttz().tz(timezone);
                        let day_of_week = time.isoWeekday() - 1;
                        let inttime = parseInt(time.format("HHmm"));
                        var new_date = momenttz.tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                        var agg = [
                            { "$match": { timezone: timezone } },
                            {
                                $project: {
                                    timezone: 1,
                                    //                                    date1: {$toInt: "$date1"},
                                    dayOfWeek: 1,
                                    timings1: '$timings',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    timings: { $arrayElemAt: ['$timings', day_of_week] }
                                }
                            },
                            {
                                $match: { timings: { $ne: undefined } }
                            },
                            {
                                $project: {
                                    timezone: 1,
                                    date1: 1,
                                    dayOfWeek: 1,
                                    timings: '$timings1',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    temp: {
                                        $filter: {
                                            input: '$timings',
                                            as: "num",
                                            cond: {
                                                $and: [
                                                    { $gte: [inttime, { $toInt: "$$num.start" }] },
                                                    { $lte: [inttime, { $toInt: "$$num.end" }] },
                                                ]
                                            }
                                        }
                                    }
                                }

                            },

                            {
                                $match: {
                                    'temp.0': { $exists: true }
                                }
                            }
                        ];
                        aggregation_pipeline = aggregation_pipeline.concat(agg);
                    }
                }

                let commonAggregaion = [
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: null, count: { $sum: 1 } } },
                                { "$project": { _id: 0, count: 1 } },
                            ],
                            as: "bookmark"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", avgrating: { $avg: "$rating" }, count_friends: { $sum: 1 } } },
                                { "$project": { _id: 0, avgrating: 1, count_friends: 1 } },
                            ],
                            as: "friends_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 0, rating: 1 } },
                            ],
                            as: "user_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", count_everyone: { $sum: 1 } } },
                                { "$project": { _id: 0, count_everyone: 1 } },
                            ],
                            as: "everyone_count"
                        }
                    },
                    // {
                    //     $match: {
                    //             $and: [
                    //                 { "friendspire_rating": { $ne: 0 } }
                    //                 //{ "friends_rating.avgrating": { $ne: 0 } }
                    //             ]
                    //         }
                    // },
                    { "$sort": sort }, // Latest first
                    //                    {"$skip": (page - 1) * global.pagination_limit},
                    //                    {"$limit": global.pagination_limit},
                    { "$project": projection }
                ];
                //                let countAggregation = commonAggregaion;
                if (req.body.map != 1 || req.body.map == undefined) {
                    commonAggregaion.push({ "$skip": (page - 1) * global.pagination_limit });
                    commonAggregaion.push({ "$limit": global.pagination_limit });
                }

                let countAggregation = [
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: null, count: { $sum: 1 } } },
                                { "$project": { _id: 0, count: 1 } },
                            ],
                            as: "bookmark"
                        }
                    },
                    // {
                    //     "$lookup": {
                    //         from: "recommendations",
                    //         let: { ref_id: "$_id" },
                    //         pipeline: [
                    //             {
                    //                 $match:
                    //                 {
                    //                     $expr:
                    //                     {
                    //                         $and:
                    //                             [
                    //                                 { $eq: ["$reference_id", "$$ref_id"] },
                    //                                 { $in: ["$user_id", friends] },
                    //                                 { $eq: ["$is_deleted", 0] },
                    //                             ]
                    //                     }
                    //                 }
                    //             },
                    //             { "$group": { _id: "$reference_id", avgrating: { $avg: "$rating" } } },
                    //             { "$project": { _id: 0, avgrating: 1 } },
                    //         ],
                    //         as: "friends_rating"
                    //     }
                    // },
                    // {
                    //     "$lookup": {
                    //         from: "recommendations",
                    //         let: { ref_id: "$_id" },
                    //         pipeline: [
                    //             {
                    //                 $match:
                    //                 {
                    //                     $expr:
                    //                     {
                    //                         $and:
                    //                             [
                    //                                 { $eq: ["$reference_id", "$$ref_id"] },
                    //                                 { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                    //                                 { $eq: ["$is_deleted", 0] },
                    //                             ]
                    //                     }
                    //                 }
                    //             },
                    //             { "$project": { _id: 0, rating: 1 } },
                    //         ],
                    //         as: "user_rating"
                    //     }
                    // },
                    //{ "$sort": sort }, // Latest first
                    //{ "$sort": { "updated_at": -1 } }, // Latest first
                    {
                        $group: {
                            _id: null,
                            count: { $sum: 1 }
                        }
                    }
                ];

                var countAggregationPipeline = aggregation_pipeline;
                countAggregationPipeline = countAggregationPipeline.concat(countAggregation);
                //console.log(util.inspect(countAggregationPipeline, {depth: null}));
                //                countAggregationPipeline = countAggregationPipeline.concat(commonAggregaion);
                //                countAggregationPipeline.push({
                //                    $group: {
                //                        _id: null,
                //                        count: {$sum: 1}
                //                    }
                //                });
                // console.time("---4---");
                var countpost = posts.aggregate.sync(null, countAggregationPipeline);
                //console.log(util.inspect(countpost, {depth: null}));
                // console.timeEnd("---4---");

                if (countpost.status === 0) {
                    throw countpost;
                }
                var count;
                if (countpost["data"].length > 0) {
                    var count = countpost["data"][0]["count"];
                } else {
                    var count = 0;
                }

                //                console.log(filter);
                //                console.log(commonAggregaion);
                // console.time("---5---");
                aggregation_pipeline = aggregation_pipeline.concat(commonAggregaion);
                // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                // console.log(util.inspect(aggregation_pipeline, { depth: null }));
                let result = posts.aggregate.sync(null, aggregation_pipeline);
                // console.timeEnd("---5---");
                // console.log("result");
                // console.log(util.inspect(result, { depth: null }));
                if (result.status == 1) {
                    // console.time("---6---");
                    result["data"] = result["data"].map(obj => {
                        if (req.body.category == 1 || req.body.category == 2) {
                            let closed_at = 0;
                            let is_open = 2;
                            if (obj["timezone"] && obj["timings"].length > 0) {
                                //                                console.log(obj["timings"]);
                                let time = momenttz().tz(obj["timezone"]);
                                //                                console.log(time);
                                let timing = obj["timings"][time.isoWeekday() - 1];
                                let inttime = time.format("HHmm");
                                //                                console.log(inttime);
                                timing.forEach(object => {
                                    closed_at = parseInt(object["end"]) > closed_at ? parseInt(object["end"]) : closed_at;
                                    if (parseInt(object["start"]) <= inttime && inttime <= parseInt(object["end"])) {
                                        is_open = 1;
                                    }
                                    //                                    let endTime;
                                    //                                    if (object["end"].charAt(0) == "+") {
                                    //                                        endTime = 2400 + parseInt(object["end"]);
                                    //                                    } else {
                                    //                                        endTime = parseInt(object["end"]);
                                    //                                    }
                                    //
                                    //                                    if (parseInt(object["start"]) <= inttime && inttime <= endTime) {
                                    //                                        is_open = 1;
                                    //                                    }
                                    //                                    closed_at = endTime > closed_at ? endTime : closed_at;
                                    //                                    if (closed_at >= 2400) {
                                    //                                        closed_at = closed_at - 2400;
                                    //                                    }
                                });
                                obj["closed_at"] = moment(closed_at.toString(), "HHmm").format("HH:mm");


                                delete obj["timings"];
                                delete obj["timezone"];
                            } else {
                                is_open = 0;
                            }
                            obj["is_open"] = is_open;

                            if (obj["location"]) {
                                obj["location"] = [obj["location"]["coordinates"][1], obj["location"]["coordinates"][0]]
                            }
                        }
                        if (obj["bookmark"].length > 0) {
                            if (obj["bookmark"][0]["count"] > 0) {
                                obj["is_bookmarked"] = true;
                            }
                        } else {
                            obj["is_bookmarked"] = false;
                        }


                        if (obj["everyone_count"].length > 0) {
                            obj["everyone_count"] = parseInt(obj["everyone_count"][0]["count_everyone"]);

                        } else {
                            obj["everyone_count"] = parseInt(0)
                        }

                        if (obj["friends_rating"].length > 0) {
                            obj["count_friends"] = parseInt(obj["friends_rating"][0]["count_friends"]);
                            obj["friends_rating"] = parseFloat(obj["friends_rating"][0]["avgrating"]);

                        } else {
                            obj["count_friends"] = parseInt(0)
                            obj["friends_rating"] = parseFloat(0.00);
                        }

                        if (!obj["cuisine"]) {
                            obj["cuisine"] = [];
                        }
                        //                        if (obj["cuisine"].length < 1) {
                        //                            obj["cuisine"] = [];
                        //                        }

                        if (obj["user_rating"].length > 0) {
                            obj["user_rating"] = obj["user_rating"][0]["rating"];
                        } else {
                            obj["user_rating"] = 0.00
                        }
                        delete obj["bookmark"];
                        return obj;
                    });
                    // console.timeEnd("---6---");
                    // console.log("result");
                    // console.log(result);
                    res.status(200).send({ status: 1, message: "success", distance_unit: userfriend["data"]["distance_unit"], count: count, data: result["data"] });
                } else {
                    throw result;
                }
            } catch (err) // catch errors
            {
                if (err.message == "" || err.message == undefined) {
                    res.status(400).send({ message: "Something went worng", status: 0 });
                } else {
                    res.status(400).send({ message: err.message, status: 0 });
                }
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
router.get('/cuisine', function (req, res, next) {
    Sync(function () {
        try {
            //var query = "cuisine";
            if (req.query.type) {
                // console.log(cuisine.main_cuisine);
                let aggregation_condition = [
                    { $match: { type: parseInt(req.query.type), dummy: 0 } },
                    { "$project": { cuisine: 1, _id: 0 } },
                    { $group: { _id: null, uniqueTags: { $push: "$cuisine" } } },
                    {
                        $project: {
                            _id: 0,
                            uniqueTags: {
                                $reduce: {
                                    input: "$uniqueTags",
                                    initialValue: [],
                                    in: {
                                        $let: {
                                            vars: { elem: { $concatArrays: ["$$this", "$$value"] } },
                                            in: { $setUnion: "$$elem" }
                                        }
                                    }
                                }
                            }
                        }
                    }
                ]
                //                var data_list = posts.distinct.sync(null, "genre", {type: parseInt(req.query.type), dummy: 0}, {"genre": 1});
                var data_list = posts.aggregate.sync(null, aggregation_condition);
                //                console.log(data_list);
                if (data_list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1, data: data_list.data[0]["uniqueTags"] });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});

router.get('/new_cuisine', function (req, res, next) {
    Sync(function () {
        try {
            //var query = "cuisine";
            if (req.query.type) {
                if (req.query.type == 1) {
                    res.status(200).send({ message: "Success", status: 1, data: cuisine.main_cuisine });
                } else if (req.query.type == 2) {
                    let aggregation_condition = [
                        { $match: { type: parseInt(req.query.type), dummy: 0 } },
                        { "$project": { cuisine: 1, _id: 0 } },
                        { $group: { _id: null, uniqueTags: { $push: "$cuisine" } } },
                        {
                            $project: {
                                _id: 0,
                                uniqueTags: {
                                    $reduce: {
                                        input: "$uniqueTags",
                                        initialValue: [],
                                        in: {
                                            $let: {
                                                vars: { elem: { $concatArrays: ["$$this", "$$value"] } },
                                                in: { $setUnion: "$$elem" }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    ]
                    var data_list = posts.aggregate.sync(null, aggregation_condition);
                    if (data_list.status == 1) {
                        res.status(200).send({ message: "Success", status: 1, data: data_list.data[0]["uniqueTags"] });
                    } else {
                        res.status(400).send({ message: data_list.message, status: 0 });
                    }
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});


router.get('/genre', function (req, res, next) {
    Sync(function () {
        try {
            if (req.query.type) {
                let aggregation_condition = [
                    { $match: { type: parseInt(req.query.type), dummy: 0 } },
                    { "$project": { genre: 1, _id: 0 } },
                    { $group: { _id: null, uniqueTags: { $push: "$genre" } } },
                    {
                        $project: {
                            _id: 0,
                            uniqueTags: {
                                $reduce: {
                                    input: "$uniqueTags",
                                    initialValue: [],
                                    in: {
                                        $let: {
                                            vars: { elem: { $concatArrays: ["$$this", "$$value"] } },
                                            in: { $setUnion: "$$elem" }
                                        }
                                    }
                                }
                            }
                        }
                    }
                ]
                //                var data_list = posts.distinct.sync(null, "genre", {type: parseInt(req.query.type), dummy: 0}, {"genre": 1});
                var data_list = posts.aggregate.sync(null, aggregation_condition);
                //console.log(data_list.data[0]["uniqueTags"]);
                if (data_list.status == 1) {
                    //console.log(data_list.data[0]["uniqueTags"].includes("Adult"));
                    if (data_list.data[0]["uniqueTags"].includes("Adult")) {
                        for (var i = 0; i < data_list.data[0]["uniqueTags"].length - 1; i++) {
                            if (data_list.data[0]["uniqueTags"][i] === "Adult") {
                                data_list.data[0]["uniqueTags"].splice(i, 1);
                            }
                        }
                        data_list.data[0]["uniqueTags"].push("Adult");
                        res.status(200).send({ message: "Success", status: 1, data: data_list.data[0]["uniqueTags"] });
                    } else {
                        //console.log(data_list.data[0]["uniqueTags"]);
                        res.status(200).send({ message: "Success", status: 1, data: data_list.data[0]["uniqueTags"] });
                    }
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
function create_search_condition(req, res, projection, callback) {
    let today = new Date();
    process.nextTick(function () {
        Sync(() => {
            var query = {};
            query.type = parseInt(req.query.category);
            query["_id"] = mongoose.Types.ObjectId(req.query.postId);
            //            //        console.log(req.query.search_text)
            //            if (req.query.search_text) {
            //                query.title = new RegExp('^' + req.query.search_text + '.*', 'i');
            //            }
            //            if (match) {
            //                Object.assign(query, match);
            //            }
            //            //            query["expiry_time"] = {$lte: today.getTime()};
            var aggregate_condition = [];
            var category = req.query.category;
            switch (category) {
                case '1':
                    {
                        let user = users.findOne.sync(null, { "_id": req.decoded._id, is_deleted: 0, status: 1 }, { distance_unit: 1 });
                        if (req.query.lat_long.length != 0) {
                            let coords = JSON.parse(req.query.lat_long)
                            aggregate_condition = [
                                {
                                    $geoNear: {
                                        //                                near: {type: "Point", "location.coordinates": [coords[1], coords[0]]},
                                        near: [coords[1], coords[0]],
                                        distanceField: "distance",
                                        query: query,
                                        //                                    maxDistance: config.nearbyRaduis,
                                        distanceMultiplier: 6371000,
                                        spherical: true
                                    }
                                },
                                { $project: projection },
                            ];
                        } else {
                            return res.status(400).send({ message: "Location missing", status: 0 })
                        }
                        break;
                    }
                case '2':
                    {
                        if (req.query.lat_long.length != 0) {
                            let coords = JSON.parse(req.query.lat_long)
                            aggregate_condition = [
                                {
                                    $geoNear: {
                                        near: [coords[1], coords[0]],
                                        distanceField: "distance",
                                        query: query,
                                        //                                    maxDistance: config.nearbyRaduis,
                                        distanceMultiplier: 6371000,
                                        spherical: true
                                    }
                                },
                                { $project: projection },
                            ];
                        } else {
                            return res.status(400).send({ message: "Location missing", status: 0 })
                        }
                        break;
                    }
                case '3':
                    {
                        //Movie
                        query.type = 3;
                        aggregate_condition = [
                            { $match: query },
                            { $project: projection },
                        ];
                        break;
                    }
                case '4':
                    {
                        //TV  or series
                        query.type = 4;
                        aggregate_condition = [
                            { $match: query },
                            { $project: projection },
                        ];
                        break;
                    }
                case '5':
                    {
                        //                    console.log("book");
                        //Book
                        query.type = 5;
                        aggregate_condition = [
                            { $match: query },
                            { $project: projection },
                        ];
                        break;
                    }
            }
            callback(null, aggregate_condition);
        });
    }
    );
}
//function search_from_api(req, res, callback) {
//    process.nextTick(function () {
//        Sync(() => {
//            var query = {};
//            //        query.title = new RegExp('^' + req.query.search_text + '.*', 'i');
//            //        var aggregate_condition = [];
//            var category = req.query.category;
//            let data;
//            switch (category) {
//                case '1':
//                {
////Restaurant
//                    let coords = JSON.parse(req.query.lat_long)
//                    data = foresquare.search_foursquare.sync(null, coords[0], coords[1], req.query.search_text, 1);
//                    if (data["status"] === 1) {
//                        data["data"]["type"] = 1;
//                    }
//                    break;
//                }
//                case '2':
//                {
////Bar
//                    let coords = JSON.parse(req.query.lat_long)
//                    data = foresquare.search_foursquare.sync(null, coords[0], coords[1], req.query.search_text, 2);
//                    if (data["status"] === 1) {
//                        data["data"]["type"] = 2;
//                    }
//                    break;
//                }
//                case '3':
//                {
////Movie
//                    data = omdb.search_omdb.sync(null, req.query.search_text, "movie");
//                    if (data.status == 1) {
//                        data["data"]["year"] = parseInt(data["data"]["year"]);
//                    }
//                    data["data"]["type"] = 3;
//                    break;
//                }
//                case '4':
//                {
////series
//                    data = omdb.search_omdb.sync(null, req.query.search_text, "series");
//                    if (data.status == 1) {
//                        let year = data["data"]["year"].split("–");
//                        data["data"]["start_year"] = parseInt(year[0]);
//                        if (data["data"]["end_year"]) {
//                            data["data"]["end_year"] = parseInt(year[1]);
//                        } else {
//                            data["data"]["end_year"] = null;
//                        }
//                        delete data.data["year"];
//                    }
//                    data["data"]["type"] = 4;
//                    break;
//                }
//                case '5':
//                {
////                    console.log("Book");
//                    data = goodreads.search_goodreads.sync(null, req.query.search_text);
//                    break;
//                }
//            }
//            callback(null, data);
//        });
//    });
//}

function search_from_api(reference_id, category, callback) {
    process.nextTick(function () {
        Sync(() => {
            let data;
            switch (category) {
                case '1':
                    {
                        //Restaurant
                        data = foresquare.search_foursquare.sync(null, reference_id);
                        //                    console.log(data);
                        //                    if (data["status"] === 1) {
                        //                    }
                        break;
                    }
                case '2':
                    {
                        //Bar
                        data = foresquare.search_foursquare.sync(null, reference_id);
                        //                    if (data["status"] === 1) {
                        //                        data["data"]["type"] = 2;
                        //                    }
                        break;
                    }
                case '3':
                    {
                        //Movie
                        data = omdb.search_omdb.sync(null, reference_id, "movie");
                        //                    if (data.status == 1) {
                        //                        data["data"]["year"] = parseInt(data["data"]["year"]);
                        //                    }
                        //                    data["data"]["type"] = 3;
                        break;
                    }
                case '4':
                    {
                        //series
                        data = omdb.search_omdb.sync(null, reference_id, "series");
                        //                    if (data.status == 1) {
                        //                        let year = data["data"]["year"].split("–");
                        //                        data["data"]["start_year"] = parseInt(year[0]);
                        //                        if (data["data"]["end_year"]) {
                        //                            data["data"]["end_year"] = parseInt(year[1]);
                        //                        } else {
                        //                            data["data"]["end_year"] = null;
                        //                        }
                        //                        delete data.data["year"];
                        //                    }
                        //                    data["data"]["type"] = 4;
                        break;
                    }
                case '5':
                    {
                        //                    console.log("Book");
                        data = goodreads.search_goodreads.sync(null, reference_id);
                        break;
                    }
            }
            callback(null, data);
        });
    });
}



module.exports = router;