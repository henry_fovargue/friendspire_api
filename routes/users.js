var express = require('express');
var router = express.Router();
var user = require('../controller/users.js');
var mongoose = require('mongoose');
var Sync = require('sync');
/* GET users listing. */
router.get('/', function (req, res) {
  res.send('respond with a resource');
});

router.put('/addressUpdate', function (req, res) {
  Sync(function () {
    try {
      if (req.body.address && req.body.long && req.body.lat) {
        var query = { _id: req.decoded._id, is_deleted: 0 };
        var data = {};
        data.is_address_added = 1;
        data.address = req.body.address;
        data.longlat = [req.body.long, req.body.lat];
        var data_list = user.update.sync(null, query, data);
        if (data_list.status == 1) {
          res.status(200).send({ message: "Success", status: 1 });
        } else {
          res.status(400).send({ message: data_list.message, status: 0 });
        }
      } else {
        res.status(400).send({ message: "Invalid request", status: 0 });
      }
    } catch (err) // catch errors
    {
      res.send({ message: err.message, status: 0 });
    }
  });
});

module.exports = router;
