var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
var recommend = require('../controller/recommendations.js');
// require passport-hash module for encrypt the password

//get profile detail of user
router.get('/', function (req, res, next) {
    Sync(function ()
    {
        try
        {
            var user_data = users.findOne.sync(null, {_id: req.decoded._id, is_deleted: 0, status: 1}, {_id: 1, distance_unit: 1, is_private: 1, push_notification: 1});
            if (user_data.status === 1)
            {
                res.status(200).send({message: "success", status: 1, data: user_data.data});
            } else
            {
                throw Error("Invalid user.");
            }
        } catch (err)
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });
});
router.put('/connect_facebook', function (req, res, next) {
    Sync(function () {
        try {
            var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {});
            if (user_data.status === 1)
            {
                if (user_data.data.status === 1) {
                    if (req.body.sns_id) {
                        var finduser = users.findOne.sync({sns_id: req.body.sns_id,is_deleted:0,status:1});
                        if (finduser.status == 1) {
                             res.status(400).send({message: "You or someone else has already register with this id", status: 0});
                        } else {
                            users.update.sync(null, {_id: req.decoded._id}, {sns_id: req.body.sns_id, fb_connected: 1});
                            res.status(200).send({message: "Success", status: 1});
                        }
                    } else {
                        res.status(400).send({message: "Please provide sns_id", status: 0});
                    }

                } else if (user_data.data.status === 0) {
                    res.status(400).send({message: global.messages.profileDataUpdated, status: 0});
                } else {
                    res.status(400).send({message: global.messages.emailNotVerified, status: 0});
                }
            } else
            {
                res.status(400).send({message: user_data.message, status: 0});
            }
        } catch (err) // catch errors
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });
});
//route for update 
router.put('/update_distance_unit', function (req, res, next) {
    Sync(function () {
        try {
            var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {});
            if (user_data.status === 1)
            {
                if (user_data.data.status === 1) {
                    if (req.body.distance_unit == 1 || req.body.distance_unit == 2) {
                        users.update.sync(null, {_id: req.decoded._id}, {distance_unit: req.body.distance_unit});
                        res.status(200).send({message: "Success", status: 1});
                    } else {
                        res.status(400).send({message: "Please provide the valid preference for units", status: 2});
                    }
                } else if (user_data.data.status === 0) {
                    res.status(400).send({message: global.messages.profileDataUpdated, status: 0});
                } else {
                    res.status(400).send({message: global.messages.emailNotVerified, status: 0});
                }
            } else
            {
                res.status(400).send({message: user_data.message, status: 0});
            }
        } catch (err) // catch errors
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });
});

router.put('/private_account', function (req, res, next) {
    Sync(function () {
        try {
//            console.log(req.body);
            //console.log(req.decoded._id);
            var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {});
            if (user_data.status === 1)
            {
                //console.log(user_data);
                if (user_data.data.status === 1) {
                    // update profile
//                   console.log(update_data);
                    var update_data = users.updateNew.sync(null, {_id: req.decoded._id}, {is_private: req.body.is_private});
                    if (req.body.is_private == 1) {
                        var data_list = recommend.updateMultiple.sync(null, {user_id: req.decoded._id, is_deleted: 0}, {is_private: 1});
                        if (data_list.status == 0) {
                            return res.status(400).send({message: data_list.message, status: 0});
                        }
                    } else {
                        var data_list = recommend.updateMultiple.sync(null, {user_id: req.decoded._id, is_deleted: 0}, {is_private: 0});
                        if (data_list.status == 0) {
                            return res.status(400).send({message: data_list.message, status: 0});
                        }

                        var request_list = users.updateNew.sync(null, {friends: {$elemMatch: {user_id: mongoose.Types.ObjectId(req.decoded._id), status: 2}}}, {'friends.$.status': 1});
//                        console.log(request_list);
                        if (request_list.status == 0) {
                            return res.status(400).send({message: request_list.message, status: 0});
                        }

//                      
                    }
                    res.status(200).send({message: "Success", status: 1});
                } else if (user_data.data.status === 0) {
                    res.status(400).send({message: global.messages.notActiveAccount, status: 0});
                } else {
                    res.status(400).send({message: global.messages.emailNotVerified, status: 0});
                }
            } else
            {
                res.status(400).send({message: user_data.message, status: 0});
            }
        } catch (err) // catch errors
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });
});

router.put('/update_push_status', function (req, res, next) {
    Sync(function () {
        try {
            var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {});
            if (user_data.status === 1)
            {
                if (user_data.data.status === 1) {
                    // update profile
                    users.update.sync(null, {_id: req.decoded._id}, {push_notification: req.body.push_notification});
                    res.status(200).send({message: "Success", status: 1});
                } else if (user_data.data.status === 0) {
                    res.status(400).send({message: global.messages.profileDataUpdated, status: 0});
                } else {
                    res.status(400).send({message: global.messages.emailNotVerified, status: 0});
                }
            } else
            {
                res.status(400).send({message: user_data.message, status: 0});
            }
        } catch (err) // catch errors
        {
            res.status(400).send({message: err.message, status: 0});
        }
    });
});


router.put('/update_batch_count', function (req, res, next) {
    Sync(function () {
        try {
                console.log(req.decoded._id);
                var query = { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 };
                var batch_count_data = users.update.sync(null, query, {batch_count: 0 });
                if (batch_count_data.status == 1) {
                    res.status(200).send({ message: "Success", status: 1 });
                } else {
                    res.status(400).send({ message: batch_count_data.message, status: 0 });
                }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});


module.exports = router;
