var express = require('express');
var router = express.Router();
var mongoose = require("mongoose");
var Sync = require('sync');
const recommendation = require("../controller/recommendations");
var comment = require('../controller/comment.js');
var notification = require('../helper/notifications.js');
var notification_controller = require('../controller/notification.js');
var users = require("../controller/users");
var util = require("util");
var push_messages = require('../locales/en_push');
var async = require('async');
var commentPostValidate = function (req, res, next) {
    req.checkBody('recommendation_id', 'invaild recommendation_id').notEmpty();
    req.checkBody('comment', 'Please enter comment').notEmpty();
    return req.validationErrors(true);
}

function insertionOfDataNotification(unique_id_array, commented_userid, recommendationid, notification_message, commentid) {
    var notification_data = [];
    for (var i = 0; i < unique_id_array.length; i++) {
        notification_data.push({
            recommendation_id: recommendationid,
            comment_id: commentid,
            reference_id: null,
            from: commented_userid,
            to: unique_id_array[i],
            type: 8,
            message: notification_message,
            is_deleted: 0,
            is_read: 0
        });
    }
    //console.log(notification_data);
    var notification_datalist = notification_controller.create.sync(null, notification_data);
    //console.log("----------------------",notification_datalist);
    if (notification_datalist.status == 1) {
        //console.log("~~~~~~~~~~~~~~~~~~~~~~~~entered");
        var batch_count_data = users.update.sync(null, { _id: { $in: unique_id_array }, is_deleted: 0, status: 1 }, { $inc: { batch_count: 1 } });
        //console.log("+++++++++++++++++++++",batch_count_data);
    }
    //console.log(notification_datalist);
}

function pushNotification(token_array, referenceid, recommendationId, postType, push_message, commentid, badge_array) {
    // console.log(push_message);
    //    console.log("------------------------------", friend);?
    //console.log("friend.data[0].device_token------------------------------", friend.data[0].device_token);
    //console.log("friend.data[0].push_notification------------------------------", friend.data[0].push_notification);
    var data = {
        title: 'Friendspire',
        body: push_message,
        type: 8,
        post_id: referenceid,
        comment_id: commentid,
        recommendation_id: recommendationId,
        post_type: postType
    };
    //console.log("data", data);
    notification.send_notification_to_all(token_array, data, badge_array);
}

function responseSend(req, res, next, data) {
    // console.log("-------------------data-----------------------");
    // console.log(util.inspect(data, { depth: null }));
    delete data.commented_user_data.friends;
    res.status(200).send({ message: "Success", status: 1, data: data });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


router.post("/", function (req, res, next) {
    let errors = commentPostValidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                console.log(req.decoded._id);
                console.log(req.decoded.blocked_user);
                var query = [
                    { $match: { _id: mongoose.Types.ObjectId(req.body.recommendation_id), is_deleted: 0 } },
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$status", 1] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1, push_notification: 1, device_token: 1, batch_count: 1 } }
                            ],
                            as: "user_id"
                        }
                    },
                    { $unwind: "$user_id" },
                    { $match: { "user_id.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) }, "user_id._id": { $nin: req.decoded.blocked_user } } },
                    {
                        "$lookup": {
                            from: "comments",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$recommendation_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 0, user_id: 1 } }
                            ],
                            as: "comment_ids"
                        }
                    },
                    { "$unwind": { path: "$comment_ids", "preserveNullAndEmptyArrays": true } },
                    //{ $unwind: "$comment_ids" },
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$comment_ids.user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$status", 1] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, push_notification: 1, device_token: 1, batch_count: 1 } }
                            ],
                            as: "commented_user_detail"
                        }
                    },
                    { "$unwind": { path: "$commented_user_detail", "preserveNullAndEmptyArrays": true } },
                    // {
                    //     '$match': { 'commented_user_detail._id': { '$nin': req.decoded.blocked_user } }
                    // },
                    // {
                    //     $group: {
                    //         _id: "$_id",
                    //         commented_user_detail: { $push: "$commented_user_detail" },
                    //         user_id: { $first: "$user_id" },
                    //         type: { $first: "$type" },
                    //         created_at: { $first: "$created_at" },
                    //         updated_at: { $first: "$updated_at" },
                    //         reference_id: { $first: "$reference_id" },
                    //         is_deleted: { $first: "$is_deleted" },
                    //         is_private: { $first: "$is_private" },
                    //         review: { $first: "$review" },
                    //         rating: { $first: "$rating" }
                    //     }
                    // },
                    {
                        $project: {
                            _id: 1,
                            is_deleted: 1,
                            reference_id: 1,
                            updated_at: 1,
                            created_at: 1,
                            is_private: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            type: 1,
                            commented_user_detail: 1,
                            comment_flag: {
                                $cond: {
                                    if: { $in: ["$commented_user_detail._id", req.decoded.blocked_user] },
                                    then: 0,
                                    else: 1

                                }
                            },

                        },
                    },
                    {
                        '$group':
                        {
                            _id: '$_id',
                            commented_user_detail: {
                                "$push": {
                                    "$cond": [
                                        { "$eq": ["$comment_flag", 1] },
                                        "$commented_user_detail",
                                        {}

                                    ]
                                }
                            },
                            user_id: { '$first': '$user_id' },
                            type: { '$first': '$type' },
                            created_at: { '$first': '$created_at' },
                            updated_at: { '$first': '$updated_at' },
                            reference_id: { '$first': '$reference_id' },
                            is_deleted: { '$first': '$is_deleted' },
                            is_private: { '$first': '$is_private' },
                            review: { '$first': '$review' },
                            rating: { '$first': '$rating' }
                        }
                    },
                    {
                        $project: {
                            commented_user_detail: {
                                $filter: {
                                    input: '$commented_user_detail',
                                    as: 'item',
                                    cond: { $ne: ['$$item', {}] }
                                },
                            },
                            _id: 1,
                            updated_at: 1,
                            user_id: 1,
                            review: 1,
                            rating: 1,
                            type: 1,
                            created_at: 1,
                            reference_id: 1,
                            is_deleted: 1,
                            is_private: 1
                        }
                    },
                ];
                //console.log(util.inspect(query, { depth: null }));
                //let recommendData = recommendation.findOne.sync(null, { _id: req.body.recommendation_id, is_deleted: 0 });
                let recommendData = recommendation.aggregate.sync(null, query);
                //console.log(util.inspect(recommendData, { depth: null }));
                if (recommendData.status == 1) {
                    if (recommendData.data.length != 0) {
                        let commentData = {
                            recommendation_id: req.body.recommendation_id,
                            user_id: req.decoded._id,
                            is_deleted: 0,
                            comment: req.body.comment.toString().trim(),
                            post_id: recommendData.data[0].reference_id
                        };
                        //if(recommendData.data.)
                        var push_message = "";
                        var notification_message = "";
                        let commentRes = comment.save.sync(null, commentData);
                        if (commentRes["status"] == 1) {

                            var new_comment = users.findOne.sync(null, { _id: commentRes.data.user_id, is_deleted: 0, status: 1 }, { "firstName": 1, "lastName": 1, "_id": 1, "profile_pic": 1, "friends": 1 });
                            //console.log(new_comment);
                            commentRes["data"]["commented_user_data"] = new_comment.data;
                            push_message = push_messages.commentPushMessage;
                            push_message = push_message.replace("$CommentfirstName", new_comment.data.firstName);
                            push_message = push_message.replace("$CommentlastName", new_comment.data.lastName);
                            push_message = push_message.replace("$RecommendatefirstName", recommendData.data[0].user_id.firstName);
                            push_message = push_message.replace("$RecommendatelastName", recommendData.data[0].user_id.lastName);

                            notification_message = push_messages.commentMessage;
                            notification_message = notification_message.replace("$CommentfirstName", new_comment.data.firstName);
                            notification_message = notification_message.replace("$CommentlastName", new_comment.data.lastName);
                            notification_message = notification_message.replace("$RecommendatefirstName", recommendData.data[0].user_id.firstName);
                            notification_message = notification_message.replace("$RecommendatelastName", recommendData.data[0].user_id.lastName);
                            var token_array = [];
                            var badge_array = [];
                            var id_array = [];
                            if (new_comment.data._id != recommendData.data[0].user_id._id) {
                                id_array.push(recommendData.data[0].user_id._id);
                            }
                            if (recommendData.data[0].user_id.device_token != "" && recommendData.data[0].user_id.push_notification == 1) {
                                if (new_comment.data._id != recommendData.data[0].user_id._id) {
                                    token_array.push(recommendData.data[0].user_id.device_token);
                                    badge_array.push({ device_token: recommendData.data[0].user_id.device_token, badge_count: recommendData.data[0].user_id.batch_count+1 });
                                }
                            }
                            if (recommendData.data[0].commented_user_detail.length != 0) {
                                for (var i = 0; i < recommendData.data[0].commented_user_detail.length; i++) {
                                    var exist = 0;
                                    for (var j = 0; j < new_comment.data.friends.length; j++) {
                                        if (new_comment.data.friends[j].user_id == recommendData.data[0].commented_user_detail[i]._id && new_comment.data.friends[j].status == 1 && new_comment.data.friends[j].is_blocked == 0) {
                                            exist = 1;
                                            break
                                        } else {
                                            continue;
                                        }
                                    }
                                    if (recommendData.data[0].commented_user_detail[i].is_private == 0 || exist == 1) {
                                        if (new_comment.data._id != recommendData.data[0].commented_user_detail[i]._id) {
                                            id_array.push(recommendData.data[0].commented_user_detail[i]._id);
                                        }
                                    }
                                    // console.log("recommendData.data[0].commented_user_detail[i].batch_count",recommendData.data[0].commented_user_detail[i].batch_count);
                                    // console.log("recommendData.data[0].commented_user_detail[i].batch_count+1",recommendData.data[0].commented_user_detail[i].batch_count+1);
                                    if (recommendData.data[0].commented_user_detail[i].device_token && recommendData.data[0].commented_user_detail[i].push_notification == 1 && new_comment.data._id != recommendData.data[0].commented_user_detail[i]._id) {
                                        var count_batch = recommendData.data[0].commented_user_detail[i].batch_count+1;
                                        // console.log(count_batch);
                                        token_array.push(recommendData.data[0].commented_user_detail[i].device_token);
                                        badge_array.push({ device_token: recommendData.data[0].commented_user_detail[i].device_token, badge_count: count_batch });
                                    }
                                }

                            }


                            // console.log("---------------id_array------------------", id_array);
                            // console.log("---------------token_array------------------", token_array);
                            // console.log("---------------badge_array------------------", badge_array);
                            var unique_token_array = token_array.filter(onlyUnique);
                            var unique_id_array = id_array.filter(onlyUnique);
                            var extra_array = {};
                            badge_array = badge_array.filter(function (currentObject) {
                                if (currentObject.device_token in extra_array) {
                                    return false;
                                } else {
                                    extra_array[currentObject.device_token] = true;
                                    return true;
                                }
                            })
                            // console.log("---------------unique_id_array------------------", unique_id_array);
                            // console.log("---------------unique_token_array------------------", unique_token_array);
                            //console.log("---------------unique_badge_array------------------", badge_array);

                            async.parallel([
                                function () {
                                    responseSend(req, res, next, commentRes["data"]);
                                },
                                function () {
                                    insertionOfDataNotification(unique_id_array, req.decoded._id, recommendData.data[0]._id, notification_message, commentRes.data._id);
                                },
                                function () {
                                    pushNotification(unique_token_array, recommendData.data[0].reference_id, recommendData.data[0]._id, recommendData.data[0].type, push_message, commentRes.data._id, badge_array);
                                }
                            ]);
                            //res.status(200).send({ message: "success", status: 1, data: commentRes["data"] });
                        } else {
                            throw commentRes;
                        }
                    } else {
                        throw { message: "You can't comment on this recommendation" };
                    }
                } else {
                    throw { message: "recommendation not found" };
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
var commentPutValidate = function (req, res, next) {
    req.checkBody('comment_id', 'invaild comment_id').notEmpty();
    req.checkBody('comment', 'Please enter comment').notEmpty();
    return req.validationErrors(true);
}
router.put("/", function (req, res, next) {
    let errors = commentPutValidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                var query = [
                    { $match: { _id: mongoose.Types.ObjectId(req.body.comment_id), is_deleted: 0 } },
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$status", 1] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1 } }
                            ],
                            as: "user_id"
                        }
                    },
                    {
                        '$lookup':
                        {
                            from: 'recommendations',
                            let: { ref_id: '$recommendation_id' },
                            pipeline:
                                [{
                                    '$match':
                                    {
                                        '$expr':
                                        {
                                            '$and':
                                                [{ '$eq': ['$_id', '$$ref_id'] },
                                                { '$eq': ['$is_deleted', 0] }]
                                        }
                                    }
                                },
                                {
                                    '$project':
                                    {
                                        _id: 1,
                                        user_id: 1,
                                        reference_id: 1
                                    }
                                }
                                ],
                            as: 'recommendation_data'
                        }
                    },
                    { $unwind: "$recommendation_data" },
                    {
                        '$lookup':
                        {
                            from: 'users',
                            let: { ref_id: '$recommendation_data.user_id' },
                            pipeline:
                                [{
                                    '$match':
                                    {
                                        '$expr':
                                        {
                                            '$and':
                                                [{ '$eq': ['$_id', '$$ref_id'] },
                                                { '$eq': ['$status', 1] },
                                                { '$eq': ['$is_deleted', 0] }]
                                        }
                                    }
                                },
                                {
                                    '$project':
                                    {
                                        _id: 1,
                                        firstName: 1,
                                        lastName: 1,
                                        profile_pic: 1,
                                        blocked_user: 1
                                    }
                                }],
                            as: 'user_detail'
                        }
                    },
                    { '$unwind': '$user_detail' },
                    //{ $unwind: "$user_id" },
                    { $match: { "user_detail.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) }, "user_detail._id": { $nin: req.decoded.blocked_user } } }
                ];
                //console.log(util.inspect(query, { depth: null }));
                let commentData = comment.aggregate.sync(null, query);
                //console.log(util.inspect(recommendData, { depth: null }));
                if (commentData.status == 1) {
                    if (commentData.data.length != 0) {
                        let newcomment = req.body.comment.toString().trim();
                        let match_query = {
                            _id: mongoose.Types.ObjectId(req.body.comment_id),
                            is_deleted: 0,
                            user_id: mongoose.Types.ObjectId(req.decoded._id)
                        };
                        let updateRes = comment.update.sync(null, match_query, { comment: newcomment });
                        if (updateRes["status"] == 1) {
                            res.status(200).send({ message: "success", status: 1 });
                        } else {
                            throw updateRes;
                        }
                    } else {
                        throw { message: "You can't update your comment on this recommendation" };
                    }
                } else {
                    throw { message: "comment not found" };
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
var commentDeleteValidate = function (req, res, next) {
    req.checkBody('comment_id', 'invaild comment_id').notEmpty();
    return req.validationErrors(true);
}
router.delete("/", function (req, res, next) {
    let errors = commentDeleteValidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                var query = [
                    { $match: { _id: mongoose.Types.ObjectId(req.body.comment_id), is_deleted: 0 } },
                    {
                        "$lookup": {
                            from: "users",
                            let: { ref_id: "$user_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$_id", "$$ref_id"] },
                                                    { $eq: ["$status", 1] },
                                                    { $eq: ["$is_deleted", 0] }
                                                ]
                                        }
                                    }
                                },
                                { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1 } }
                            ],
                            as: "user_id"
                        }
                    },
                    {
                        '$lookup':
                        {
                            from: 'recommendations',
                            let: { ref_id: '$recommendation_id' },
                            pipeline:
                                [{
                                    '$match':
                                    {
                                        '$expr':
                                        {
                                            '$and':
                                                [{ '$eq': ['$_id', '$$ref_id'] },
                                                { '$eq': ['$is_deleted', 0] }]
                                        }
                                    }
                                },
                                {
                                    '$project':
                                    {
                                        _id: 1,
                                        user_id: 1,
                                        reference_id: 1
                                    }
                                }
                                ],
                            as: 'recommendation_data'
                        }
                    },
                    { $unwind: "$recommendation_data" },
                    {
                        '$lookup':
                        {
                            from: 'users',
                            let: { ref_id: '$recommendation_data.user_id' },
                            pipeline:
                                [{
                                    '$match':
                                    {
                                        '$expr':
                                        {
                                            '$and':
                                                [{ '$eq': ['$_id', '$$ref_id'] },
                                                { '$eq': ['$status', 1] },
                                                { '$eq': ['$is_deleted', 0] }]
                                        }
                                    }
                                },
                                {
                                    '$project':
                                    {
                                        _id: 1,
                                        firstName: 1,
                                        lastName: 1,
                                        profile_pic: 1,
                                        blocked_user: 1
                                    }
                                }],
                            as: 'user_detail'
                        }
                    },
                    { '$unwind': '$user_detail' },
                    //{ $unwind: "$user_id" },
                    { $match: { "user_detail.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) }, "user_detail._id": { $nin: req.decoded.blocked_user } } }
                ];
                //console.log(util.inspect(query, { depth: null }));
                let commentData = comment.aggregate.sync(null, query);
                //console.log(util.inspect(commentData, { depth: null }));
                if (commentData.status == 1) {
                    //console.log("============", commentData.data.length);
                    if (commentData.data.length != 0) {
                        //console.log("blocked_user", req.decoded.blocked_user);
                        let match_query = {
                            _id: mongoose.Types.ObjectId(req.body.comment_id),
                            is_deleted: 0,
                            user_id: mongoose.Types.ObjectId(req.decoded._id)
                        };
                        let updateRes = comment.update.sync(null, match_query, { is_deleted: 1 });
                        if (updateRes["status"] == 1) {
                            res.status(200).send({ message: "success", status: 1 });
                        } else {
                            throw updateRes;
                        }
                    } else {
                        throw { message: "You can't delete your comment on this recommendation" };
                    }
                } else {
                    throw { message: "comment not found" };
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
var commentListValidate = function (req, res, next) {
    req.checkQuery('recommendation_id', 'invaild recommenadation id').notEmpty();
    return req.validationErrors(true);
}
router.get("/", function (req, res, next) {
    let errors = commentListValidate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                //console.log("blocked_user", req.decoded.blocked_user);
                //console.log(req.decoded._id);
                let found = false;
                let recommendationData = recommendation.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.query.recommendation_id), is_deleted: 0 });
                //console.log("recommendationData");
                //console.log(recommendationData);
                if (recommendationData["status"] == 1) {
                    var ownerUserId = recommendationData["data"]["user_id"];
                    if (recommendationData["data"]["is_private"] == 0 || req.decoded._id == ownerUserId) {
                        found = true;
                    } else {
                        let userData = users.findOne.sync(null, { _id: ownerUserId, is_deleted: 0, friends: { "$elemMatch": { status: 1 } } }, { friends: 1,blocked_user:1 })
                        //console.log("userData");
                        //console.log(util.inspect(userData, { depth: null }));
//                        if(userData.data.blocked_user.length){
//                            if(userData.data.blocked_user.indexOf(req.decoded._id) > -1){
//                                throw ({message:"You account is blocked by the User"});
//                            }
//                        }
                        let friends = userData["data"]["friends"];
                        //console.log(friends);
                        for (let i = 0; i < friends.length; i++) {
                            if (friends[i]["user_id"] == req.decoded._id) {
                                found = true;
                                break;
                            }
                        }
                    }
                    
//                    var usersWhoBlockMe = users.findOne.sync(null, {_id:ownerUserId,blocked_user: {$eq: mongoose.Types.ObjectId(req.decoded._id)}}, {_id: 1});
//                    var usersWhoBlockMe_array = [];
//                    for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
//                        usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
//                    }
                    /*show the comments*/
                    if (found) {
                        let aggregationCondition = [
                            { $match: {recommendation_id: mongoose.Types.ObjectId(req.query.recommendation_id), is_deleted: 0 } },
                            {
                                "$lookup": {
                                    from: "users",
                                    let: { ref_id: "$user_id" },
                                    pipeline: [
                                        {
                                            $match:
                                            {
                                                $expr:
                                                {
                                                    $and:
                                                        [
                                                            { $eq: ["$_id", "$$ref_id"] },
                                                            { $eq: ["$status", 1] },
                                                            { $eq: ["$is_deleted", 0] }
                                                        ]
                                                }
                                            }
                                        },
                                        { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1 } }
                                    ],
                                    as: "commented_user_data"
                                }
                            },
                            { $unwind: "$commented_user_data" },
                            { $match: { "commented_user_data.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) }, "commented_user_data._id": { $nin: req.decoded.blocked_user } } },
                            {
                                '$project': {
                                    _id: 1,
                                    updated_at: 1,
                                    //created_at: 1,
                                    recommendation_id: 1,
                                    //is_deleted: 1,
                                    comment: 1,
                                    commented_user_data: {
                                        _id: 1,
                                        firstName: 1,
                                        lastName: 1,
                                        profile_pic: 1
                                    }
                                }
                            },
                            { $group: { _id: "$recommendation_id", comment_data: { $push: "$$ROOT" } } },
                            {
                                "$lookup": {
                                    from: "recommendations",
                                    let: { ref_id: "$_id" },
                                    pipeline: [
                                        {
                                            $match:
                                            {
                                                $expr:
                                                {
                                                    $and:
                                                        [
                                                            { $eq: ["$_id", "$$ref_id"] },
                                                            { $eq: ["$is_deleted", 0] }
                                                        ]
                                                }
                                            }
                                        },
                                        { "$project": { review: 1, rating: 1, user_id: 1, reference_id: 1, updated_at: 1 } }
                                    ],
                                    as: "recommendation"
                                }
                            },
                            { $unwind: "$recommendation" },
                            {
                                "$lookup": {
                                    from: "users",
                                    let: { ref_id: "$recommendation.user_id" },
                                    pipeline: [
                                        {
                                            $match:
                                            {
                                                $expr:
                                                {
                                                    $and:
                                                        [
                                                            { $eq: ["$_id", "$$ref_id"] },
                                                            { $eq: ["$status", 1] },
                                                            { $eq: ["$is_deleted", 0] }
                                                        ]
                                                }
                                            }
                                        },
                                        { "$project": { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 } }
                                    ],
                                    as: "user_id"
                                }
                            },
                            { $unwind: "$user_id" },
                            { $match: { "user_id.blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) } } },
                            {
                                $project: {
                                    _id: 1,
                                    //recommendation: 1,
                                    user_id: 1,
                                    //"recommendation_id": "$recommendation._id",
                                    "reference_id": "$recommendation.reference_id",
                                    "updated_at": "$recommendation.updated_at",
                                    "rating": "$recommendation.rating",
                                    "review": "$recommendation.review",
                                    comment_data: 1
                                }
                            }
                        ];
                        //console.log(util.inspect(aggregationCondition, { depth: null }));
                        let commentlist = comment.aggregate.sync(null, aggregationCondition);
                        if (commentlist.status == 1) {
                            console.log(commentlist["data"].length);
                            if (commentlist["data"].length == 0) {
                                var population = [{
                                    path: 'user_id',
                                    model: 'user',
                                    select: '_id firstName lastName profile_pic'
                                }]
                                var recommendation_data = recommendation.findOnePopulate.sync(null, { _id: mongoose.Types.ObjectId(req.query.recommendation_id), is_deleted: 0 }, { user_id: 1, rating: 1, review: 1, reference_id: 1, _id: 1, updated_at: 1 }, population);
                                recommendation_data.data = JSON.parse(JSON.stringify(recommendation_data.data));
                                recommendation_data.data.comment_count = 0;
                                //console.log("-----",recommendation_data.data);
                                res.status(200).send({ message: "success", status: 1, data: [recommendation_data.data] });
                            } else {
                                //console.log(util.inspect(commentlist, { depth: null }));
                                var comment_count = comment.count.sync(null, { recommendation_id: mongoose.Types.ObjectId(req.query.recommendation_id), is_deleted: 0 });
                                //console.log(comment_count);
                                commentlist["data"][0].comment_count = comment_count.data;
                                res.status(200).send({ message: "success", status: 1, data: commentlist["data"] });
                            }
                        } else {
                            throw commentlist;
                        }
                    } else {
                        throw { message: "private recommendation" };
                    }
                } else {
                    throw recommendationData;
                }
            } catch (err) {
                console.log(err.stack);
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
module.exports = router;