var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var contact_category = mongoose.model('contact_category');

//get request for conatct categories to send feedback
router.get('/', function (req, res, next)
{
    try
    {
        contact_category.find({is_deleted: 0, status: 1}, {status: 0, created_at: 0, updated_at: 0, __v: 0, is_deleted: 0}).sort({name: 1}).exec(function (contactError, contactData) {
            if (!contactError)
            {
                if (contactData.length > 0) {
                    res.status(200).send({
                        message: "Success",
                        status: 1,
                        data: contactData
                    });
                } else
                {
                    res.status(200).send({
                        message: "No data found.",
                        status: 1,
                        data: []
                    });
                }

            } else
            {
                res.status(400).send({
                    message: contactError.message,
                    status: 0
                });
            }
        });
    } catch (err)
    {
        res.status(400).send({message: err.message, status: 0});
    }
});

module.exports = router;
