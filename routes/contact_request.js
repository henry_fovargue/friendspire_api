var express = require('express');
var router = express.Router();
var Sync = require('sync');
var mongoose = require('mongoose');
var contact = require('../controller/contact.js');
var notification_controller = require('../controller/notification.js');
//var emails = require('../helper/emails.js');

router.post('/', function (req, res, next) {
    Sync(function () {
        try {
            //            req.body.message = req.body.message;
            if (req.body.message && req.body.subject_id) {
                var data = {
                    message: req.body.message,
                    subject: mongoose.Types.ObjectId(req.body.subject_id),
                    user_id: mongoose.Types.ObjectId(req.decoded._id)
                };
                var contactSave = contact.save.sync(null, data);
                if (contactSave.status == 1) {
                    console.log(contactSave);
                    var data_query = {
                        reference_id: contactSave.data._id,
                        //reference_user_id: req.decoded._id,
                        from: mongoose.Types.ObjectId(req.decoded._id),
                        to: mongoose.Types.ObjectId(global.admin),
                        type: 5,
                        message: req.body.message
                    };
                    var notification_data = notification_controller.create.sync(null, data_query);
                    if (notification_data.status == 1) {
                        console.log(notification_data);
                        res.status(200).send({ message: global.messages.contactUs, status: 1 });
                    } else {
                        res.status(400).send({ message: notification_data.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: contactSave.message, status: 0 });
                }
                //return next(contactSave.message);

                //            emails.contact_us_mail(req.body);
                // res.status(200).send({
                //     message: global.messages.querySubmitted,
                //     status: 1,
                // });
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) {
            //return next(err);
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});


module.exports = router;


