var express = require('express');
var router = express.Router();
var report = require('../controller/report.js');
var notification = require('../controller/notification.js');
var recommend = require('../controller/recommendations.js');
var Sync = require('sync');

router.post('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.recommendationid) {
                var dataRecomend = recommend.findOne.sync(null, { _id: req.body.recommendationid, is_deleted: 0 });
                //console.log(dataRecomend);
                if (dataRecomend.status == 1) {
                    if (dataRecomend.data.user_id == req.decoded._id) {
                        res.status(400).send({ message: "You can not report your review", status: 0 });
                    }
                    else {
                        var query = { users: { $elemMatch: { $eq: req.decoded._id } }, recommendation_id: req.body.recommendationid, is_deleted: 0 };
                        var data = { $push: { users: req.decoded._id }, $inc: { count: 1 } };
                        var data_list = report.findOne.sync(null, query);
                        if (data_list.status == 1) {
                            res.status(200).send({ message: global.messages.review_reported, status: 1 });
                        } else {
                            //console.log("----");
                            var updateddata_list = report.findOneAndUpdate.sync(null, { recommendation_id: req.body.recommendationid, is_deleted: 0 }, data);
                            //console.log(updateddata_list);
                            if (updateddata_list.status == 1) {
                                //console.log(updateddata_list);
                                if (updateddata_list.data.count >= 1) {
                                    //console.log("1");
                                    var condition = { reference_id: updateddata_list.data._id, is_deleted: 0 };
                                    var data_notify = {
                                        reference_id: updateddata_list.data._id,
                                        //reference_user_id: dataRecomend.data.user_id,
                                        from: global.admin,
                                        to: global.admin,
                                        message: "review is reported",
                                        type: 3
                                    };
                                    //console.log("1");
                                    //console.log("condition",condition);
                                    var data_field = notification.findOne_report.sync(null, condition);
                                    //console.log("data_field");
                                    //console.log(data_field);
                                    if (data_field.status == 1) {
                                        res.status(200).send({ message: global.messages.review_reported, status: 0 });
                                    } else {
                                        var updateddata_field = notification.create_for_admin.sync(null, data_notify);
                                        //console.log("updateddata_field");
                                        //console.log(updateddata_field);
                                        if (updateddata_field.status == 1) {
                                            res.status(200).send({ message: global.messages.review_reported, status: 1 });
                                        } else {
                                            res.status(400).send({ message: updateddata_field.message, status: 1 });
                                        }
                                    }
                                } else {
                                    res.status(200).send({ message: global.messages.success_report, status: 1 });
                                }
                            } else {
                                res.status(400).send({ message: updateddata_list.message, status: 0 });
                            }
                        }
                    }
                } else {
                    res.status(400).send({ message: dataRecomend.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});

module.exports = router;