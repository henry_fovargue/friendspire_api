var express = require('express');
var router = express.Router();
var recommend = require('../controller/recommendations.js');
var post = require('../controller/post.js');
var user = require('../controller/users.js');
var bookmark = require('../controller/bookmarks.js');
var report = require('../controller/report.js');
var mongoose = require('mongoose');
var Sync = require('sync');
var async = require('async');
const moment = require('moment');
var notification = require('../helper/notifications.js');
var notification_controller = require('../controller/notification.js');
var cuisine = require("../cuisine");
var comment = require('../controller/comment.js');
const momenttz = require('moment-timezone');
const projectionfield = require("../helper/projectionFields");
const config = require("../config");
const util = require("util");
//var olduser_rating;

function insertionOfDataNotification(friend_array, _id, referenceid, push_message) {
    var notification_data = [];
    for (var i = 0; i < friend_array.length; i++) {
        notification_data.push({
            reference_id: referenceid,
            recommendation_id: null,
            from: _id,
            to: friend_array[i],
            type: 6,
            message: push_message,
            is_deleted: 0,
            is_read: 0
        });
    }
    //console.log(notification_data);
    var notification_datalist = notification_controller.create.sync(null, notification_data);
    //console.log("----------------------",notification_datalist);
    if (notification_datalist.status == 1) {
        //console.log("~~~~~~~~~~~~~~~~~~~~~~~~entered");
        var batch_count_data = user.update.sync(null, { _id: { $in: friend_array }, is_deleted: 0, status: 1 }, { $inc: { batch_count: 1 } });
        //console.log("+++++++++++++++++++++",batch_count_data);
    }
    //console.log(notification_datalist);
}

function pushNotification(friend, token_array, referenceid, type, push_message) {
    // console.log(push_message);
    //    console.log("------------------------------", friend);?
    //console.log("friend.data[0].device_token------------------------------", friend.data[0].device_token);
    //console.log("friend.data[0].push_notification------------------------------", friend.data[0].push_notification);
    var data = {
        title: 'Friendspire',
        body: push_message,
        type: 6,
        post_id: referenceid,
        post_type: type
    };
    notification.send_notification_to_all(token_array, data);
}

function responseSend(req, res, next, data) {
    res.status(200).send({ message: "Success", status: 1, data: data });
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d * 1000;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.query.referenceid) {
                //                console.log(req.decoded._id);
                var query = { is_deleted: 0, user_id: req.decoded._id, reference_id: req.query.referenceid };
                var projection = { type: 1, _id: 1, reference_id: 1, user_id: 1, review: 1, rating: 1, updated_at: 1 };
                var data_list = recommend.findOne.sync(null, query);
                if (data_list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1, data: data_list.data });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.post('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.rating && req.body.referenceid && req.body.type) {
                //console.log(req.decoded._id);
                //console.log(req.decoded.blocked_user);
                var data = {};
                data.rating = req.body.rating;
                data.type = req.body.type;
                data.user_id = req.decoded._id;
                data.is_deleted = 0;
                data.reference_id = req.body.referenceid;
                if (req.body.review) {
                    data.review = req.body.review;
                } else {
                    data.review = "";
                }
                var user_private = user.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { _id: 0, is_private: 1, firstName: 1, lastName: 1 });
                if (user_private.data.is_private == 1) {
                    data.is_private = 1;
                } else {
                    data.is_private = 0;
                }
                var query_bookmark = { user_id: req.decoded._id, reference_id: req.body.referenceid, status: 1 };
                var recommenddata_list = recommend.findOne.sync(null, { user_id: req.decoded._id, reference_id: req.body.referenceid, is_deleted: 0 });
                if (recommenddata_list.status == 1) {
                    var post_name = "";
                    if (req.body.type == 1)
                        post_name = "Restaurant";
                    else if (req.body.type == 2)
                        post_name = "Bar";
                    else if (req.body.type == 3)
                        post_name = "Movie";
                    else if (req.body.type == 4)
                        post_name = "Series";
                    else if (req.body.type == 5)
                        post_name = "Book";
                    else
                        post_name = "post";
                    res.status(400).send({ message: "You have already review the " + post_name, status: 0 });
                } else {
                    var rating = post.findOne.sync(null, { _id: data.reference_id }, { _id: 0, type: 1, friendspire_rating: 1 });

                    if (rating.status == 1) {
                        //                        data.type = rating["data"]["type"];
                        var old_friendspire_rating = rating.data.friendspire_rating;
                        var bookmark_list = bookmark.findOne.sync(null, query_bookmark);
                        if (bookmark_list.status == 1) {
                            var bookmark_listupdate = bookmark.update.sync(null, query_bookmark, { status: 0 });
                            if (bookmark_listupdate.status == 0) {
                                throw bookmark_listupdate;
                            }
                        }
                        var data_list = recommend.findOneAndUpdate.sync(null, { user_id: req.decoded._id, reference_id: req.body.referenceid, is_deleted: 0 }, data);
                        if (data_list.status == 1) {
                            //comment.save.sync(null,{recommendation_id:data_list.data._id,post_id:req.body.referenceid});
                            var new_rating = data_list.data.rating;
                            var count = recommend.count.sync(null, { reference_id: data_list.data.reference_id, is_deleted: 0 });
                            if (count.status == 1) {
                                var val_count = count.data;
                                var updated_friendspire_rating = (((old_friendspire_rating * (val_count - 1)) + new_rating) / val_count).toFixed(1);
                                var val_update = post.update.sync(null, { _id: data_list.data.reference_id }, { friendspire_rating: updated_friendspire_rating, rated_at: new Date() });
                                if (val_update.status == 1) {
                                    var aggregate_query = [
                                        {
                                            $match: {
                                                //_id: mongoose.Types.ObjectId(req.decoded._id)
                                                friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id), status: 1, is_blocked: 0 } },
                                                _id: { $nin: req.decoded.blocked_user }
                                            }
                                        },
                                        //{ $unwind: "$friend_tokens" },
                                        {
                                            $project: {
                                                firstName: 1,
                                                lastName: 1,
                                                push_notification: 1,
                                                device_token: 1,
                                                is_private: 1,
                                                is_deleted: 1,
                                                status: 1,
                                                // friends: {
                                                //     $filter: {
                                                //         input: "$friends",
                                                //         as: "friends",
                                                //         cond: { $eq: ["$$friends.status", 1] }
                                                //     }
                                                // }
                                            }
                                        }
                                        // {
                                        //     $lookup:
                                        //         {
                                        //             from: "users",
                                        //             localField: "friends.user_id",
                                        //             foreignField: "_id",
                                        //             as: "friends"
                                        //     }
                                        // },
                                        // {
                                        //     $project: {
                                        //         firstName: 1,
                                        //         lastName: 1,
                                        //         push_notification: 1,
                                        //         device_token: 1,
                                        //         is_private: 1,
                                        //         is_deleted: 1,
                                        //         status: 1,
                                        //         "friends._id":1,
                                        //         "friends.push_notification":1,
                                        //         "friends.device_token":1
                                        //     }
                                        // },
                                    ];
                                    //console.log(util.inspect(aggregate_query, { depth: null, hidden: true }));
                                    var friend = user.aggregate.sync(null, aggregate_query);
                                    //console.log(util.inspect(friend, { depth: null, hidden: true }));
                                    //console.log(friend.data.length);
                                    //console.log(friend.data[0]._id);
                                    if (friend.status == 1) {
                                        var data = {
                                            new_friendspire_rating: parseFloat(updated_friendspire_rating),
                                            user_count: parseInt(val_count)
                                        };
                                        if (friend.data.length == 0) {
                                            async.parallel([
                                                function () {
                                                    responseSend(req, res, next, data);
                                                }
                                            ]);
                                        } else {
                                            var friend_array = [];
                                            //var token_array = [];
                                            for (let i = 0; i < friend.data.length; i++) {
                                                //console.log("111111111111111111111",friend.data[i]._id);
                                                friend_array.push(friend.data[i]._id);
                                                // if (friend.data[i].push_notification == 1 && friend.data[i].device_token) {
                                                //     token_array.push(friend.data[i].device_token);
                                                // }
                                            }
                                            //console.log(token_array);
                                            //                                            console.log(friend_array);
                                            //console.log(user_private);
                                            var push_message;
                                            push_message = global.push_messages.friendRecommendation;
                                            push_message = push_message.replace('$firstName', user_private.data.firstName);
                                            push_message = push_message.replace('$lastName', user_private.data.lastName);
                                            var category = ["Restaurant", "Bar", "Movie", "Series", "Book"];
                                            for (var j = 1; j < 6; j++) {
                                                if (j == req.body.type) {
                                                    push_message = push_message.replace('$category', category[j - 1]);
                                                }
                                            }
                                            var post_data = post.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.body.referenceid) }, { title: 1, _id: 1 });
                                            push_message = push_message.replace('$categoryName', post_data.data.title);
                                            // console.log(push_message);
                                            // console.log(post_data);
                                            async.parallel([
                                                function () {
                                                    responseSend(req, res, next, data);
                                                },
                                                function () {
                                                    insertionOfDataNotification(friend_array, req.decoded._id, req.body.referenceid, push_message);
                                                }
                                                // function () {
                                                //     pushNotification(friend, token_array, req.body.referenceid, req.body.type, push_message);
                                                // }
                                            ]);
                                        }
                                    } else {
                                        throw friend;
                                    }
                                } else {
                                    throw val_update;
                                }
                            } else {
                                throw count;
                            }
                        } else {
                            throw data_list;
                        }
                    } else {
                        throw rating;
                    }
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }


        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.put('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.rating && req.body.referenceid && req.body.type) {
                var referenceid = req.body.referenceid;
                var userupdated_rating = req.body.rating;
                var query = { is_deleted: 0 };
                query.user_id = req.decoded._id;
                query.reference_id = referenceid;
                var data = {};
                //                data.review = req.body.review;
                if (req.body.review) {
                    data.review = req.body.review;
                } else {
                    data.review = "";
                }
                data.rating = userupdated_rating;
                data.type = req.body.type;
                data.user_id = req.decoded._id;
                data.reference_id = referenceid;
                var find_list = recommend.findOne.sync(null, query);
                if (find_list.status == 1) {
                    var olduser_rating = find_list.data.rating;
                    var rating = post.findOne.sync(null, { _id: referenceid }, { _id: 0, friendspire_rating: 1 });
                    if (rating.status == 1) {
                        //                        data.type = rating["data"]["type"];
                        var data_list = recommend.update.sync(null, query, data);
                        var old_friendspire_rating = rating.data.friendspire_rating;
                        if (data_list.status == 1) {
                            var count = recommend.count.sync(null, { reference_id: referenceid, is_deleted: 0 });
                            if (count.status == 1) {
                                var val_count = count.data;
                                var updated_friendspire_rating = (old_friendspire_rating + (userupdated_rating - olduser_rating) / val_count).toFixed(1);
                                var val_update = post.update.sync(null, { _id: referenceid }, { friendspire_rating: updated_friendspire_rating, rated_at: new Date() });
                                var data = {
                                    new_friendspire_rating: parseFloat(updated_friendspire_rating),
                                    user_count: parseInt(val_count)
                                };
                                if (val_update.status == 1) {
                                    res.status(200).send({ message: "Success", status: 1, data: data });
                                } else {
                                    throw val_update;
                                }
                            } else {
                                throw count;
                            }
                        } else {
                            throw data_list;
                        }
                    } else {
                        throw rating;
                    }
                } else {
                    throw find_list;
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
router.delete('/', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.type && req.body.referenceid) {
                var query = { is_deleted: 0 };
                query.type = req.body.type;
                query.user_id = req.decoded._id;
                query.reference_id = req.body.referenceid;
                var data = { is_deleted: 1 };
                var find_data = recommend.findOne.sync(null, query);
                if (find_data.status == 1) {
                    var old_rating = post.findOne.sync(null, { _id: req.body.referenceid }, { _id: 0, friendspire_rating: 1 });
                    if (old_rating.status == 1) {
                        var data_list = recommend.update.sync(null, query, data);
                        if (data_list.status == 1) {
                            var count = recommend.count.sync(null, { reference_id: req.body.referenceid, is_deleted: 0 });
                            if (count.data == 0) {
                                var new_rating = 0;
                            } else {
                                var new_rating = ((old_rating.data.friendspire_rating * (count.data + 1)) - find_data.data.rating) / count.data;
                            }
                            var report_update = report.updateNew.sync(null, { recommendation_id: find_data.data._id, is_deleted: 0 }, { is_deleted: 1 });
                            if (report_update.status == 1) {
                                var val_update = post.update.sync(null, { _id: req.body.referenceid }, { friendspire_rating: new_rating, rated_at: new Date() });
                                if (val_update.status == 1) {
                                    var data = {
                                        new_friendspire_rating: parseFloat(new_rating),
                                        user_count: parseInt(count.data)
                                    };
                                    res.status(200).send({ message: "Success", status: 1, data: data });
                                } else {
                                    res.status(400).send({ message: val_update.message, status: 0 });
                                }
                            } else {
                                res.status(400).send({ message: report_update.message, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: data_list.message, status: 0 });
                        }
                    } else {
                        res.status(400).send({ message: old_rating.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: find_data.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});



var reviewlist_validate = (req, res, next) => {
    //add rules for validations
    if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
        req.assert("lat_long").notEmpty().withMessage('Please enter lat_long');
    }
    req.assert("category").notEmpty().withMessage('Please enter category');
    if (req.body.map != 1) {
        req.assert("page").notEmpty().withMessage('Please enter page number');
    }
    return req.validationErrors(true);
};

router.post("/reviewlist", (req, res, next) => {
    let errors = reviewlist_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                req.body.distance = parseFloat(req.body.distance);
                var user_id;
                var blockeduserarray = [];
                var friends = [];
                var timezone = req.headers.timezone;
                var distance_user = user.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 }, { distance_unit: 1 });
                if (req.body.userid) {
                    user_id = mongoose.Types.ObjectId(req.body.userid);
                    var userinfo = user.findOne.sync(null, { _id: user_id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, friends: 1, blocked_user: 1 });
                    if (userinfo.status === 1) {
                        if (userinfo["data"]["blocked_user"].includes(req.decoded._id)) {
                            throw { message: "you are currently blocked by this user" };
                        } else {
                            var selfinfo = user.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, blocked_user: 1 });
                            //                            blockeduserarray = selfinfo["data"]["blocked_user"].map(obj => {
                            //                                return mongoose.Types.ObjectId(obj);
                            //                            });
                            blockeduserarray = selfinfo["data"]["blocked_user"];
                            if (blockeduserarray.includes(user_id.toString())) {
                                throw { message: "user is currently blocked by you" };
                            }

                        }
                        if (userinfo["data"]["is_private"] == 1) {
                            let query = { _id: req.decoded._id, "friends": { $elemMatch: { user_id: req.body.userid, status: 1 } } };
                            let userpri = user.findOne.sync(null, query, {});
                            if (userpri.status == 0) {
                                throw { message: "This user has private account" };
                            }
                        }
                        for (var j = 0; j < userinfo.data.friends.length; j++) {
                            if (userinfo.data.friends[j].status == 1 && userinfo.data.friends[j].is_blocked == 0)
                                friends.push(mongoose.Types.ObjectId(userinfo.data.friends[j].user_id.toString()));
                        }
                        // console.log("------------------",friends);
                        var usersWhoBlockMe = user.find.sync(null, { blocked_user: { $eq: user_id }, is_deleted: 0, status: 1 }, { _id: 1 });
                        // console.log(usersWhoBlockMe);
                        var usersWhoBlockMe_array = [];
                        for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                            usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                        }
                        // console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);

                        friends = friends.filter(f => {
                            if (usersWhoBlockMe_array.includes(f.toString())) {
                                return false;
                            } else {
                                return true;
                            }
                        });
                        // console.log("+++++++++++++++++++",friends);
                    } else {
                        throw userinfo;
                    }

                } else {
                    user_id = mongoose.Types.ObjectId(req.decoded._id);
                    var userinfo = user.findOne.sync(null, { _id: user_id, is_deleted: 0, status: 1 }, { is_private: 1, distance_unit: 1, friends: 1 });
                    for (var j = 0; j < userinfo.data.friends.length; j++) {
                        if (userinfo.data.friends[j].status == 1 && userinfo.data.friends[j].is_blocked == 0)
                            friends.push(mongoose.Types.ObjectId(userinfo.data.friends[j].user_id.toString()));
                    }
                    // console.log("------------------",friends);
                    var usersWhoBlockMe = user.find.sync(null, { blocked_user: { $eq: user_id }, is_deleted: 0, status: 1 }, { _id: 1 });
                    // console.log(usersWhoBlockMe);
                    var usersWhoBlockMe_array = [];
                    for (var i = 0; i < usersWhoBlockMe.data.length; i++) {
                        usersWhoBlockMe_array.push(usersWhoBlockMe.data[i]._id.toString());
                    }
                    // console.log("usersWhoBlockMe_array", usersWhoBlockMe_array);

                    friends = friends.filter(f => {
                        if (usersWhoBlockMe_array.includes(f.toString())) {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    // console.log("+++++++++++++++++++",friends);
                }
                var recommendlist = recommend.find.sync(null, { user_id: user_id, type: parseInt(req.body.category), is_deleted: 0 });
                if (recommendlist.status === 0) {
                    throw recommendlist;
                }
                var recommendarray = recommendlist["data"].map(obj => {
                    return mongoose.Types.ObjectId(obj.reference_id)
                });
                //                console.log(recommendarray);

                var filter = { type: parseInt(req.body.category), dummy: 0 };
                var filterPagesDuration;
                filter["_id"] = { "$in": recommendarray };
                var page = req.body.page;
                var aggregation_pipeline = [];
                let projection = projectionfield.projectlist(req.body.category.toString());
                var ref_ids = [];
                //var sort = { "user_rating.updated_at": -1 };
                var sort = { "user_rating.rating": -1, _id: 1 };

                /* filters */
                if (req.body.search_text) {
                    filter["title"] = new RegExp('^' + req.body.search_text + '.*', 'i');
                }
                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (req.body.cuisine) {
                        if (Array.isArray(req.body.cuisine)) {
                            if (req.body.cuisine.includes("Other")) {
                                var index = req.body.cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    req.body.cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", req.body.cuisine);
                                req.body.cuisine = req.body.cuisine.concat(cuisine.other_cuisine);
                            }
                            filter["cuisine"] = { "$in": req.body.cuisine };
                        } else {
                            var array_cuisine = JSON.parse(req.body.cuisine);
                            // console.log(array_cuisine);
                            if (array_cuisine.includes("Other")) {
                                var index = array_cuisine.indexOf("Other");
                                // console.log(index);
                                if (index > -1) {
                                    array_cuisine.splice(index, 1);
                                }
                                // console.log("--------++++++------------", array_cuisine);
                                array_cuisine = array_cuisine.concat(cuisine.other_cuisine);
                            }
                            // console.log("--------------------", array_cuisine);
                            filter["cuisine"] = { "$in": array_cuisine };
                        }
                    }
                    if (req.body.price) {
                        //between 1 to 4
                        if (Array.isArray(req.body.price)) {
                            filter["price_tier"] = { "$in": req.body.price };
                        } else {
                            filter["price_tier"] = { "$in": JSON.parse(req.body.price) };
                        }
                    }
                } else if (req.body.category == 3 || req.body.category == 4) { // for movies and series

                    if (req.body.genre) {
                        if (Array.isArray(req.body.genre)) {
                            filter["genre"] = { "$in": req.body.genre };
                        } else {
                            filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                        }
                    }
                    if (req.body.release_year && req.body.category == 3) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.release_year && req.body.category == 4) {
                        //from 0 to current year
                        if (Array.isArray(req.body.release_year)) {
                            var array = req.body.release_year;
                        } else {
                            var array = JSON.parse(req.body.release_year);
                        }
                        if (array[0])
                            filter["start_year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["start_year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.duration) {
                        // for movie duraion
                        if (Array.isArray(req.body.duration)) {
                            var array = req.body.duration;
                        } else {
                            var array = JSON.parse(req.body.duration);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                } else if (req.body.category == 5) {
                    // for books
                    if (req.body.pages) {
                        // for movie duraion
                        if (Array.isArray(req.body.pages)) {
                            var array = req.body.pages;
                        } else {
                            var array = JSON.parse(req.body.pages);
                        }
                        var pages_condition_array = [];
                        for (var i = 0; i < array.length; i++) {
                            var new_json = [];
                            if (array[i][0])
                                new_json = { "$gte": parseInt(array[i][0]) };
                            if (array[i][1])
                                new_json = { "$gte": parseInt(array[i][0]), "$lte": parseInt(array[i][1]) };
                            //                            console.log(new_json);
                            pages_condition_array.push({ "pages_duration": new_json });
                        }
                        //                        console.log(new_json);
                        //                        filter["pages_duration"] = {$or: pages_condition_array};
                        filterPagesDuration = { $or: pages_condition_array };
                        //                        console.log(util.inspect(filterPagesDuration, {depth: null}));
                    }
                    if (req.body.publish_year) {
                        //from 0 to current year
                        if (Array.isArray(req.body.publish_year)) {
                            var array = req.body.publish_year;
                        } else {
                            var array = JSON.parse(req.body.publish_year);
                        }
                        if (array[0])
                            filter["year"] = { "$gte": parseInt(array[0]) };
                        if (array[1])
                            filter["year"] = { "$gte": parseInt(array[0]), "$lte": parseInt(array[1]) };
                    }
                    if (req.body.genre) {
                        if (Array.isArray(req.body.genre)) {
                            filter["genre"] = { "$in": req.body.genre };
                        } else {
                            filter["genre"] = { "$in": JSON.parse(req.body.genre) };
                        }
                    }
                }
                if (req.body.rating) {
                    //between 1 to 5
                    if (Array.isArray(req.body.rating)) {
                        var array = req.body.rating;
                    } else {
                        var array = JSON.parse(req.body.rating);
                    }
                    if (array[0])
                        filter["rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };


                }
                if (req.body.friendspire_rating) {
                    //between 1 to 10
                    if (Array.isArray(req.body.friendspire_rating)) {
                        var array = req.body.friendspire_rating;
                    } else {
                        var array = JSON.parse(req.body.friendspire_rating);
                    }
                    if (array[0])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]) };
                    if (array[1])
                        filter["friendspire_rating"] = { "$gte": parseFloat(array[0]), "$lte": parseFloat(array[1]) };
                }

                if (req.body.category == 1 || req.body.category == 2) {  //restaurant and bar
                    if (Array.isArray(req.body["lat_long"])) {
                        var coordinates = req.body["lat_long"];
                    } else {
                        var coordinates = JSON.parse(req.body["lat_long"]);
                    }
                    let distance;

                    if (req.body.distance) {
                        distance = parseFloat(req.body.distance) / 6371000; //in meters
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance,
                                    spherical: true
                                }
                            }
                        ]
                    } else {
                        //console.log("~~~~~~~~review~~~~~~~",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1){
                            distance = config.nearbyRaduisMiles;
                        } else{
                            distance = config.nearbyRaduisKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    //                                maxDistance: distance,
                                    spherical: true
                                }
                            }
                        ]
                    }

                    if (req.body.filterLatLong || req.body.distance == 0) {
                        //distance = parseFloat(req.body.distance) / 6371000; //in meters
                        //console.log("~~~~~~~~filterLatLong~~~~~~~",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1){
                            var distance_filterLatLong = config.nearbyRaduisMiles;
                        } else{
                            var distance_filterLatLong = config.nearbyRaduisKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance_filterLatLong,
                                    spherical: true
                                }
                            }
                        ]
                    }
                    if (req.body.map) {
                        //console.log("~~~~~~~~map~~~~~~~",distance_user.data.distance_unit);
                        if(distance_user.data.distance_unit == 1){
                            var distance_map = config.nearbyRaduisMiles;
                        } else{
                            var distance_map = config.nearbyRaduisKm;
                        }
                        aggregation_pipeline = [
                            {
                                $geoNear: {
                                    near: [coordinates[1], coordinates[0]],
                                    distanceField: "distance",
                                    query: filter,
                                    distanceMultiplier: 6371000,
                                    maxDistance: distance_map,
                                    spherical: true
                                }
                            }
                        ]
                    }
                } else {
                    aggregation_pipeline = [
                        { "$match": filter }
                    ];
                    if (filterPagesDuration) {
                        aggregation_pipeline.push({ "$match": filterPagesDuration });
                    }
                }

                if (req.body.category == 1 || req.body.category == 2) {  //restau
                    if (req.body.open_now == 1) {
                        let time = momenttz().tz(timezone);
                        let day_of_week = time.isoWeekday() - 1;
                        let inttime = parseInt(time.format("HHmm"));
                        var new_date = momenttz.tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                        var agg = [
                            { "$match": { timezone: timezone } },
                            {
                                $project: {
                                    timezone: 1,
                                    //                                    date1: {$toInt: "$date1"},
                                    dayOfWeek: 1,
                                    timings1: '$timings',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    timings: { $arrayElemAt: ['$timings', day_of_week] }
                                }
                            },
                            {
                                $match: { timings: { $ne: undefined } }
                            },
                            {
                                $project: {
                                    timezone: 1,
                                    date1: 1,
                                    dayOfWeek: 1,
                                    timings: '$timings1',
                                    type: 1, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
                                    reference_id: 1,
                                    title: 1,
                                    friendspire_rating: 1,
                                    rating: 1,
                                    image: 1,
                                    description: 1, //plot in case of book
                                    country: 1,
                                    city: 1,
                                    address: 1,
                                    location: 1, //store in the form of long lat
                                    price_tier: 1,
                                    phone: 1,
                                    url: 1,
                                    cuisine: 1,
                                    timezone: 1,
                                    bookmark: 1,
                                    distance: 1,
                                    updated_at: 1,
                                    date: 1,
                                    temp: {
                                        $filter: {
                                            input: '$timings',
                                            as: "num",
                                            cond: {
                                                $and: [
                                                    { $gte: [inttime, { $toInt: "$$num.start" }] },
                                                    { $lte: [inttime, { $toInt: "$$num.end" }] },
                                                ]
                                            }
                                        }
                                    }
                                }

                            },

                            {
                                $match: {
                                    'temp.0': { $exists: true }
                                }
                            }
                        ];
                        aggregation_pipeline = aggregation_pipeline.concat(agg);
                    }
                }

                let commonAggregaion = [
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", user_id] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "user_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $in: ["$user_id", friends] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", avgrating: { $avg: "$rating" }, count_friends: { $sum: 1 } } },
                                { "$project": { _id: 0, avgrating: 1, count_friends: 1 } },
                            ],
                            as: "friends_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "my_rating"
                        }
                    },
                    {
                        "$lookup": {
                            from: "bookmarks",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                                    { $eq: ["$status", 1] },
                                                ]
                                        }
                                    }
                                },
                            ],
                            as: "my_bookmark_status"
                        }
                    },
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }
                                },
                                { "$group": { _id: "$reference_id", count_everyone: { $sum: 1 } } },
                                { "$project": { _id: 0, count_everyone: 1 } },
                            ],
                            as: "everyone_count"
                        }
                    },
                    { "$sort": sort }, // Latest first
                    //{ "$skip": (page - 1) * global.pagination_limit },
                    //{ "$limit": global.pagination_limit },
                    { "$project": projection }
                ];

                if (req.body.map != 1 || req.body.map == undefined) {
                    commonAggregaion.push({ "$skip": (page - 1) * global.pagination_limit });
                    commonAggregaion.push({ "$limit": global.pagination_limit });
                }

                let countAggregation = [
                    {
                        "$lookup": {
                            from: "recommendations",
                            let: { ref_id: "$_id" },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr:
                                        {
                                            $and:
                                                [
                                                    { $eq: ["$reference_id", "$$ref_id"] },
                                                    { $eq: ["$user_id", user_id] },
                                                    { $eq: ["$is_deleted", 0] },
                                                ]
                                        }
                                    }

                                },
                            ],
                            as: "user_rating"
                        }
                    },
                    { "$sort": sort }, // Latest first
                    {
                        $group: {
                            _id: null,
                            count: { $sum: 1 }
                        }
                    }
                ];
                var countAggregationPipeline = aggregation_pipeline;
                countAggregationPipeline = countAggregationPipeline.concat(countAggregation);
                var countpost = post.aggregate.sync(null, countAggregationPipeline);
                //                console.log(countpost);

                if (countpost.status === 0) {
                    throw countpost;
                }
                var count;
                if (countpost["data"].length > 0) {
                    var count = countpost["data"][0]["count"];
                } else {
                    var count = 0;
                }
                aggregation_pipeline = aggregation_pipeline.concat(commonAggregaion);
                //                console.log(util.inspect(aggregation_pipeline, { depth: null }));
                let result = post.aggregate.sync(null, aggregation_pipeline);
                //console.log(util.inspect(result, { depth: null }));
                if (result.status == 1) {
                    result["data"] = result["data"].map(obj => {
                        if (req.body.category == 1 || req.body.category == 2) {
                            let closed_at = 0;
                            let is_open = 2;
                            if (obj["timezone"] && obj["timings"].length > 0) {
                                let time = momenttz().tz(obj["timezone"]);
                                let timing = obj["timings"][time.isoWeekday() - 1];
                                let inttime = time.format("HHmm");
                                timing.forEach(object => {
                                    closed_at = parseInt(object["end"]) > closed_at ? parseInt(object["end"]) : closed_at;
                                    if (parseInt(object["start"]) <= inttime && inttime <= parseInt(object["end"])) {
                                        is_open = 1;
                                    }
                                    //                                    let endTime;
                                    //                                    if (object["end"].charAt(0) == "+") {
                                    //                                        endTime = 2400 + parseInt(object["end"]);
                                    //                                    } else {
                                    //                                        endTime = parseInt(object["end"]);
                                    //                                    }
                                    //
                                    //                                    if (parseInt(object["start"]) <= inttime && inttime <= endTime) {
                                    //                                        is_open = 1;
                                    //                                    }
                                    //                                    closed_at = endTime > closed_at ? endTime : closed_at;
                                    //                                    if (closed_at >= 2400) {
                                    //                                        closed_at = closed_at - 2400;
                                    //                                    }
                                });
                                obj["closed_at"] = moment(closed_at.toString(), "HHmm").format("HH:mm");

                                delete obj["timings"];
                                delete obj["timezone"];
                            } else {
                                is_open = 0;
                            }
                            obj["is_open"] = is_open;
                            if (obj["location"]) {
                                obj["location"] = [obj["location"]["coordinates"][1], obj["location"]["coordinates"][0]]
                            }
                        }

                        if (obj["user_rating"].length > 0) {
                            obj["user_review"] = obj["user_rating"][0]["review"];
                            obj["user_rating"] = obj["user_rating"][0]["rating"];
                        } else {
                            obj["user_review"] = "";
                            obj["user_rating"] = 0;
                        }

                        if (obj["friends_rating"].length > 0) {
                            obj["count_friends"] = parseInt(obj["friends_rating"][0]["count_friends"]);
                            obj["friends_rating"] = parseFloat(obj["friends_rating"][0]["avgrating"]);

                        } else {
                            obj["count_friends"] = parseInt(0)
                            obj["friends_rating"] = parseFloat(0.00);
                        }

                        if (obj["my_rating"].length > 0) {
                            obj["my_rating"] = obj["my_rating"][0]["rating"];
                        } else {
                            obj["my_rating"] = 0;
                        }

                        if (obj["everyone_count"].length > 0) {
                            obj["everyone_count"] = parseInt(obj["everyone_count"][0]["count_everyone"]);

                        } else {
                            obj["everyone_count"] = parseInt(0)
                        }

                        if (obj["my_bookmark_status"].length > 0) {
                            obj["my_bookmark_status"] = obj["my_bookmark_status"][0]["status"];
                        } else {
                            obj["my_bookmark_status"] = 0;
                        }

                        if (!obj["cuisine"]) {
                            obj["cuisine"] = [];
                        }
                        if (obj["cuisine"].length < 1) {
                            obj["cuisine"] = [];
                        }
                        return obj;
                    });
                    res.status(200).send({ status: 1, message: "success", distance_unit: distance_user["data"]["distance_unit"], count: count, data: result["data"] });

                } else {
                    throw result;
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }

        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

router.get('/profilereview', function (req, res, next) {
    Sync(function () {
        try {
            if (!(req.query.profile_id && req.query.page)) {
                return res.status(400).send({ message: "Invalid request", status: 0 });
            }
            if (req.query.lat_long) {
                //                console.log("here");
                var coords = JSON.parse(req.query.lat_long);
            }
            req.query.profile_id = mongoose.Types.ObjectId(req.query.profile_id);
            var condition_bookmark = { status: 1, user_id: req.decoded._id };
            var projection_bookmark = { reference_id: 1, _id: 0 };
            var bookmark_post = [];
            var skip = (req.query.page - 1) * global.pagination_limit;
            var sort = { updated_at: -1 };
            var userinfo = user.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1 }, { friends: 1, distance_unit: 1 });
            if (userinfo.status === 0) {
                throw userinfo;
            }
            //            var condition_recommend = {is_deleted: 0, user_id: req.query.profile_id};
            //            var projection_recommend = {type: 1, _id: 1, reference_id: 1, user_id: 1, review: 1, rating: 1};
            //            var bookmark_list = bookmark.find.sync(null, condition_bookmark, projection_bookmark);
            //            if (bookmark_list.status == 1) {
            //                for (var i = 0; i < bookmark_list.data.length; i++) {
            //                    bookmark_post.push(mongoose.Types.ObjectId(bookmark_list.data[i].reference_id));
            //                }
            var condition = [{ $match: { user_id: req.query.profile_id, is_deleted: 0 } },
            {
                $project: {
                    type: 1, _id: 1, reference_id: 1, user_id: 1, updated_at: 1,
                    review: 1,
                    rating: 1
                }
            },
            //                    {
            //                        "$lookup": {
            //                            from: "bookmarks",
            //                            let: {ref_id: "$reference_id"},
            //                            pipeline: [
            //                                {
            //                                    $match:
            //                                            {
            //                                                $expr:
            //                                                        {
            //                                                            $and:
            //                                                                    [
            //                                                                        {$eq: ["$reference_id", "$$ref_id"]},
            //                                                                        {$eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)]},
            //                                                                        {$eq: ["$status", 1]},
            //                                                                    ]
            //                                                        }
            //                                            }
            //                                },
            //                                {"$group": {_id: null, count: {$sum: 1}}},
            //                                {"$project": {_id: 0, count: 1}},
            //                                {$unwind: "$count"},
            //                            ],
            //                            as: "bookmark"
            //                        }
            //                    },
            {
                $lookup: {
                    from: "posts",
                    localField: "reference_id",
                    foreignField: "_id",
                    as: "post"
                }
            },
            { $unwind: "$post" },
            {
                "$lookup": {
                    from: "recommendations",
                    let: { ref_id: "$reference_id" },
                    pipeline: [
                        {
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$reference_id", "$$ref_id"] },
                                            { $eq: ["$user_id", mongoose.Types.ObjectId(req.decoded._id)] },
                                            { $eq: ["$is_deleted", 0] },
                                        ]
                                }
                            }
                        },
                    ],
                    as: "user_rating"
                }
            },
            { $project: { "post.description": 0 } },
            { $sort: { updated_at: -1, _id: 1 } },
            { $skip: skip },
            { $limit: global.pagination_limit }
            ]
            var data_list = recommend.aggregate.sync(null, condition);
            if (data_list.status == 1) {
                data_list["data"].map(obj => {
                    if (obj["post"]["type"] == 1 || obj["post"]["type"] == 2) {
                        let closed_at = 0;
                        let is_open = 2;
                        if (obj["post"]["timezone"] && obj["post"]["timings"].length > 0) {
                            let time = momenttz().tz(obj["post"]["timezone"]);
                            let timing = obj["post"]["timings"][time.isoWeekday() - 1];
                            let inttime = time.format("HHmm");
                            timing.forEach(object => {
                                closed_at = parseInt(object["end"]) > closed_at ? parseInt(object["end"]) : closed_at;
                                if (parseInt(object["start"]) <= inttime && inttime <= parseInt(object["end"])) {

                                    is_open = 1;
                                }
                                //                                let endTime;
                                //                                if (object["end"].charAt(0) == "+") {
                                //                                    endTime = 2400 + parseInt(object["end"]);
                                //                                } else {
                                //                                    endTime = parseInt(obj["end"]);
                                //                                }
                                //
                                //                                if (parseInt(object["start"]) <= inttime && inttime <= endTime) {
                                //                                    is_open = 1;
                                //                                }
                                //                                closed_at = endTime > closed_at ? endTime : closed_at;
                                //                                if (closed_at >= 2400) {
                                //                                    closed_at = closed_at - 2400;
                                //                                }
                            });
                            obj["post"]["closed_at"] = moment(closed_at.toString(), "HHmm").format("HH:mm");

                            delete obj["post"]["timings"];
                            delete obj["post"]["timezone"];
                        } else {
                            is_open = 0;
                        }
                        obj["post"]["is_open"] = is_open;
                        if (obj["post"]["location"]) {
                            obj["post"]["location"] = [obj["post"]["location"]["coordinates"][1], obj["post"]["location"]["coordinates"][0]]
                        }
                        if (req.query.lat_long) {
                            obj["post"]["distance"] = getDistanceFromLatLonInKm(obj["post"]["location"][0], obj["post"]["location"][1], coords[0], coords[1]);
                        }
                    }
                    if (!obj["post"]["cuisine"]) {
                        obj["post"]["cuisine"] = [];
                    }

                    //                        if (obj["cuisine"].length < 1) {
                    //                            obj["cuisine"] = [];
                    //                        }

                    //                        if (obj["bookmark"][0]) {
                    //                            if (obj["bookmark"][0]["count"] > 0) {
                    //                                obj["is_bookmarked"] = true;
                    //                            }
                    //                        } else {
                    //                            obj["is_bookmarked"] = false;
                    //                        }
                    //                        delete obj["bookmark"];

                    if (obj["user_rating"].length > 0) {
                        obj["user_review"] = obj["user_rating"][0]["review"];
                        obj["user_rating"] = obj["user_rating"][0]["rating"];
                    } else {
                        obj["user_review"] = "";
                        obj["user_rating"] = 0;
                    }

                });
                res.status(200).send({ message: "Success", status: 1, distance_unit: userinfo["data"]["distance_unit"], data: data_list.data });
            } else {
                res.status(400).send({ message: data_list.message, status: 0 });
            }
            //            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
module.exports = router;

