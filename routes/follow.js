var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
var Sync = require('sync');
var notification = require('../helper/notifications.js');
var notification_controller = require('../controller/notification.js');

router.get('/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            // var blockeduser_array = [];
            // for (var i = 0; i < req.decoded.friends.length; i++) {
            //     if (req.decoded.friends[i].is_blocked == 1) {
            //         blockeduser_array.push(req.decoded.friends[i].user_id);
            //     }
            // }
            //req.decoded.blocked_user.push(mongoose.Types.ObjectId(req.decoded._id));
            //console.log(req.decoded.blocked_user);
            var _skip = (req.params.page_no - 1) * global.pagination_limit;
            var query = { _id: { $nin: req.decoded.blocked_user }, blocked_user: { $ne: mongoose.Types.ObjectId(req.decoded._id) }, friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id), status: 1, is_blocked: 0 } }, is_deleted: 0, status: 1 };
            var projection = { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 };
            var sort = { firstName: 1, _id: 1 };
            var follow_list = users.find_pagination_sort.sync(null, query, projection, _skip, global.pagination_limit, sort);
            //            var _skip = (req.params.page_no - 1) * global.pagination_limit;
            //            var query = [{$match: {'friends.user_id': mongoose.Types.ObjectId(req.decoded._id), 'friends.status': 1}},
            //                {$project: {_id: 1, firstName: 1, lastName: 1, profile_pic: 1}},
            //                {$skip: _skip},
            //                {$limit: global.pagination_limit}];
            //            var follow_list = users.aggregate.sync(null, query);
            if (follow_list.status == 1) {
                var user_data = users.findOne.sync(null, { _id: req.decoded._id }, { friends: 1 });
                for (var i = 0; i < follow_list.data.length; i++) {
                    for (var j = 0; j < user_data.data.friends.length; j++) {
                        follow_list.data[i].status = 0;
                        if (user_data.data.friends[j].user_id == follow_list.data[i]._id) {
                            if (user_data.data.friends[j].status == 1)
                                follow_list.data[i].status = 1;
                            else if (user_data.data.friends[j].status == 2)
                                follow_list.data[i].status = 2;
                            else
                                follow_list.data[i].status = 3;
                            break;
                        } else {
                            continue;
                        }
                    }
                    if (user_data.data.friends.length == 0) {
                        follow_list.data[i].status = 0;
                    }
                }
                var pages = 0;
                var total_count = users.count.sync(null, query);
                if (total_count.status == 1) {
                    pages = Math.ceil(total_count.data / global.pagination_limit)
                }
                res.status(200).send({ message: "Success", status: 1, pages: pages, data: follow_list.data });
            } else {
                res.status(400).send({ message: follow_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});
router.put('/', function (req, res, next) {

    Sync(function () {
        try {
            if (req.body.friend_id) {
                // console.log(req.decoded._id);
                // console.log(req.decoded.firstName);
                // console.log(req.decoded.lastName);
                var update_json = {};
                var send_status = 0;
                var user_data = users.findOne.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0, status: 1 }, { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, is_private: 1, friends: 1, push_notification: 1, device_token: 1, batch_count: 1 });
                // console.log(user_data);
                if (user_data.status == 1) {
                    if (user_data.data.is_private == 1) {
                        update_json = { 'friends.$.status': 2 };
                        send_status = 2;
                    } else {
                        update_json = { 'friends.$.status': 1 };
                        send_status = 1;
                    }
                    var find_friend = users.findOne.sync(null, { _id: req.decoded._id, 'friends.user_id': mongoose.Types.ObjectId(req.body.friend_id) }, {})
                    if (find_friend.status != 1) {
                        users.update.sync(null, { _id: req.decoded._id }, { $push: { friends: { user_id: mongoose.Types.ObjectId(req.body.friend_id), status: 0 } } });
                    }
                    var update_user = users.update.sync(null, { _id: req.decoded._id, 'friends.user_id': mongoose.Types.ObjectId(req.body.friend_id) }, update_json);
                    if (update_user.status == 1) {

                        var friend_status = find_follow_status(req.body.friend_id, req.decoded._id);
                        // console.log("friend_status", friend_status);
                        var data_send = {
                            _id: user_data.data._id,
                            firstName: user_data.data.firstName,
                            lastName: user_data.data.lastName,
                            profile_pic: user_data.data.profile_pic,
                            status: friend_status
                        };

                        var push_message, database_message;
                        if (user_data.data.is_private == 1) {
                            push_message = global.push_messages.requestPushReceive;
                            push_message = push_message.replace('$firstName', req.decoded.firstName);
                            push_message = push_message.replace('$lastName', req.decoded.lastName);

                            database_message = global.push_messages.requestReceive;
                            database_message = database_message.replace('$firstName', req.decoded.firstName);
                            database_message = database_message.replace('$lastName', req.decoded.lastName);
                        } else {
                            push_message = global.push_messages.requestReceiveForPublicPush;
                            push_message = push_message.replace('$firstName', req.decoded.firstName);
                            push_message = push_message.replace('$lastName', req.decoded.lastName);

                            database_message = global.push_messages.requestReceiveForPublic;
                            database_message = database_message.replace('$firstName', req.decoded.firstName);
                            database_message = database_message.replace('$lastName', req.decoded.lastName);
                        }
                        if (user_data.data.push_notification == 1 && user_data.data.device_token != "") {
                            var data = {
                                title: 'Friendspire',
                                body: push_message,
                                type: 4,
                                user_id: req.decoded._id,
                                friend_id: req.body.friend_id
                            };
                            //console.log("1",user_data.data.user_id.device_token)
                            notification.send_notification(user_data.data.device_token, data, user_data.data.batch_count+1);
                        }
                        var query_data = {
                            reference_id: req.body.friend_id,
                            from: req.decoded._id,
                            to: req.body.friend_id,
                            type: 4,
                            message: database_message
                        };
                        // var notification_find = notification_controller.findOne.sync(null, { from: req.decoded._id, to: req.body.friend_id, type: 2 });
                        // console.log(notification_find);
                        var notification_data = notification_controller.create.sync(null, query_data);
                        // console.log(notification_data);
                        if (notification_data.status == 1) {
                            var batch_count_data = users.update.sync(null, { _id: mongoose.Types.ObjectId(req.body.friend_id), is_deleted: 0, status: 1 }, { $inc: { batch_count: 1 } });
                            // console.log(batch_count_data);
                            if (batch_count_data.status == 1) {
                                res.status(200).send({ message: "Success", status: 1, request_status: send_status, data: data_send });
                            } else {
                                res.status(400).send({ message: batch_count_data.message, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: notification_data.message, status: 0 });
                        }
                    } else
                        res.status(400).send({ message: update_user.message, status: 0 });
                } else {
                    res.status(400).send({ message: global.messages.userBlockMessage, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });

});


var find_follow_status = function (friend_id, my_id) {
    var query =
        [{ $match: { '_id': mongoose.Types.ObjectId(my_id), 'friends.user_id': mongoose.Types.ObjectId(friend_id) } },
        {
            $project: {

                friend: {
                    $filter: {
                        input: "$friends",
                        as: "items",
                        cond: { $eq: ["$$items.user_id", mongoose.Types.ObjectId(friend_id)] }
                    }
                }
            }
        },
        { $project: { status: '$friend.status' } },
        { $unwind: "$status" }
        ];
    var status_data = users.aggregate.sync(null, query);
    if (status_data.status == 1) {
        if (status_data.data.length)
            return (status_data.data[0].status);
        else
            return (0);
    } else {
        return (0);
    }
}
module.exports = router;