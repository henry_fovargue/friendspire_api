var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
var emails = require('../helper/emails.js');
var recommend = require('../controller/recommendations.js');
var bookmark = require('../controller/bookmarks.js');
var comment = require('../controller/comment.js');
var mongoose = require('mongoose');

//get profile detail of user
router.get('/:profileId', function (req, res, next) {
    Sync(function () {
        try {
            if (req.params.profileId) {
                console.log(req.params.profileId);
                var user_id = req.params.profileId;
                var myinfo = users.findOne.sync(null, { _id: req.decoded._id, is_deleted: 0, status: 1 }, { _id: 1, blocked_user: 1 });
                if (myinfo["status"] == 1) {
                    if (myinfo["data"]["blocked_user"].includes(user_id)) {
                        throw { message: "user is currently blocked by you" };
                    }
                } else {
                    throw myinfo;
                }
            } else {
                var user_id = req.decoded._id;
            }

            var user_data = users.findOne.sync(null, { _id: user_id, is_deleted: 0, status: 1 }, { _id: 1, firstName: 1, lastName: 1, email: 1, profile_pic: 1, is_email_verified: 1, address: 1, dob: 1, gender: 1, longlat: 1, blocked_user: 1, tutorial_read: 1, reference_code: 1, referral_code: 1 });
            if (user_data.status === 1) {
                if (user_data["data"]["blocked_user"].includes(req.decoded._id.toString())) {
                    throw { message: "you are currently blocked by user" };
                }
                res.status(200).send({ message: "success", status: 1, data: user_data.data });
            } else {
                throw Error("Invalid user.");
            }
        } catch (err) {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});


var update_profile_validate = function (req, res, next) {
    //req.assert('dob', 'Please enter dob').notEmpty();
    req.assert('gender', 'Please enter gender').notEmpty();
    //req.assert('address', 'Please enter address').notEmpty();
    req.assert('lastName', 'Please enter lastName').notEmpty();
    req.assert('firstName', 'Please enter firstName').notEmpty();
    //req.assert('longlat', 'Please enter longlat').notEmpty();
    //req.assert('profile_pic', 'Please provide the profile image.').notEmpty();
    return req.validationErrors(true);
};

//route for update profile
router.put('/', function (req, res, next) {
    Sync(function () {
        try {

            var errors = update_profile_validate(req, res, next);
            var query_data = {};
            if (!errors) {
                console.log(req.body);
                if (req.body.address && req.body.longlat && req.body.dob) {
                    query_data = { is_address_added: 1, address: req.body.address, longlat: req.body.longlat, dob: req.body.dob };
                } else
                    if (req.body.address && req.body.longlat) {
                        query_data = { is_address_added: 1, address: req.body.address, longlat: req.body.longlat };
                    } else
                        if (req.body.dob) {
                            query_data = { dob: req.body.dob };
                        }
                // console.log(query_data.longlat);
                query_data.longlat = query_data.longlat.reverse();
                // console.log(query_data.longlat);
                if (req.body.profile_pic) {
                    query_data.profile_pic = req.body.profile_pic;
                }
                var user_data = users.findOne.sync(null, { _id: req.decoded._id }, {});
                if (user_data.status === 1) {
                    if (user_data.data.status === 1 && user_data.data.is_deleted === 0) {
                        // req.body.email = req.body.email.toLowerCase();
                        // if (req.body.email) {
                        //     delete req.body.email;
                        // }
                        var response_message = global.messages.profileDataUpdated;
                        // if (user_data.data.email != req.body.email) {
                        //     var email_user = users.findOne.sync(null, { email: req.body.email }, {});
                        //     if (email_user.status === 1) {
                        //         res.status(400).send({ message: global.messages.emailAlreadyExists, status: 0 });
                        //     } else {
                        //         emails.change_email(user_data.data, req.body.email);
                        //         response_message = global.messages.verificationLinkSent
                        //         req.body.is_email_changed = 1;
                        //         req.body.temp_email = req.body.email;
                        //         delete req.body.email;
                        //     }
                        // }
                        // update profile


                        // query_data = {
                        //     //is_address_added: 1,
                        //     //address: req.body.address,
                        //     //longlat: req.body.longlat,
                        //     lastName: req.body.lastName,
                        //     firstName: req.body.firstName,
                        //     //dob: dateOfBirth,
                        //     gender: req.body.gender,
                        //     profile_pic: req.body.profile_pic
                        // };
                        query_data.lastName = req.body.lastName;
                        query_data.firstName = req.body.firstName;
                        query_data.gender = req.body.gender;
                        query_data.profile_pic = req.body.profile_pic;
                        // console.log(query_data);
                        var data_update = users.update.sync(null, { _id: req.decoded._id }, query_data);
                        if (data_update.status === 1) {
                            res.status(200).send({ message: response_message, status: 1, data: query_data });
                        } else {
                            res.status(400).send({ message: data_update.message, status: 0 });
                        }
                    } else if (user_data.data.status === 0) {
                        res.status(400).send({ message: global.messages.profileDataUpdated, status: 0 });
                    } else {
                        res.status(400).send({ message: global.messages.emailNotVerified, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: user_data.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            console.log(err);
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
    //    } else
    //    {
    //        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    //    }

});


//route for update email
router.put('/email_update', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.email) {
                var user_data = users.findOne.sync(null, { _id: req.decoded._id }, {});
                if (user_data.status === 1) {
                    if (user_data.data.status === 1 && user_data.data.is_deleted === 0) {
                        req.body.email = req.body.email.toLowerCase();
                        var response_message = global.messages.profileDataUpdated;
                        if (user_data.data.email != req.body.email) {
                            var email_user = users.findOne.sync(null, { email: req.body.email }, {});
                            if (email_user.status === 1) {
                                res.status(400).send({ message: global.messages.emailAlreadyExists, status: 0 });
                            } else {
                                emails.change_email_otp.sync(null, user_data.data, req.body.email);
                                response_message = global.messages.emailChangeOtp
                                req.body.is_email_changed = 1;
                                req.body.temp_email = req.body.email;
                                delete req.body.email;
                                console.log(req.body);
                                // update profile data
                                var data_update = users.updateNew.sync(null, { _id: req.decoded._id }, req.body);
                                if (data_update.status === 1) {
                                    res.status(200).send({ message: response_message, status: 1 });
                                } else {
                                    res.status(400).send({ message: data_update.message, status: 0 });
                                }
                            }
                        } else {
                            res.status(400).send({ message: global.messages.emailSameError, status: 0 });
                        }
                    } else if (user_data.data.status === 0) {
                        res.status(400).send({ message: global.messages.profileDataUpdated, status: 0 });
                    } else {
                        res.status(400).send({ message: global.messages.emailNotVerified, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: user_data.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
    //    } else
    //    {
    //        res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    //    }

});

//otp-verfication
router.post('/otpVerify', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.otp) {
                var user_data = users.findOne.sync(null, { _id: req.decoded._id }, {});
                //console.log(user_data);
                if (user_data.status === 1) {
                    if (user_data.data.status === 1 && user_data.data.is_deleted === 0) {
                        if (user_data.data.email_otp_code == req.body.otp) {
                            var current_date = new Date();
                            var check_otp_expire = users.findOne.sync(null, { _id: req.decoded._id, email_otp_code: req.body.otp, email_otp_expiry: { $gte: current_date } }, {});
                            //console.log(check_otp_expire);
                            if (check_otp_expire.status === 1) {
                                // console.log(check_otp_expire.data.email);
                                // console.log(check_otp_expire.data.temp_email);
                                // console.log(check_otp_expire.data.is_email_verified);
                                // console.log(check_otp_expire.data.is_email_changed);
                                var user_update_data = users.update.sync(null, { _id: req.decoded._id, status: 1, is_deleted: 0 }, { email: check_otp_expire.data.temp_email, temp_email: "", is_email_changed: 0, is_email_verified: 1, email_otp_code: "" });
                                if (user_update_data.status == 1) {
                                    res.status(200).send({ message: user_update_data.message, status: 1, data: user_update_data.data });
                                } else {
                                    res.status(400).send({ message: user_update_data.message, status: 0 });
                                }
                            } else {
                                res.status(400).send({ message: global.messages.otpExpired, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: global.messages.otpEnteredIncorrect, status: 0 });
                        }
                    } else {
                        res.status(400).send({ message: "user not found", status: 0 });
                    }
                } else {
                    throw Error("Invalid user.");
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});



//update profile pic
router.put('/profile_pic', function (req, res, next) {
    var errors = profile_pic_req_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                users.update.sync(null, { _id: req.decoded._id }, { profile_pic: req.body.profile_pic });
                res.status(200).send({ message: "Profile image updated successfully.", status: 1 });
            } catch (err) {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

//update address
router.put('/addressUpdate', function (req, res, next) {
    Sync(function () {
        try {
            if (req.body.address && req.body.long && req.body.lat) {
                var query = { _id: req.decoded._id, is_deleted: 0 };
                var data = {};
                data.is_address_added = 1;
                data.address = req.body.address;
                data.longlat = [req.body.long, req.body.lat];
                var data_list = users.update.sync(null, query, data);
                if (data_list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1 });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});

//delete user profile
router.put('/deleteAccount', function (req, res, next) {
    Sync(function () {
        try {
            //delete comments
            var comment_list = comment.updateMultiple.sync(null, { is_deleted: 0, user_id: req.decoded._id }, { is_deleted: 1 });
            if (comment_list.status == 1) {
                //delete recomendations
                var data_list = recommend.updateMultiple.sync(null, { is_deleted: 0, user_id: req.decoded._id }, { is_deleted: 1 });
                if (data_list.status == 1) {
                    //console.log("1");
                    //delete bookmarks
                    var bookmark_data = bookmark.updateNew.sync(null, { status: 1, user_id: req.decoded._id }, { status: 0 });
                    if (bookmark_data.status == 1) {
                        //console.log("2");
                        // delete from friends listing
                        //var update_users = users.updateNew.sync(null, { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id) } } }, { $pull: { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id) } } } });
                        var update_users = users.updateNew.sync(null, { friends: { $elemMatch: { user_id: mongoose.Types.ObjectId(req.decoded._id) } } }, { $pull: { friends: { 'user_id': mongoose.Types.ObjectId(req.decoded._id) } } });
                        if (update_users.status == 1) {
                            //console.log("3");
                            // delete account
                            var update_user_account = users.update.sync(null, { _id: req.decoded._id, is_deleted: 0 }, { is_deleted: 1 });
                            if (update_user_account.status == 1) {
                                //console.log("4");
                                res.status(200).send({ message: "Your account has been deleted successfully.", status: 1 });
                            } else {
                                res.status(400).send({ message: update_user_account.message, status: 0 });
                            }
                        } else {
                            res.status(400).send({ message: update_users.message, status: 0 });
                        }
                    } else {
                        res.status(400).send({ message: bookmark_data.message, status: 0 });
                    }
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: comment_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});


//tutorial read
router.put('/tutorialRead', function (req, res, next) {
    Sync(function () {
        try {
            console.log(req.body.is_tutorial_read);
            if (req.body.is_tutorial_read !== undefined && req.body.is_tutorial_read !== null) {
                var query = { _id: req.decoded._id, is_deleted: 0 };
                var data = {};
                if (req.body.is_tutorial_read == 1) {
                    data.tutorial_read = req.body.is_tutorial_read;
                } else if (req.body.is_tutorial_read == 0) {
                    data.tutorial_read = req.body.is_tutorial_read;
                } else {
                    res.status(400).send({ message: "wrong data value to update", status: 0 });
                }
                var data_list = users.update.sync(null, query, data);
                if (data_list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1 });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});


//function to validate profile request params
var req_validate = function (req, res, next) {
    req.assert('name', 'Please enter your name.').notEmpty();
    req.assert('address', 'Please enter your address.').notEmpty();
    return req.validationErrors(true);
};

//function to validate profile_pic req
var profile_pic_req_validate = function (req, res, next) {
    req.assert('profile_pic', 'Please provide the profile image.').notEmpty();
    return req.validationErrors(true);
};
module.exports = router;
