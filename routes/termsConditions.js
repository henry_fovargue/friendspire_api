var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var cms_page = mongoose.model('cms_page');
var template_json = require('../emails_templates'); // get email template json file

// cms page for T&C
var terms_condition = template_json.cms_pages.termsConditions;

/* Get method for cms_page */
router.get('/', function (req, res, next)
{
    try
    {
        // find cms-page Terms & Conditions
        cms_page.findOne({_id: terms_condition}, {_id: 0, heading: 1, description: 1}, function (pageError, pageData) {
            if (!pageError)
            {
                res.render("term", {message: "Success", status: 1, data: pageData.description});
            } else
            {
                res.send({message: pageError.message, status: 0});
            }
        });
    } catch (err)
    {
        console.log(err);
        res.send({message: "Error", status: 0});
    }
});

module.exports = router;
