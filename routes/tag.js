var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../controller/users.js');
const util = require("util");
var Sync = require('sync');

router.post('/:text/:page_no', function (req, res, next) {
    Sync(function () {
        try {
            // console.log(req.body.user_id);
            // console.log(req.body.user_id.length);
            if (req.body.user_id.length != 0) {
                for (var i = 0; i < req.body.user_id.length; i++) {
                    req.decoded.blocked_user.push(mongoose.Types.ObjectId(req.body.user_id[i]));
                }
            }
            var _skip = (req.params.page_no - 1) * global.pagination_limit;
            req.decoded.blocked_user.push(mongoose.Types.ObjectId(req.decoded._id));
            search_firstName = req.params.text.split(' ')[0];
            search_lastName = req.params.text.split(' ')[1];
            var condition = { _id: { $nin: req.decoded.blocked_user }, is_deleted: 0, status: 1 };
            condition.firstName = new RegExp('^' + search_firstName + '.*', 'i');
            if (search_lastName != '' && search_lastName != undefined) {
                condition.firstName = new RegExp('^' + search_firstName, 'i');
                condition.lastName = new RegExp(search_lastName + '.*', 'i');
            }
            var query = [
                { $match: condition },
                { $project: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1, blocked_user: 1 } },
                { $match: { "blocked_user": { $ne: mongoose.Types.ObjectId(req.decoded._id) } } },
                { $project: { _id: 1, firstName: 1, lastName: 1, profile_pic: 1 } },
                { $sort: { firstName: 1 } },
                { $skip: _skip },
                { $limit: global.pagination_limit }
            ];
            //console.log(util.inspect(query, { depth: null }));
            var users_list = users.aggregate.sync(null, query);
            //console.log(util.inspect(users_list, { depth: null }));
            if (users_list.status == 1) {
                res.status(200).send({ message: "Success", status: 1, data: users_list.data });
            } else {
                res.status(400).send({ message: users_list.message, status: 0 });
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});
module.exports = router;