var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var loginResponse = require('../helper/login_res_json.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sync = require('sync');

//route for verify a user
router.post('/', function (req, res, next)
{
    var errors = otp_verification_validate(req, res, next);
    if (!errors)
    {
        Sync(function ()
        {
            try
            {
                var user_data = users.findOne.sync(null, {_id: req.decoded._id}, {});
                if (user_data.status == 1) {
                    if (user_data.data.status === 1) {
                        res.status(400).send({message: "Your account is already verified please login.", status: 0});
                    }
                    else {
                        if (user_data.data.email_otp_code != req.body.otp_code){
                            return res.status(200).send({message: global.messages.otpEnteredIncorrect, status: 0});
                        } else {
                            var current_date = new Date();
                            var check_otp_expire = users.findOne.sync(null, {_id: req.decoded._id, email: user_data.data.email, email_otp_code: req.body.otp_code, email_otp_expiry: {$gte: current_date}}, {});
                            var data_update = {is_email_verified: 1, status: 1,email_otp_code:''}; // coming first time
                            if (check_otp_expire.status === 0)
                                return res.status(200).send({message: global.messages.resendOtp, status: 2});
                            //~ throw Error("Your code has been expired. Please generate new code through click on RESEND OTP ");
                            users.update.sync(null, {_id: req.decoded._id, email_otp_code: req.body.otp_code}, data_update);

                            user_data.data.is_email_verified = 1;
                            var token_obj = {_id:user_data.data._id,email:user_data.data.email,firstName:user_data.data.firstName,lastName:user_data.data.lastName}
                            var token = jwt.sign(token_obj, global.secret, {});
                            user_data.data.token = token;
                            var response = loginResponse.login_response_json(user_data.data);
                            res.send({message: global.messages.otpVerifiedSuccessfully, status: 1, data: response});


                        }
                    }

                } else {
                    throw Error("Invalid user");
                }
            } catch (err) {
                console.log(err);
                res.send({message: err.message, status: 0});
            }
        });
    } else {
        res.send({message: "Validation Errors", status: 0, errors: errors});
    }
});

var otp_verification_validate = function (req, res, next) {
    req.assert('otp_code', 'Please enter OTP to proceed').notEmpty();
    return req.validationErrors(true);
};

module.exports = router;
