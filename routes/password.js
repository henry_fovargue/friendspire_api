var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
// require passport-hash module for encrypt the password
var passwordHash = require('password-hash');

//route for change password of a user
router.put('/', function (req, res, next)
{
    var errors = change_password_validate(req, res, next);
    if (!errors)
    {
        Sync(function ()
        {
            try
            {
                var user_data = users.findOne.sync(null, {_id: req.decoded._id},{});
                if (user_data.status === 1)
                {
                    if (passwordHash.verify(req.body.password, user_data.data.password))
                    {
                        var new_password = passwordHash.generate(req.body.new_password);
                        
                        // update new password of a user
                        users.update.sync(null, {_id: req.decoded._id}, {password: new_password});
                        res.status(200).send({message: "Password Updated successfully", status: 1});
                    } else
                    {
                        res.status(200).send({message: "Current password is wrong", status: 0});
                    }
                } else
                {
                    throw Error("Invalid user");
                }
            } catch (err) // catch errors
            {
                 res.status(400).send({message: err.message, status: 0});
            }
        });
    } else
    {
         res.status(400).send({message: "Validation Errors", status: 0, errors: errors});
    }
});

var change_password_validate = function (req, res, next) {
    req.assert('password', 'password key is required').notEmpty();
    req.assert('new_password', 'new_password key is required').notEmpty();
    return req.validationErrors(true);
};

module.exports = router;
