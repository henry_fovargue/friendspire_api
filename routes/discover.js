var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Sync = require('sync');
var users = require('../controller/users.js');
var notifications = require('../controller/notification.js');
var discover_algo = require('../helper/discover_daily.js');
const util = require("util");
router.get('/:page_no', function (req, res, next) {
    Sync(function () {
        // console.log(req.query);
        try {
            if (req.query.lat && req.query.long && req.params.page_no && req.query.current_city) {
                //console.log(req.decoded._id);
//                console.log(req.params.page_no)
                
                var notification_data = users.findOne.sync(null, {_id: mongoose.Types.ObjectId(req.decoded._id), is_deleted: 0, status: 1}, {batch_count: 1});
                //console.log(notification_data);
                if (notification_data.status == 1) {
                    var data = discover_algo.discover.sync(null, req, res, req.decoded._id, [parseFloat(req.query.lat), parseFloat(req.query.long)]);
                    //console.log(data);
                    //console.log(util.inspect(data, { depth: null }));
                    res.status(200).send({message: "Success", status: 1, data: data, notification_count: notification_data.data.batch_count});
                } else {
                    res.status(400).send({message: notification_data.message, status: 0});
                }
            } else
                res.status(400).send({message: "Invalid request", status: 0});
        } catch (err) // catch errors
        {
            res.send({message: err.message, status: 0});
        }
    });
});
module.exports = router;