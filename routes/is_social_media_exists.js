var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
var loginResponse = require('../helper/login_res_json.js');
// require jsonwebtoken
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens



//route for fb exists or not
router.post('/', function (req, res, next) {
    // validate req params
    var errors = login_social_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                var check_sns_id = users.findOne.sync(null, { sns_id: req.body.sns_id, is_deleted: 0, status: 1 }, {});

                if (check_sns_id.status === 1) // user found
                {
                    if (check_sns_id.data.status == 0) {
                        res.status(200).send({ message: global.messages.accountDisabledByAdmin, status: 0 });
                    } else {
                        var token_obj = { _id: check_sns_id.data._id, email: check_sns_id.data.email, firstName: check_sns_id.data.firstName, lastName: check_sns_id.data.lastName }
                        // Create token with default (HMAC SHA256) Algorithm
                        var token = jwt.sign(token_obj, global.secret, {});
                        if (token) // check for token is created or not
                        {

                            // make mongoose object to json object that can be edited
                            var user1 = JSON.parse(JSON.stringify(check_sns_id.data));
                            user1.token = token;
                            var response = loginResponse.login_response_json(user1);
                            res.status(200).send({ message: global.messages.loginSuccess, status: 1, data: response });
                        } else {
                            res.status(400).send({ message: "Something wrong", status: 0 });
                        }
                    }
                } else // user not found
                {
                    res.status(200).send({ message: "Not exists", status: 2 });
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});

var login_social_validate = function (req, res, next) {
    req.assert("sns_type").notEmpty().withMessage("Please provide sns type.");
    req.assert("sns_id").notEmpty().withMessage("Please provide sns id.");
    //    req.assert("device_token").notEmpty().withMessage("Please provide device_token.");

    return req.validationErrors(true);
};

module.exports = router;
