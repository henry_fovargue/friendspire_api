var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var notifications = require('../controller/notification.js');
var Sync = require('sync');

//route for get notification list
router.get('/:pageNo', function (req, res, next) {
    Sync(function () {
        try {
            console.log(req.decoded._id);
            // find user data
            var user_data = users.findOne.sync(null, { _id: req.decoded._id }, {});
            if (user_data.status === 1) {
                // current date
                var current_date = new Date();
                // date 30 days before
                var before_one_month = current_date.setDate(current_date.getDate() - 30);
                // get notification list
                var projection = { _id: 1, is_read: 1, message: 1, type: 1, from: 1, reference_id: 1 };
                var population = [{
                    path: 'from',
                    select: '_id firstName lastName profile_pic'
                }, {
                    path: 'to',
                    select: '_id firstName lastName profile_pic'
                }, {
                    path: 'reference_id',
                    model: 'post',
                    select: '_id type title'
                }, {
                    path: 'recommendation_id',
                    model: 'recommendation',
                    select: '_id type reference_id'
                }, {
                    path: 'comment_id',
                    model: 'comment',
                    select: '_id'
                }];
                var notifications_list = notifications.find_with_projection_population_and_pagination.sync(null, { to: req.decoded._id, created_at: { $gt: before_one_month } }, projection, population, { _id: -1 }, req.params.pageNo);
                if (notifications_list.status === 1) {
                    res.status(200).send({ message: notifications_list.message, status: 1, data: notifications_list.data });
                } else {
                    throw Error(notifications_list.message);
                }
            } else {
                throw Error("Invalid User");
            }
        } catch (err) // catch errors
        {
            res.status(400).send({ message: err.message, status: 0 });
        }
    });
});

//route for read notification
router.put('/', function (req, res, next) {
    var errors = req_validate(req, res, next);
    if (!errors) {
        Sync(function () {
            try {
                // find user data
                var user_data = users.findOne.sync(null, { _id: req.decoded._id }, {});
                if (user_data.status === 1) {
                    // read notification
                    var notifications_read = notifications.update.sync(null, { _id: req.body.notification_id, is_read: 0 }, { is_read: 1 });
                    if (notifications_read.status === 1) {
                        res.status(200).send({ message: "success", status: 1 });
                    } else {
                        throw Error(notifications_read.message);
                    }
                } else {
                    throw Error("Invalid User");
                }
            } catch (err) // catch errors
            {
                res.status(400).send({ message: err.message, status: 0 });
            }
        });
    } else {
        res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
    }
});
var req_validate = function (req, res, next) {
    req.assert('notification_id', 'notification_id required').notEmpty();
    return req.validationErrors(true);
};




module.exports = router;
