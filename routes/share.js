var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('../config');


router.get('/profile/:id', function (req, res, next) {
    try {
        var url = config.url_redirect + "user_profile?profile_id=" + req.params.id;
        console.log(url);
        var data = {
            "dynamicLinkInfo": {
                "dynamicLinkDomain": config.deeplink.dynamicLinkDomain,
                "link": url,
                "androidInfo": {
                    "androidPackageName": config.deeplink.androidPackageName,
                    "androidFallbackLink": config.deeplink.androidFallbackLink
                },
                "iosInfo": {
                    "iosBundleId": config.deeplink.iosBundleId,
                    "iosFallbackLink": config.deeplink.iosFallbackLink
                }
            },
            "suffix": {
                "option": "SHORT"
            }
        };
        var dataString = JSON.stringify(data);
        var options = {
            url: "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + config.deeplink.API_Key,
            method: 'POST',
            body: dataString
        };

        function callback(error, response, body) {
            body = JSON.parse(body);
            if (!error && response.statusCode === 200) {
                res.status(200).send({ status: 1, message: "success", link: body.shortLink });
            } else {
                res.status(400).send({ status: 0, message: body.error });
            }
        }
        request(options, callback);
    } catch (err) {
        return next(err);
    }
});

router.get('/:id/:type', function (req, res, next) {
    try {
        var url = config.url_redirect + "posts_detail?postId=" + req.params.id + "&category=" + req.params.type;
        console.log(url);
        var data = {
            "dynamicLinkInfo": {
                "dynamicLinkDomain": config.deeplink.dynamicLinkDomain,
                "link": url,
                "androidInfo": {
                    "androidPackageName": config.deeplink.androidPackageName,
                    "androidFallbackLink": config.deeplink.androidFallbackLink
                },
                "iosInfo": {
                    "iosBundleId": config.deeplink.iosBundleId,
                    "iosFallbackLink": config.deeplink.iosFallbackLink
                }
            },
            "suffix": {
                "option": "SHORT"
            }
        };
        var dataString = JSON.stringify(data);
        var options = {
            url: "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + config.deeplink.API_Key,
            method: 'POST',
            body: dataString
        };

        function callback(error, response, body) {
            body = JSON.parse(body);
            if (!error && response.statusCode === 200) {
                res.status(200).send({ status: 1, message: "success", link: body.shortLink });
            } else {
                res.status(400).send({ status: 0, message: body.error });
            }
        }
        request(options, callback);
    } catch (err) {
        return next(err);
    }
});


module.exports = router;