var express = require('express');
var router = express.Router();
var post = require('../controller/post.js');
var mongoose = require('mongoose');
var Sync = require('sync');
var projectionfield = require("../helper/projectionFields");
const config = require("../config");
router.get('/', function (req, res, next) {
    Sync(function () {
        try {
            req.query.type = parseInt(req.query.type);
            if (req.query.title && req.query.type) {
                if (req.query.type == 1 || req.query.type == 2) {
                    if (!req.query.lat_long) {
                        throw { message: "please enter lat long" };
                    }
                }
                //var try_string = req.query.title;
                //var string_get = try_string.replace(/[-[\]{}()*+?.,\\/^$|#\s]/g, "\\$&");
                //console.log(string_get);
                var condition = {};
                var try_string = req.query.title;
                var string_get = try_string.replace(/[-[\]{}()*+?.,\\/^$|#\s]/g, "\\$&");
                condition.title = new RegExp('^' + string_get + '.*', 'i');
                condition.type = parseInt(req.query.type);
                var projection = { title: 1 };
                if (req.query.type == 1 || req.query.type == 2) {  //restaurant and bar
                    var coordinates = JSON.parse(req.query["lat_long"]);
                    let distance = config.nearbyRaduis;
                    aggregation_pipeline = [
                        {
                            $geoNear: {
                                near: [coordinates[1], coordinates[0]],
                                distanceField: "distance",
                                query: condition,
                                distanceMultiplier: 6371000,
                                maxDistance: distance,
                                spherical: true
                            }
                        },
                        { "$project": projection }
                    ]
                } else {
                    aggregation_pipeline = [
                        { "$match": condition },
                        { "$project": projection }
                    ];
                }
                var data_list = post.aggregate.sync(null, aggregation_pipeline);
                if (data_list.status == 1) {
                    res.status(200).send({ message: "Success", status: 1, data: data_list.data });
                } else {
                    res.status(400).send({ message: data_list.message, status: 0 });
                }
            } else {
                res.status(400).send({ message: "Invalid request", status: 0 });
            }
        } catch (err) // catch errors
        {
            res.send({ message: err.message, status: 0 });
        }
    });
});


module.exports = router;