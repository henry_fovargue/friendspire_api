var express = require('express');
var router = express.Router();
var users = require('../controller/users.js');
var Sync = require('sync');
var crypto = require('../helper/crypto.js'); //for encryption and decryption of user_id

router.get('/:token', function (req, res, next)
{
    Sync(function () {
        try {
            // find user data
            var user_id = crypto.decrypt(req.params.token);
            var user_data = users.findOne.sync(null, {_id: user_id},{});
            if (user_data.status === 1) {
                if(user_data.data.is_email_changed == 0)
                    return res.render("email_verified", {message:global.messages.linkExpired});
                // update email of user
                users.update.sync(null, {_id: user_id}, {email: user_data.data.temp_email, temp_email: "", is_email_verified:1, is_email_changed:0});
                res.render('email_verified',{message:global.messages.emailVerified});
            } else {
                res.render("error", {error: "Session Expired"});
            }
        } catch (err) // catch errors
        {
            res.render("error", {error: err});
        }
    });
});

module.exports = router;