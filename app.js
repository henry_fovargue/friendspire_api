// require connection file to connect mongo
require('./connection');

var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var validator = require('express-validator');

var app = express();
var swig = require('swig');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', swig.renderFile);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(validator());

app.use(function (req, res, next) {
    global.messages = require('./locales/en');
    global.push_messages = require('./locales/en_push');
    if (!req.headers.lang)
        req.headers.lang = "en";
    next();
});



require('./routes/cron_job');

app.use('/invalid_url', function (req, res, next) {
    res.render('error_desktop');
});
app.use('/user_profile', function (req, res, next) {
    res.render('error_desktop');
});
app.use('/posts_detail', function (req, res, next) {
    res.render('error_desktop');
});
app.use('/', require('./routes/index'));
app.use('/is_social_media_exists', require('./routes/is_social_media_exists'));
app.use('/privacy_policy', require('./routes/privacy_policy'));
app.use('/termsConditions', require('./routes/termsConditions'));
app.use('/aboutUs', require('./routes/aboutUs'));
app.use('/verify_email', require('./routes/verify_email'));
var auth = require('./routes/auth');
app.use('/otp_verification', auth, require('./routes/otp_verification'));
app.use('/update_device', auth, require('./routes/update_device'));
app.use('/settings', auth, require('./routes/settings'));
app.use('/profile', auth, require('./routes/profile'));
app.use('/password', auth, require('./routes/password'));
app.use('/sync_friends', auth, require('./routes/sync_friends'));
app.use('/get_friends', auth, require('./routes/get_friends'));
app.use('/network', auth, require('./routes/network'));
app.use('/follow', auth, require('./routes/follow'));
app.use('/following', auth, require('./routes/following'));
app.use('/share', auth, require('./routes/share'));
app.use('/invite', auth, require('./routes/invite'));
app.use('/users', auth, require('./routes/users'));
app.use('/bookmark', auth, require('./routes/bookmark'));
app.use('/searchdata', auth, require('./routes/pre_search_item'));
app.use('/recommendation', auth, require('./routes/recommendation'));
app.use('/discover', auth, require('./routes/discover'));
app.use('/report', auth, require('./routes/report'));
app.use('/get_contact_category', auth, require('./routes/get_contact_category'));
app.use('/notification', auth, require('./routes/notification'));
//milestone 2
app.use('/posts', auth, require('./routes/posts'));
//milestone 4
app.use('/faqs', auth, require('./routes/faqs'));
app.use('/change_password', auth, require('./routes/change_password'));
app.use('/notification', auth, require('./routes/notification'));
app.use('/contact_request', auth, require('./routes/contact_request'));
app.use('/comment', auth, require('./routes/comment'));
app.use('/logout', auth, require('./routes/logout'));
app.use('/invitecode', auth, require('./routes/invitecode'));
app.use('/tag', auth, require('./routes/tag'));

//app.use('/cron', auth, require('./routes/cron_job'));
app.use('/block', auth, require('./routes/block'));
// app.use('/version', auth, require('./routes/version'));

/// catch 404 and forwarding to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});




/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
});


module.exports = app;


